using UnityEngine;

namespace GameReady.Build
{
    public class BuildSettings : ScriptableObject
    {
        private static BuildSettings _instance;

        [SerializeField] private string _version;

        [SerializeField] private int _buildNumberAndroid;
        [SerializeField] private int _buildNumberIos;

        [SerializeField] private bool _debug;

        [SerializeField] private string _certificateRelativePath;

        [SerializeField] private string _certificateAlias;

        [SerializeField] private bool _useCustomKeyStore;

        [SerializeField] private string _certificateKeyStorePassword;

        public string Version
        {
            get => _version;
            set => _version = value;
        }

        public int BuildNumber
        {
#if UNITY_ANDROID
            get => _buildNumberAndroid;
            set => _buildNumberAndroid = value;
#elif UNITY_IOS
            get => _buildNumberIos;
            set => _buildNumberIos = value;
#else
            get => _buildNumberAndroid;
            set => _buildNumberAndroid = value;
#endif
        }

        public bool DebugBuild
        {
            get => _debug;
            set => _debug = value;
        }

        public static BuildSettings Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = Resources.Load<BuildSettings>(nameof(BuildSettings));
                }

                return _instance;
            }
        }

        public string CertificatePassword
        {
            get => _certificateKeyStorePassword;
            set => _certificateKeyStorePassword = value;
        }

        public string CertificateAlias
        {
            get => _certificateAlias;
            set => _certificateAlias = value;
        }

        public string CertificateRelativePath
        {
            get => _certificateRelativePath;
            set => _certificateRelativePath = value;
        }

        public bool UseCustomKeyStore
        {
            get => _useCustomKeyStore;
            set => _useCustomKeyStore = value;
        }

        public void FillFromProjectSettings()
        {
#if UNITY_EDITOR

            _version = Application.version;
            _debug = Debug.isDebugBuild;
            /*
             * Compile Symbols
             */
            _buildNumberAndroid = UnityEditor.PlayerSettings.Android.bundleVersionCode;
            _buildNumberIos = int.Parse(UnityEditor.PlayerSettings.iOS.buildNumber);

#endif
        }
    }
}