using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gameready.Gameplay.FSM
{
    /// <summary>
    /// Base generic FSM using Enum types
    /// Implement SetState method, Current and Prev states 
    /// </summary>
    /// <typeparam name="TEnum"></typeparam>
    public sealed class SimpleEnumFSM<TEnum> where TEnum : struct, Enum
    {
        private readonly bool _printLogs;

        public event Action<TEnum, TEnum> OnStateChanged;

        private TEnum _state;

        private TEnum _prevState;

        private readonly Predicate<TEnum> _setStatePredicate;

        private readonly string _stateTypeName;

        private bool DefaultSetStatePredicate(TEnum state)
        {
            return !EqualityComparer<TEnum>.Default.Equals(_state, state);
        }

        public SimpleEnumFSM(bool printLogs = true, Predicate<TEnum> setStatePredicate = null)
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("FSMGeneric: type must be Enumeration!");
            }

            _printLogs = printLogs;
            _stateTypeName = typeof(TEnum).Name;
            _setStatePredicate = setStatePredicate ?? DefaultSetStatePredicate;
        }

        public TEnum CurrentState => _state;

        public TEnum PreviousState => _prevState;

        public void SetState(TEnum state)
        {
            if (_setStatePredicate(state))
            {
                _prevState = _state;
                _state = state;
                if (_printLogs)
                {
                    Debug.Log($"FSM-{_stateTypeName} (PrevState:{_prevState} CurrentState:{_state})");
                }

                OnStateChanged?.Invoke(_prevState, state);
            }
        }
    }
}