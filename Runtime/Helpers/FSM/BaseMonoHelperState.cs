using System;
using Gameready.Utils;

namespace Gameready.Gameplay.FSM
{
    /// <summary>
    /// abstract State helper with Monohelper
    /// </summary>
    /// <typeparam name="TEnumState"></typeparam>
    public abstract class BaseMonoHelperState<TEnumState> : IState where TEnumState : struct, Enum
    {
        public IStateMachine<TEnumState> stateMachine;

        public virtual void Dispose()
        {
            if (this is IBaseUpdatable updatable)
            {
                MonoHelper.RemoveFromUpdate(updatable);
            }
        }
        
        public virtual void OnEnter()
        {
            if (this is IBaseUpdatable updatable)
            {
                MonoHelper.AddOnUpdate(updatable);
            }
        }

        public virtual void OnExit()
        {
            if (this is IBaseUpdatable updatable)
            {
                MonoHelper.RemoveFromUpdate(updatable);
            }
        }
    }
}