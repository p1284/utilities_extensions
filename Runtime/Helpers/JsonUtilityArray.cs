using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gameready.Helpers
{
    public static class JsonUtilityArray
    {
       
        public static List<T> FromJsonArray<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return new List<T>(wrapper.Items);
        }
        
        public static string ToJsonArray<T>(T[] array, bool prettyPrint = false)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = new List<T>(array);
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }
        
        
        public static string ToJsonList<T>(List<T> list, bool prettyPrint = false)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = list;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public List<T> Items;
        }
    }
}