using Gameready.Utils;
using UnityEngine;

namespace Gameready.Helpers
{
    public class BillBoardHelper : MonoBehaviour, ILateUpdate
    {
        [Tooltip("MainCamera by default")] [SerializeField]
        private Camera _camera;

        [SerializeField] private Vector3 _worldUp = Vector3.up;
        [SerializeField] private Vector3 _orientationAngles = new Vector3(0, 180, 0);

        private Quaternion _startRotation;

        private void Awake()
        {
            _startRotation = transform.rotation;

            if (_camera == null)
            {
                _camera = Camera.main;
            }
        }

        private void Start()
        {
            OnLateUpdate();
        }

        private void OnEnable()
        {
            MonoHelper.AddOnUpdate(this);
        }

        private void OnDisable()
        {
            transform.rotation = _startRotation;
            MonoHelper.RemoveFromUpdate(this);
        }


        public void OnLateUpdate()
        {
            transform.LookAt(_camera.transform, _worldUp);
            transform.Rotate(_orientationAngles);
        }
    }
}