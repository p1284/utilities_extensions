using Gameready.Utils.Attributes;
using UnityEngine;

namespace Gameready.Helpers
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    public class HideInHierarchy : MonoBehaviour
    {
        [SerializeField] private Transform _child;

        private void Start()
        {
            if(!Application.isPlaying)return;
            Destroy(this);
        }

        [Button]
        public void ShowLevelObject()
        {
            _child = _child == null ? transform.GetChild(0) : _child;
            if (_child != null)
            {
                _child.gameObject.hideFlags = HideFlags.None;
            }
        }
        
        [Button]
        public void HideLevelObject()
        {
            _child = _child == null ? transform.GetChild(0) : _child;
            if (_child != null)
            {
                _child.gameObject.hideFlags = HideFlags.HideInHierarchy;
            }
        }
        
        private void OnEnable()
        {
            gameObject.hideFlags = HideFlags.None;
        }
    }
}