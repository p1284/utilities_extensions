using System.Collections.Generic;
using Gameready.Utils.Data;
using UnityEngine;

namespace Gameready.Helpers
{
    public static class JsonUtilityDictionary<TKey, TValue>
    {
        private static List<KeyValue<TKey, TValue>> _list;
        
        
        public static void FromJson(string json, ref Dictionary<TKey, TValue> dictionary)
        {
            _list ??= new List<KeyValue<TKey, TValue>>(20);
            _list.Clear();

            dictionary ??= new Dictionary<TKey, TValue>();

            JsonUtility.FromJsonOverwrite(json, _list);

            KeyValue<TKey, TValue>.FillDictionary(_list, ref dictionary);
        }
        
        public static  Dictionary<TKey, TValue> FromJson(string json)
        {
            _list ??= new List<KeyValue<TKey, TValue>>(20);
            _list.Clear();

            JsonUtility.FromJsonOverwrite(json, _list);

            return KeyValue<TKey, TValue>.CreateDictionary(_list);
        }

        public static string ToJson(Dictionary<TKey, TValue> dictionary,bool prettyPrint = false)
        {
            _list ??= new List<KeyValue<TKey, TValue>>(20);
            _list.Clear();

            KeyValue<TKey, TValue>.FillList(dictionary, ref _list);

            return JsonUtility.ToJson(_list,prettyPrint);
        }
    }
}