using System;
using UnityEngine;

namespace Gameready.Gameplay
{
    public enum UpgradeCalcType
    {
        Linear,
        Exponentially,
        Percentage,
        Logarithmic,
        Square,
        Cubic,
        Curve
    }

    [Serializable]
    public class UpgradeElement
    {
        [SerializeField] private AnimationCurve _upgradeCurve = AnimationCurve.Linear(0, 0, 1f, 1f);
        [SerializeField] private float _upgradeKoof = 1f;
        [SerializeField] private float _baseUpgradeValue;
        [SerializeField] private UpgradeCalcType _upgradeCalcType;

       // private Func<int, int, float, float, float> _custom;

        public float GetUpgradeValue(int level, int maxUpgrades)
        {
            level = Mathf.Max(level, 0);
            return UpgradeInternal(_upgradeCalcType, level, maxUpgrades);
        }

        public int GetUpgradeRoundedValue(int level, int maxUpgrades)
        {
            return Mathf.RoundToInt(GetUpgradeValue(level, maxUpgrades));
        }

        private float UpgradeInternal(UpgradeCalcType calcType, int level, int maxUpgrades)
        {
            level = Mathf.Min(level, maxUpgrades);

            // if (useCustom && _custom != null)
            // {
            //     return _custom.Invoke(level, maxUpgrades, _baseUpgradeValue, _upgradeKoof);
            // }

            switch (calcType)
            {
                case UpgradeCalcType.Linear:
                    return _baseUpgradeValue + level * _upgradeKoof;
                case UpgradeCalcType.Exponentially:
                    return _baseUpgradeValue * Mathf.Pow(level, _upgradeKoof);
                case UpgradeCalcType.Percentage:
                    return _baseUpgradeValue * Mathf.Pow(1 + _upgradeKoof / 100, level - 1);
                case UpgradeCalcType.Logarithmic:
                    return level == 0 ? _baseUpgradeValue : _baseUpgradeValue + _upgradeKoof * Mathf.Log(level);
                case UpgradeCalcType.Square:
                    return _baseUpgradeValue + _upgradeKoof * Mathf.Pow(level, 2);
                case UpgradeCalcType.Cubic:
                    return _baseUpgradeValue + _upgradeKoof * Mathf.Pow(level, 3);
                case UpgradeCalcType.Curve:
                    return _baseUpgradeValue + _upgradeKoof * _upgradeCurve.Evaluate(level / (float)maxUpgrades);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        // /// <summary>
        // /// 
        // /// </summary>
        // /// <param name="type"></param>
        // /// <param name="maxUpgrades"></param>
        // /// <returns>CalcUpgradeFunc(int level)</returns>
        // public Func<int, float> GetUpgradeCalcFunc(UpgradeCalcType type, int maxUpgrades)
        // {
        //     return level =>
        //         UpgradeInternal(type, level, maxUpgrades);
        // }
        //
        //
        // /// <summary>
        // /// Custom calc function where:
        // /// level,maxUpgrades,baseUpgradeValue,upgradeKoof - arguments
        // /// return calculated upgrade value
        // /// </summary>
        // /// <param name="custom"></param>
        // public void SetUpgradeCalcFunc(Func<int, int, float, float, float> custom)
        // {
        //     _custom = custom;
        // }
    }
}