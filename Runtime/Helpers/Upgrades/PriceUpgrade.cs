﻿using System;
using UnityEngine;

namespace Gameready.Gameplay
{
    [Serializable]
    public sealed class PriceUpgrade:Upgrade
    {
        [SerializeField] private UpgradeElement _price;

        
        /// <summary>
        /// For level=1 return price:0
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public int GetRoundedPrice(int level)
        {
            return _price.GetUpgradeRoundedValue(level, _maxUpgrades);
        }

        public float GetPrice(int level)
        {
            return _price.GetUpgradeValue(level, _maxUpgrades);
        }
    }
}