using System;
using UnityEngine;

namespace Gameready.Gameplay
{
    [Serializable]
    public class Upgrade
    {
        [SerializeField] private UpgradeElement _upgrade;

        [SerializeField] protected int _maxUpgrades;

        public float GetUpgradeValue(int level)
        {
            return _upgrade.GetUpgradeValue(level, _maxUpgrades);
        }

        public int GetUpgradeRoundedValue(int level)
        {
            return _upgrade.GetUpgradeRoundedValue(level, _maxUpgrades);
        }

        public int MaxUpgrades => _maxUpgrades;
    }
}