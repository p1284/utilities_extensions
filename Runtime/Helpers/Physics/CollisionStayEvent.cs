using UnityEngine;

namespace Gameready.Helpers.Physics
{
    public class CollisionStayEvent : CollisionEvent
    {
        private void OnCollisionStay(Collision other)
        {
            CollideEvent(PhysicsCallbackState.Stay, other);
        }
    }
}