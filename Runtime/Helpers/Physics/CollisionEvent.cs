using System;
using UnityEngine;

namespace Gameready.Helpers.Physics
{
    public interface ICollisionEvent
    {
        event Action<PhysicsCallbackState, Collision> OnCollided;
        
        void Destroy();
    }
    
    public class CollisionEvent : MonoBehaviour, ICollisionEvent
    {
        public event Action<PhysicsCallbackState, Collision> OnCollided;
        
        public void Destroy()
        {
            Destroy(this);
        }

        protected void CollideEvent(PhysicsCallbackState state, Collision collision)
        {
            OnCollided?.Invoke(state, collision);
        }

        private void OnCollisionEnter(Collision other)
        {
            CollideEvent(PhysicsCallbackState.Enter, other);
        }

        private void OnCollisionExit(Collision other)
        {
            CollideEvent(PhysicsCallbackState.Exit, other);
        }
    }
}