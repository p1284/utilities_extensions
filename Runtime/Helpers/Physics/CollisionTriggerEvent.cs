using System;
using UnityEngine;

namespace Gameready.Helpers.Physics
{
    public interface ICollisionTriggerEvent
    {
        event Action<PhysicsCallbackState, Collider> OnTriggered;
        
        void Destroy();
    }

    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider))]
    public class CollisionTriggerEvent : MonoBehaviour, ICollisionTriggerEvent
    {
        public event Action<PhysicsCallbackState, Collider> OnTriggered;
        
        protected void TriggerEvent(PhysicsCallbackState state, Collider other)
        {
            OnTriggered?.Invoke(state, other);
        }

        private void Start()
        {
            GetComponent<Collider>().isTrigger = true;
        }

        public void Destroy()
        {
            Destroy(this);
        }

        private void OnTriggerEnter(Collider other)
        {
            TriggerEvent(PhysicsCallbackState.Enter, other);
        }

        private void OnTriggerExit(Collider other)
        {
            TriggerEvent(PhysicsCallbackState.Exit, other);
        }
    }
}