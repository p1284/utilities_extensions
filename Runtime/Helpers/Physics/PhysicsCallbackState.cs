namespace Gameready.Helpers.Physics
{
    public enum PhysicsCallbackState
    {
        Enter,
        Exit,
        Stay
    }
}