using UnityEngine;

namespace Gameready.Helpers.Physics
{
    public sealed class CollisionTriggerStayEvent : CollisionTriggerEvent
    {
        private void OnTriggerStay(Collider other)
        {
            TriggerEvent(PhysicsCallbackState.Stay, other);
        }
    }
}