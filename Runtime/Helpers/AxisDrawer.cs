using UnityEngine;

namespace Gameready.Helpers
{
    /// <summary>
    /// Draw gizmos axis in Editor mode
    /// </summary>
    [ExecuteInEditMode]
    public class AxisDrawer : MonoBehaviour
    {
        [Range(0f, 100f)] [SerializeField] private float _forward = 1f;
        [Range(0f, -100f)] [SerializeField] private float _back = 0f;
        [Range(0f, 100f)] [SerializeField] private float _right = 1f;
        [Range(0f, -100f)] [SerializeField] private float _left = 0f;
        [Range(0f, 100f)] [SerializeField] private float _up = 1f;
        [Range(0, -100f)] [SerializeField] private float _down = 0f;

        [SerializeField] private Color _yColor = new Color32(0, 1, 0, 1);
        [SerializeField] private Color _xColor = new Color32(1, 0, 0, 1);
        [SerializeField] private Color _zColor = new Color32(0, 0, 1, 1);

        [Range(1, 20)] [SerializeField] private int _lineWidth = 1;

        private void Start()
        {
            if (Application.isPlaying)
                DestroyImmediate(this);
        }

        private void Reset()
        {
            _yColor = new Color(0, 1, 0, 1);
            _xColor = new Color(1, 0, 0, 1);
            _zColor = new Color(0, 0, 1, 1);

            _left = -1f;
            _right = 1f;

            _up = 1f;
            _down = -1f;

            _forward = 1f;
            _back = -1f;
        }

        private void OnDrawGizmos()
        {
            
            // Gizmos.color = Color.white;
            // Gizmos.DrawSphere(transform.position,0.2f);

            for (int i = 0; i < _lineWidth; i++)
            {
              
                var step = (i * 0.0001f);
                
                Gizmos.color = _xColor;
                Gizmos.DrawLine(transform.position + Vector3.forward*step, transform.right * _right + Vector3.forward*step);
                Gizmos.DrawLine(transform.position - Vector3.forward*step, transform.right * _right - Vector3.forward*step);
                Gizmos.DrawLine(transform.position + Vector3.forward*step, transform.right * _left + Vector3.forward*step);
                Gizmos.DrawLine(transform.position - Vector3.forward*step, transform.right * _left - Vector3.forward*step);
                
                Gizmos.DrawLine(transform.position + Vector3.up*step, transform.right * _right + Vector3.up*step);
                Gizmos.DrawLine(transform.position - Vector3.up*step, transform.right * _right - Vector3.up*step);
                Gizmos.DrawLine(transform.position + Vector3.up*step, transform.right * _left + Vector3.up*step);
                Gizmos.DrawLine(transform.position - Vector3.up*step, transform.right * _left - Vector3.up*step);
              

                Gizmos.color = _yColor;
                Gizmos.DrawLine(transform.position+ Vector3.forward*step, transform.up * _up+ Vector3.forward*step);
                Gizmos.DrawLine(transform.position- Vector3.forward*step, transform.up * _up- Vector3.forward*step);
                Gizmos.DrawLine(transform.position+ Vector3.forward*step, transform.up * _down+ Vector3.forward*step);
                Gizmos.DrawLine(transform.position- Vector3.forward*step, transform.up * _down- Vector3.forward*step);
                
                Gizmos.DrawLine(transform.position+ Vector3.right*step, transform.up * _up+ Vector3.right*step);
                Gizmos.DrawLine(transform.position- Vector3.right*step, transform.up * _up- Vector3.right*step);
                Gizmos.DrawLine(transform.position+ Vector3.right*step, transform.up * _down+ Vector3.right*step);
                Gizmos.DrawLine(transform.position- Vector3.right*step, transform.up * _down- Vector3.right*step);

                Gizmos.color = _zColor;
                Gizmos.DrawLine(transform.position+ Vector3.right*step, transform.forward * _forward+ Vector3.right*step);
                Gizmos.DrawLine(transform.position- Vector3.right*step, transform.forward * _forward- Vector3.right*step);
                Gizmos.DrawLine(transform.position+ Vector3.right*step, transform.forward * _back+ Vector3.right*step);
                Gizmos.DrawLine(transform.position- Vector3.right*step, transform.forward * _back- Vector3.right*step);
                
                Gizmos.DrawLine(transform.position+ Vector3.up*step, transform.forward * _forward+ Vector3.up*step);
                Gizmos.DrawLine(transform.position- Vector3.up*step, transform.forward * _forward- Vector3.up*step);
                Gizmos.DrawLine(transform.position+ Vector3.up*step, transform.forward * _back+ Vector3.up*step);
                Gizmos.DrawLine(transform.position- Vector3.up*step, transform.forward * _back- Vector3.up*step);
                
            }
        }
    }
}