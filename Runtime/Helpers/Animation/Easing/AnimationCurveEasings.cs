﻿using System;
using Gameready.Utils.Attributes;
using UnityEngine;

namespace Gameready.Animations.Easing
{
    [Serializable]
    public class AnimationCurveEasings : ISerializationCallbackReceiver
    {
        public event Action OnCurveUpdated;

        [SerializeField] private AnimationCurve _curve = AnimationCurve.Linear(0, 0, 1, 1);

        [SerializeField] private EasingFunctions.Ease _ease = EasingFunctions.Ease.Linear;

        public float Evaluate(float time)
        {
            return _curve.Evaluate(time);
        }

       
        private void SetEaseToCurve()
        {
            if (_ease == EasingFunctions.Ease.Custom) return;
            _curve = EasingAnimationCurve.EaseToAnimationCurve(_ease);
        }
        
        public void UpdateCurve()
        {
            OnCurveUpdated?.Invoke();
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            SetEaseToCurve();
        }
    }
}