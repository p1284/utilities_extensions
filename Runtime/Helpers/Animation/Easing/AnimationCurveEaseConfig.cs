﻿using UnityEngine;

namespace Gameready.Animations.Easing
{
    [ExecuteAlways]
    [CreateAssetMenu(menuName = "AnimationCurveEase", fileName = "CreateAnimationCurveEase")]
    public class AnimationCurveEaseConfig : ScriptableObject
    {
        [SerializeField] private AnimationCurveEasings _curveEasings;
        
        public float Evaluate(float time)
        {
            return _curveEasings.Evaluate(time);
        }
        
        public void UpdateEasing()
        {
            _curveEasings.UpdateCurve();
        }
    }
}