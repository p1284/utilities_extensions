using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gameready.Utils.Animation
{
    public class AnimatorStateBehaviorHelper : StateMachineBehaviour
    {
        public event Action<AnimatorStateBehaviorHelper> OnEnter;

        public event Action<AnimatorStateBehaviorHelper> OnExit;

        public event Action<AnimatorStateBehaviorHelper> OnUpdate;

        public AnimatorStateInfo currentState;

        public readonly List<AnimatorClipInfo> currentClips = new List<AnimatorClipInfo>();

        [SerializeField] private string _stateId;

        public string StateId => _stateId;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            currentClips.Clear();
            animator.GetCurrentAnimatorClipInfo(layerIndex, currentClips);
            currentState = stateInfo;
            OnEnter?.Invoke(this);
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            currentState = stateInfo;
            currentClips.Clear();
            animator.GetCurrentAnimatorClipInfo(layerIndex, currentClips);
            OnExit?.Invoke(this);
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            currentState = stateInfo;
            OnUpdate?.Invoke(this);
        }

        // public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo,
        //     int layerIndex)
        // {
        // }
        //
        // public override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo,
        //     int layerIndex)
        // {
        // }
    }
}