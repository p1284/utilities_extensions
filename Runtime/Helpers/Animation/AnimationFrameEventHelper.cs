using System;
using UnityEngine;

namespace Gameready.Utils.Animation
{
    [RequireComponent(typeof(Animator))]
    public class AnimationFrameEventHelper : MonoBehaviour
    {
        public event Action OnFrameEvent;

        public event Action<string> OnFrameDataEvent;

        public event Action<float> OnFrameFloatEvent;

        public void OnEventFrame()
        {
            OnFrameEvent?.Invoke();
        }

        public void OnDataEventFrame(string data)
        {
            OnFrameDataEvent?.Invoke(data);
        }

        public void OnFloatEventFrame(float value)
        {
            OnFrameFloatEvent?.Invoke(value);
        }
    }
}