﻿using System;
using UnityEngine;

namespace Gameready.Utils.Attributes
{
    public enum ButtonInvokeType
    {
        SimpleInvoke,
        InvokeInPrefabSceneModeWithSceneDirty
    }
    
    [AttributeUsage(AttributeTargets.Method)]
    public class ButtonAttribute : PropertyAttribute
    {
        public readonly string Name;
        public readonly ButtonInvokeType InvokeType;
        public readonly bool EditorOnly;

        public ButtonAttribute(string name = "",ButtonInvokeType invokeType = ButtonInvokeType.SimpleInvoke,bool editorOnly = true)
        {
            Name = name;
            InvokeType = invokeType;
            EditorOnly = editorOnly;
        }
    }
}