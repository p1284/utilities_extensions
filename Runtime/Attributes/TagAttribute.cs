using System;
using UnityEngine;

namespace Gameready.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class TagAttribute:PropertyAttribute
    {
        
    }
}