using System;

namespace Gameready.Utils.Attributes
{
    public class ScriptOrderAttribute : Attribute
    {
        public int order;

        public ScriptOrderAttribute(int order)
        {
            this.order = order;
        }
    }
}