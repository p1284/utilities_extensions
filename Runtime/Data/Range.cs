using System;
using Gameready.Random;
using UnityEngine;

namespace Gameready.Utils.Data
{
    [Serializable]
    public struct Range
    {
        [SerializeField] private float _min;
        [SerializeField] private float _max;

        public Range(float min, float max)
        {
            _min = min;
            _max = max;
        }

        public void SetMinMax(float min, float max)
        {
            _min = min;
            _max = max;
            if (!Validate())
            {
                Debug.LogError($"Range validation error: min={_min} max={_max}");
            }
        }

        public bool Validate()
        {
            return !(_min > _max);
        }

        public int MinInt => (int)_min;
        public int MaxInt => (int)_max;

        public float Min => _min;

        public float Max => _max;

        public bool IsInRange(float value)
        {
            return value >= _min && value <= _max;
        }

        public float GetRemapped(float value, float destMin, float destMax)
        {
            return MathUtils.Remap(value, _min, _max, destMin, destMax);
        }

        public float GetClamped(float value)
        {
            return Mathf.Clamp(value, _min, _max);
        }

        public float GetRandomValue()
        {
            return GameRandom.Range(_min, _max);
        }

        public int GetRandomInt()
        {
            return GameRandom.Range((int)_min, (int)_max);
        }
    }
}