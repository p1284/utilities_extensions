using System;
using System.Collections.Generic;

namespace Gameready.Utils.Data
{
    [Serializable]
    public class KeyValue<TKey, TValue> : IEquatable<KeyValue<TKey, TValue>>, IEquatable<TKey>
    {
        public TKey key;

        public TValue value;

        public KeyValue(TKey key, TValue value)
        {
            this.key = key;
            this.value = value;
        }

        public static void FillList<TK, TV>(IDictionary<TK, TV> dictionary, ref List<KeyValue<TK, TV>> list)
        {
            if (dictionary == null) return;

            list.Clear();
            foreach (var kvData in dictionary)
            {
                list.Add(new KeyValue<TK, TV>(kvData.Key, kvData.Value));
            }
        }

        public static List<KeyValue<TK, TV>> CreateList<TK, TV>(IDictionary<TK, TV> dictionary)
        {
            if (dictionary == null) return null;
            var list = new List<KeyValue<TK, TV>>();

            foreach (var kvData in dictionary)
            {
                list.Add(new KeyValue<TK, TV>(kvData.Key, kvData.Value));
            }

            return list;
        }

        public static void FillDictionary<TK, TV>(IEnumerable<KeyValue<TK, TV>> list, ref Dictionary<TK, TV> dictionary)
        {
            if (list == null) return;
            dictionary.Clear();
            foreach (var keyValue in list)
            {
                dictionary.TryAdd(keyValue.key, keyValue.value);
            }
        }

        public static Dictionary<TK, TV> CreateDictionary<TK, TV>(IEnumerable<KeyValue<TK, TV>> list)
        {
            if (list == null) return null;
            var dict = new Dictionary<TK, TV>();
            foreach (var keyValue in list)
            {
                dict.TryAdd(keyValue.key, keyValue.value);
            }

            return dict;
        }

        public bool Equals(TKey other)
        {
            return EqualityComparer<TKey>.Default.Equals(key, other);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((KeyValue<TKey, TValue>)obj);
        }

        public bool Equals(KeyValue<TKey, TValue> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return EqualityComparer<TKey>.Default.Equals(key, other.key) &&
                   EqualityComparer<TValue>.Default.Equals(value, other.value);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (EqualityComparer<TKey>.Default.GetHashCode(key) * 397) ^
                       EqualityComparer<TValue>.Default.GetHashCode(value);
            }
        }
    }
}