using UnityEngine;

namespace Gameready.Utils.Data
{
    public struct LoopInt
    {
        public readonly int MIN;
        public readonly int MAX;
        private int _counter;
        
        public LoopInt(int min,int max)
        {
            _counter = 0;
            MIN = min;
            MAX = max;
            _counter = Mathf.Clamp(_counter, min, max);
        }

        public void Reset()
        {
            _counter = MIN;
        }

        public int Value
        {
            get
            {
                if (_counter > MAX)
                {
                   Reset();
                }
                return _counter++;
            }
        }
    }
}