﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameready.Utils.Data
{
    /// <summary>
    /// KeyValue Dictionary Helper
    /// </summary>
    /// <typeparam name="TK"></typeparam>
    /// <typeparam name="TV"></typeparam>
    [Serializable]
    public sealed class KeyValueDictionary<TK, TV> : ISerializationCallbackReceiver, IEnumerable<KeyValue<TK, TV>>
    {
// #if UNITY_EDITOR
//
//         [HideInInspector] [SerializeField] private TK _keyType;
//         //  [HideInInspector] [SerializeField] private TV _valueType;
//
// #endif

        [NonSerialized] private bool _disableDictUpdate;

        private Dictionary<TK, TV> _dictionary = new Dictionary<TK, TV>(8);

        [NonReorderable] [SerializeField] private List<KeyValue<TK, TV>> _keyValues;

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            if (!_disableDictUpdate)
            {
                KeyValue<TK, TV>.FillDictionary(_keyValues, ref _dictionary);
            }

            _disableDictUpdate = false;
        }

        public bool Remove(TK key)
        {
            if (!_dictionary.Remove(key)) return false;
            _disableDictUpdate = true;
            KeyValue<TK, TV>.FillList(_dictionary, ref _keyValues);
            return true;
        }

        public TV this[TK key]
        {
            get
            {
                try
                {
                    return _dictionary[key];
                }
                catch (Exception e)
                {
                    Debug.LogError($"KeyValueDictionary {e.Message}");
                }

                return default;
            }
            set => _dictionary[key] = value;
        }

        public int Count => _keyValues.Count;

        public bool ContainsKey(TK key)
        {
            return _dictionary.ContainsKey(key);
        }

        public bool TryGetValue(TK key, out TV value)
        {
            return _dictionary.TryGetValue(key, out value);
        }

        public void Update(TK key, TV value)
        {
            if (!_dictionary.ContainsKey(key))
            {
                _disableDictUpdate = false;
                _keyValues.Add(new KeyValue<TK, TV>(key, value));
            }
            else
            {
                _disableDictUpdate = true;
                _dictionary[key] = value;
                KeyValue<TK, TV>.FillList(_dictionary, ref _keyValues);
            }
        }

        public bool TryAdd(TK key, TV value)
        {
            if (!_dictionary.TryAdd(key, value)) return true;
            _disableDictUpdate = true;
            _keyValues.Add(new KeyValue<TK, TV>(key, value));

            return true;
        }

        public void Add(TK key, TV value)
        {
            _dictionary.Add(key, value);
            _disableDictUpdate = true;
            _keyValues.Add(new KeyValue<TK, TV>(key, value));
        }

        public void Clear()
        {
            _disableDictUpdate = false;
            //_dictionary.Clear();
            _keyValues.Clear();
        }

        public IEnumerator<KeyValue<TK, TV>> GetEnumerator()
        {
            foreach (var keyValue in _keyValues)
            {
                yield return keyValue;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}