using System;
using UnityEngine;
using UnityEngine.Pool;

namespace Gameready.Utils.Pool
{
    public interface IPooledItem
    {
        event Action<IPooledItem> OnReleased;

        void Release();
    }

    [Serializable]
    public abstract class BaseObjectPoolGeneric<T> : ISerializationCallbackReceiver where T : class,IPooledItem
    {
        [SerializeField] private int _poolCapacity = 10;

        private ObjectPool<T> _pool;

        protected BaseObjectPoolGeneric()
        {
            _pool = new ObjectPool<T>(CreateItem, GetItem, ReleaseItem, DestroyItem);
        }

        public virtual void Clear()
        {
            _pool.Clear();
        }
        
        protected abstract void ReleaseItem(T obj);

        protected abstract void GetItem(T obj);

        protected abstract T CreateItem();

        public void Release(T obj)
        {
            _pool.Release(obj);
        }

        protected T GetOrCreate()
        {
            return _pool.Get();
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            _pool?.Clear();
            _pool?.Dispose();

            _pool = new ObjectPool<T>(CreateItem, GetItem, ReleaseItem, DestroyItem, true, _poolCapacity);

            AfterDeserialize();
        }

        protected virtual void DestroyItem(T item)
        {
        }

        protected virtual void AfterDeserialize()
        {
        }
    }
}