using System;
using UnityEngine;

namespace Gameready.Utils.ItemsSystem
{
    /// <summary>
    /// https://docs.unity3d.com/ScriptReference/Hash128.html
    /// </summary>
    [Serializable]
    public struct HashItemId : IEquatable<HashItemId>
    {
        public static readonly HashItemId EmptyItemId = new("EmptyId", $"{nameof(HashItemId)}");

        [SerializeField] private string _name;

        [SerializeField] private string _storageName;

        [SerializeField] private Hash128 _hash;

        public string Name => _name;

        public string StorageName => _storageName;

        private HashItemId(string name, string storageName)
        {
            _storageName = storageName;
            _name = name;
            _hash = Hash128.Compute($"{storageName}.{name}");
        }

        

        public static HashItemId Create(string name, string storageName)
        {
            return string.IsNullOrEmpty(name) && string.IsNullOrEmpty(storageName)
                ? EmptyItemId
                : new HashItemId(name, storageName);
        }


        public static HashItemId Create<T>(string name, string storageName, params T[] elements) where T : struct
        {
            if (string.IsNullOrEmpty(name) && string.IsNullOrEmpty(storageName)) return EmptyItemId;
            var hashItemId = new HashItemId(name, storageName);
            hashItemId._hash.Append(elements);
            return hashItemId;
        }

        public override string ToString()
        {
            return $"{_storageName}.{_name}";
        }

        public bool Equals(HashItemId other)
        {
            return _hash.Equals(other._hash);
        }

        public override bool Equals(object obj)
        {
            return obj is HashItemId other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _hash.GetHashCode();
        }

        public static bool operator ==(HashItemId left, HashItemId right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(HashItemId left, HashItemId right)
        {
            return !left.Equals(right);
        }
    }
}