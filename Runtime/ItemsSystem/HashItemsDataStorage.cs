using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gameready.Utils.ItemsSystem
{
    public sealed class HashItemsDataStorage : ScriptableObject
    {
        [SerializeField] private List<HashItemId> _items = new List<HashItemId>();

        public HashItemId GetItem(string itemName)
        {
            foreach (var itemId in _items)
            {
                if (itemId.Name == itemName)
                {
                    return itemId;
                }
            }

            return HashItemId.EmptyItemId;
        }


        public void SortByName()
        {
            _items.Sort((id, itemId) => string.Compare(id.Name, itemId.Name, StringComparison.Ordinal));
        }

        public IReadOnlyList<HashItemId> Items => _items;
    }
}