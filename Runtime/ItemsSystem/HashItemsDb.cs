using System.Collections.Generic;
using System.Linq;
using Gameready.Utils.Attributes;
using UnityEngine;

namespace Gameready.Utils.ItemsSystem
{
    /// <summary>
    /// Main Storage for all hash items
    /// helper for PropertyDrawers and Editors
    /// </summary>
    public sealed class HashItemsDb : ScriptableObject
    {
        [SerializeField] private List<HashItemsDataStorage> _itemsDataStorage = new List<HashItemsDataStorage>();

        public IReadOnlyList<HashItemsDataStorage> ItemsDataStorage => _itemsDataStorage;


        public IReadOnlyList<HashItemId> GetHashItemsFromStorage(string storageName)
        {
            if (string.IsNullOrEmpty(storageName)) return GetAllHashItems();
            return _itemsDataStorage.Find(storage => storage.name == storageName)?.Items ?? new List<HashItemId>();
        }

        public IReadOnlyList<HashItemId> GetAllHashItems()
        {
            return _itemsDataStorage.SelectMany(storage => storage.Items).ToList().AsReadOnly();
        }

#if UNITY_EDITOR
        [Button("Validate And Save", ButtonInvokeType.InvokeInPrefabSceneModeWithSceneDirty)]
        private void Validate()
        {
            _itemsDataStorage = ItemsDataStorage.Distinct().ToList();

            for (int i = 0; i < ItemsDataStorage.Count; i++)
            {
                if (ItemsDataStorage[i] == null)
                {
                    _itemsDataStorage.RemoveAt(i);
                    i--;
                }
            }

            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
        }
#endif
    }
}