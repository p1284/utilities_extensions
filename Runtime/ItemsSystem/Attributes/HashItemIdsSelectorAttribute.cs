using System;
using UnityEngine;

namespace Gameready.Utils.ItemsSystem.Attributes
{
    [AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public class HashItemIdsSelectorAttribute : PropertyAttribute
    {
        public readonly string StorageName;

        public HashItemIdsSelectorAttribute(string storageName)
        {
            StorageName = storageName;
        }

        public HashItemIdsSelectorAttribute()
        {
            StorageName = string.Empty;
        }
    }
}