using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Gameready.Utils.Data;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Gameready.Utils
{
    public interface IBaseUpdatable
    {
    }

    public interface ILateUpdate : IBaseUpdatable
    {
        void OnLateUpdate();
    }

    public interface IUpdate : IBaseUpdatable
    {
        void OnUpdate();
    }

    public interface IFixedUpdate : IBaseUpdatable
    {
        void OnFixedUpdate();
    }

    public interface ILateFixedUpdate : IBaseUpdatable
    {
        void OnLateFixedUpdate();
    }

    public interface IPauseFocusChanged
    {
        void OnPaused(bool paused);

        void OnFocus(bool focused);
    }


    public class MonoInstance : MonoBehaviour
    {
        private bool _paused;
        private HashSet<IPauseFocusChanged> _pauseFocusListeners;

        private FastRemoveList<IUpdate> _updates;
        private FastRemoveList<ILateUpdate> _lateUpdates;
        private FastRemoveList<IFixedUpdate> _fixedUpdaters;
        private FastRemoveList<ILateFixedUpdate> _fixedLateUpdaters;

        private bool _isLateFixedUpdateRunning;
        private WaitForFixedUpdate waitForFixedUpdate;

        public bool Paused => _paused;

        public void AddPauseFocusListener(IPauseFocusChanged handler)
        {
            _pauseFocusListeners ??= new HashSet<IPauseFocusChanged>();
            _pauseFocusListeners.Add(handler);
        }

        public void RemovePauseFocusListeners(IPauseFocusChanged handler)
        {
            _pauseFocusListeners ??= new HashSet<IPauseFocusChanged>();
            if (_pauseFocusListeners.Contains(handler))
            {
                _pauseFocusListeners.Remove(handler);
            }
        }

        public void AddToUpdate(IBaseUpdatable updateable)
        {
            if (updateable == null) return;

            if (updateable is IUpdate update)
            {
                _updates ??= new FastRemoveList<IUpdate>();
                _updates.Add(update);
            }

            if (updateable is ILateUpdate lateUpdate)
            {
                _lateUpdates ??= new FastRemoveList<ILateUpdate>();
                _lateUpdates.Add(lateUpdate);
            }

            if (updateable is IFixedUpdate fixedUpdate)
            {
                _fixedUpdaters ??= new FastRemoveList<IFixedUpdate>();
                _fixedUpdaters.Add(fixedUpdate);
            }

            if (updateable is ILateFixedUpdate lateFixedUpdate)
            {
                _fixedLateUpdaters ??= new FastRemoveList<ILateFixedUpdate>();
                _fixedLateUpdaters.Add(lateFixedUpdate);

                if (!_isLateFixedUpdateRunning)
                {
                    _isLateFixedUpdateRunning = true;
                    StartCoroutine(LateFixedUpdate());
                }
            }
        }


        public void RemoveFromUpdate(IBaseUpdatable updateable)
        {
            if (updateable == null) return;

            if (updateable is IUpdate update)
            {
                _updates ??= new FastRemoveList<IUpdate>();
                _updates.Remove(update);
            }

            if (updateable is ILateUpdate lateUpdate)
            {
                _lateUpdates ??= new FastRemoveList<ILateUpdate>();
                _lateUpdates.Remove(lateUpdate);
            }

            if (updateable is IFixedUpdate fixedUpdate)
            {
                _fixedUpdaters ??= new FastRemoveList<IFixedUpdate>();
                _fixedUpdaters.Remove(fixedUpdate);
            }

            if (updateable is ILateFixedUpdate lateFixedUpdate)
            {
                _fixedLateUpdaters ??= new FastRemoveList<ILateFixedUpdate>();
                _fixedLateUpdaters.Remove(lateFixedUpdate);
            }
        }

        private void Awake()
        {
            waitForFixedUpdate = new WaitForFixedUpdate();
            DontDestroyOnLoad(gameObject);
        }

        private void OnApplicationQuit()
        {
            Debug.Log("On App Quit");
        }

        private void OnDestroy()
        {
            _updates?.Clear();
            _lateUpdates?.Clear();
            _fixedUpdaters?.Clear();
            _fixedLateUpdaters?.Clear();
            _pauseFocusListeners?.Clear();
        }

#if UNITY_ANDROID || UNITY_EDITOR || UNITY_STANDALONE
        void OnApplicationPause(bool pauseStatus)
        {
            _paused = pauseStatus;
            if (_pauseFocusListeners != null)
            {
                _pauseFocusListeners.RemoveWhere(p => p == null);

                foreach (var listener in _pauseFocusListeners)
                {
                    try
                    {
                        listener.OnPaused(pauseStatus);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
            }


#if !UNITY_EDITOR
        Debug.Log("Application pause changed: " + _paused);
#endif
        }

#endif

#if UNITY_IOS || UNITY_EDITOR || UNITY_STANDALONE
        void OnApplicationFocus(bool focused)
        {
            _paused = !focused;
            if (_pauseFocusListeners != null)
            {
                _pauseFocusListeners.RemoveWhere(p => p == null);

                foreach (var listener in _pauseFocusListeners)
                {
                    try
                    {
                        listener.OnFocus(focused);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
            }


#if !UNITY_EDITOR
        Debug.Log("Application focus changed: " + _paused);
#endif
        }
#endif

        private IEnumerator LateFixedUpdate()
        {
            while (_isLateFixedUpdateRunning)
            {
                yield return waitForFixedUpdate;

#if SAFE_UPDATES
                foreach (var fixedLateUpdater in _fixedLateUpdaters)
                {
                    try
                    {
                        fixedLateUpdater.OnLateFixedUpdate();
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
#else
                foreach (var fixedLateUpdater in _fixedLateUpdaters)
                {
                    fixedLateUpdater.OnLateFixedUpdate();
                }
#endif

                _isLateFixedUpdateRunning = _fixedLateUpdaters.Count > 0;
            }
        }


        private void Update()
        {
            if (_updates == null) return;
#if SAFE_UPDATES
            foreach (var updateListener in _updates)
            {
                try
                {
                    updateListener.OnUpdate();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
#else
            foreach (var updateListener in _updates)
            {
                updateListener.OnUpdate();
            }
#endif
        }

        private void LateUpdate()
        {
            if (_lateUpdates == null) return;
#if SAFE_UPDATES
            foreach (var lateUpdateListener in _lateUpdates)
            {
                try
                {
                    lateUpdateListener.OnLateUpdate();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
#else
            foreach (var lateUpdateListener in _lateUpdates)
            {
                lateUpdateListener.OnLateUpdate();
            }
#endif
        }

        private void FixedUpdate()
        {
            if (_fixedUpdaters == null) return;

#if SAFE_UPDATES
            foreach (var fixedUpdate in _fixedUpdaters)
            {
                try
                {
                    fixedUpdate.OnFixedUpdate();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
#else
            foreach (var fixedUpdate in _fixedUpdaters)
            {
                fixedUpdate.OnFixedUpdate();
            }
#endif
        }

        public IEnumerator RunThrowingIterator(
            IEnumerator enumerator,
            Action<Exception> done
        )
        {
            while (true)
            {
                object current;
                try
                {
                    if (enumerator.MoveNext() == false)
                    {
                        break;
                    }

                    current = enumerator.Current;
                }
                catch (Exception ex)
                {
                    done(ex);
                    yield break;
                }

                yield return current;
            }

            done(null);
        }
    }


    public static class MonoHelper
    {
#if UNITY_EDITOR

        public const string AutoInitDefine = "MONOHELPER_AUTOINIT";
        public const string SafeUpdatersDefine = "SAFE_UPDATES";

#endif


        private static MonoInstance _instance;

        private static bool _isInitialized;

#if MONOHELPER_AUTOINIT
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
#endif
        public static void Initialize()
        {
            if (!_isInitialized && _instance == null)
            {
                _isInitialized = true;
                _instance = Object.FindObjectOfType<MonoInstance>();
                _instance = _instance != null
                    ? _instance
                    : new GameObject("MonoGameHelper").AddComponent<MonoInstance>();
                Debug.Log("MonoHelper Initialized!");
            }
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void ResetDomainReload()
        {
            _isInitialized = false;
            _instance = null;
        }

        public static bool IsPaused => IsInitialized() && _instance.Paused;

        public static void AddPauseFocusListener(IPauseFocusChanged pauseFocusChanged)
        {
            if (IsInitialized())
            {
                _instance.AddPauseFocusListener(pauseFocusChanged);
            }
        }

        public static void RemovePauseFocusListener(IPauseFocusChanged pauseFocusChanged)
        {
            if (IsInitialized())
            {
                _instance.RemovePauseFocusListeners(pauseFocusChanged);
            }
        }

        public static void AddOnUpdate(IBaseUpdatable update)
        {
            if (IsInitialized())
            {
                _instance.AddToUpdate(update);
            }
        }

        public static void RemoveFromUpdate(IBaseUpdatable update)
        {
            if (IsInitialized())
            {
                _instance.RemoveFromUpdate(update);
            }
        }

        public static void StartSafeCoroutine(IEnumerator coroutine, Action<Exception> callback)
        {
            if (IsInitialized())
            {
                _instance.StartCoroutine(_instance.RunThrowingIterator(coroutine, callback));
            }
        }

        public static void StopAllCoroutines()
        {
            if (_instance != null)
            {
                _instance.StopAllCoroutines();
            }
        }

        public static void StartCoroutine(IEnumerator coroutine)
        {
            if (IsInitialized())
            {
                _instance.StartCoroutine(coroutine);
            }
        }

        public static void StartCoroutine(IEnumerator coroutine, out Coroutine output)
        {
            output = null;
            if (IsInitialized())
            {
                output = _instance.StartCoroutine(coroutine);
            }
        }

        public static void StopCoroutine(Coroutine coroutine)
        {
            if (_instance != null && coroutine != null)
                _instance.StopCoroutine(coroutine);
        }

        public static void StopCoroutine(string methodName)
        {
            if (_instance != null)
                _instance.StopCoroutine(methodName);
        }

        public static void StopCoroutine(IEnumerator coroutine)
        {
            if (_instance != null)
            {
                _instance.StopCoroutine(coroutine);
            }
        }

        public static void StartSafeCoroutineEx(this MonoBehaviour behavior, IEnumerator coroutine,
            Action<Exception> onError)
        {
            if (IsInitialized())
            {
                behavior.StartCoroutine(_instance.RunThrowingIterator(coroutine, onError));
            }
        }

        private static bool IsInitialized()
        {
            if (_isInitialized && _instance != null) return true;
            #if SAFE_UPDATES
            Debug.LogWarning("MonoHelper not Initialized with safe updaters!");
            #else
            Debug.LogWarning("MonoHelper not Initialized!");
            #endif
            
            return false;
        }
    }
}