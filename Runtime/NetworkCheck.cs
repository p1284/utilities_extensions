using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Gameready.Utils
{
    public enum InternetConnection
    {
        Active = 1,
        NotAvailable = 0
    }

    public static class NetworkCheck
    {
        public static event Action OnNetworkStateChanged;

        private static CancellationTokenSource _monitorToken;

        public static InternetConnection LastConnectionStatus { get; private set; } = InternetConnection.Active;


        static NetworkCheck()
        {
            CheckConnection();
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void ReloadDomainDisabled()
        {
            OnNetworkStateChanged.RemoveAll();
            if (_monitorToken != null)
            {
                _monitorToken.Cancel(false);
                _monitorToken = null;
            }
        }

        public static void StartMonitor(int delay = 5)
        {
            StopMonitor();

            _monitorToken = new CancellationTokenSource();

            StartMonitorNetwork(delay, _monitorToken.Token).Forget();
        }

        public static void StopMonitor()
        {
            if (_monitorToken != null)
            {
                _monitorToken.Cancel(false);
            }

            CheckConnection();
        }

        public static void Ping(Action callback = null, string pingUrl = "https://www.google.com/")
        {
            StartPing(callback, pingUrl).Forget();
        }

        public static void CheckConnection()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                LastConnectionStatus = InternetConnection.NotAvailable;
            }
            else
            {
                LastConnectionStatus = InternetConnection.Active;
            }

            OnNetworkStateChanged?.Invoke();
        }

        private static async UniTaskVoid StartMonitorNetwork(int delay, CancellationToken token)
        {
            var monitor = true;
            while (monitor)
            {
                CheckConnection();

                if (_monitorToken.IsCancellationRequested)
                {
                    monitor = false;
                }

                await UniTask.Delay(TimeSpan.FromSeconds(delay), DelayType.UnscaledDeltaTime, PlayerLoopTiming.Update,
                    token);
            }

            _monitorToken.Dispose();
            _monitorToken = null;
        }

        private static async UniTaskVoid StartPing(Action callback, string pingUrl)
        {
            if (!string.IsNullOrEmpty(pingUrl))
            {
                UnityWebRequest request = new UnityWebRequest(pingUrl);
                var result = await request.SendWebRequest();
                if (result.result == UnityWebRequest.Result.Success)
                {
                    LastConnectionStatus = InternetConnection.Active;
                }
                else
                {
                    LastConnectionStatus = InternetConnection.NotAvailable;
                }
            }
            else
            {
                CheckConnection();
            }

            callback?.Invoke();
            OnNetworkStateChanged?.Invoke();
        }
    }
}