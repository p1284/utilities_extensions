using System;
using Gameready.Random;
using UnityEngine;

namespace Gameready.Gameplay.LootDropSystem
{
    [Serializable]
    public class LootDropItem : GenericLootDropItem
    {
        public int id;
    }

    [Serializable]
    public class LootDropTableTemplate : GenericLootDropTable<LootDropItem>
    {
    }

    public class LootDropTable : MonoBehaviour
    {
        [SerializeField] LootDropTableTemplate _lootDropTable;

        [SerializeField] private int _lastDroppedId;

        [SerializeField] private GameRandomType _randomType;

        public int LastDroppedId => _lastDroppedId;

        private void Awake()
        {
            _lootDropTable.CreateRandomGenerator(GameRandom.GenerateUniqueSeed(), _randomType);
        }

        public void SetupSeed(int seed)
        {
            _lootDropTable.CreateRandomGenerator(seed, _randomType);
        }

        public void AddLootItem(int id, float weight)
        {
            _lootDropTable.lootDropItems.Add(new LootDropItem()
            {
                id = id,
                probabilityWeight = weight
            });
        }

        public void ClearDropTable()
        {
            _lootDropTable.lootDropItems.Clear();
        }

        public void RecalculateWeights()
        {
            _lootDropTable.RestoreAndClearCachedWeights();
            _lootDropTable.ValidateTable();
        }

        public int DropMaxDropItem()
        {
            var id = GetMaxDrop();
            foreach (var lootDropItem in _lootDropTable.lootDropItems)
            {
                if (lootDropItem.id == id)
                {
                    lootDropItem.probabilityWeight = 0f;
                }
            }

            _lastDroppedId = id;
            _lootDropTable.ValidateTable();
            return id;
        }

        public int DropItem()
        {
            var item = _lootDropTable.PickDropItemOnce();
            _lastDroppedId = item.id;
            return item.id;
        }

        public int GetMinDrop()
        {
            if (_lootDropTable.lootDropItems.Count == 0) return -1;
            return _lootDropTable.GetMinProbabilityItem().id;
        }

        public int GetMaxDrop()
        {
            if (_lootDropTable.lootDropItems.Count == 0) return -1;
            return _lootDropTable.GetMaxProbabilityItem().id;
        }

        private void OnValidate()
        {
            _lootDropTable?.ValidateTable();
        }
    }
}