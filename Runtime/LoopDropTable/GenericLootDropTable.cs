using System;
using System.Collections.Generic;
using System.Linq;
using Gameready.Random;
using UnityEngine;

namespace Gameready.Gameplay.LootDropSystem
{
    /// <summary>
    /// Class serves for assigning and picking loot drop items.
    /// </summary>
    [Serializable]
    public class GenericLootDropTable<T> where T : GenericLootDropItem
    {
        public List<T> lootDropItems;

        [SerializeField] private string _lastDropPercent;

        private Dictionary<T, float> _cachedWeghts;

        // List where we'll assign the items.
        // Sum of all weights of items.
        float probabilityTotalWeight;

        private IGameRandomGenerator _randomGenerator;

        public void CreateRandomGenerator(GameRandomType randomType)
        {
            _randomGenerator = GameRandom.CreateRandomGenerator(randomType);
            Debug.Log($"LootDropTable setup new RandomGenerator with:{randomType}");
        }
        
        public void CreateRandomGenerator(int seed, GameRandomType randomType = GameRandomType.System_Random)
        {
            _randomGenerator = GameRandom.CreateRandomGenerator(randomType, seed);
            Debug.Log($"LootDropTable setup new random generator:{randomType} seed{seed}");
        }
        
        public string LastDropPercent => _lastDropPercent;

        /// <summary>
        /// Calculates the percentage and asigns the probabilities how many times
        /// the items can be picked. Function used also to validate data when tweaking numbers in editor.
        /// </summary>	
        public void ValidateTable()
        {
            // Prevent editor from "crying" when the item list is empty :)
            if (lootDropItems != null && lootDropItems.Count > 0)
            {
                float currentProbabilityWeightMaximum = 0f;

                // Sets the weight ranges of the selected items.
                foreach (T lootDropItem in lootDropItems)
                {
                    if (lootDropItem.probabilityWeight < 0f)
                    {
                        // Prevent usage of negative weight.
                        Debug.Log("You can't have negative weight on an item. Reseting item's weight to 0.");
                        lootDropItem.probabilityWeight = 0f;
                    }
                    else
                    {
                        lootDropItem.ProbabilityRangeFrom = currentProbabilityWeightMaximum;
                        currentProbabilityWeightMaximum += lootDropItem.probabilityWeight;
                        lootDropItem.ProbabilityRangeTo = currentProbabilityWeightMaximum;
                    }
                }

                probabilityTotalWeight = currentProbabilityWeightMaximum;

                // Calculate percentage of item drop select rate.
                foreach (T lootDropItem in lootDropItems)
                {
                    lootDropItem.ProbabilityPercent = ((lootDropItem.probabilityWeight) / probabilityTotalWeight) * 100;
                }
            }
        }

        public void RestoreAndClearCachedWeights()
        {
            if (_cachedWeghts == null) return;
            foreach (var lootDropItem in lootDropItems)
            {
                if (_cachedWeghts.TryGetValue(lootDropItem, out var weght))
                {
                    lootDropItem.probabilityWeight = weght;
                }
            }

            _cachedWeghts.Clear();
        }

        public T GetMinProbabilityItem()
        {
            return lootDropItems.OrderBy(item => item.probabilityWeight).First();
        }

        public T GetMaxProbabilityItem()
        {
            return lootDropItems.OrderByDescending(item => item.probabilityWeight).First();
        }

        public T PickDropItemOnce()
        {
            var dropItem = PickLootDropItem();
            _cachedWeghts ??= new Dictionary<T, float>();
            _cachedWeghts.TryAdd(dropItem, dropItem.probabilityWeight);

            dropItem.probabilityWeight = 0f;

            ValidateTable();

            return dropItem;
        }

        /// <summary>
        /// Picks and returns the loot drop item based on it's probability.
        /// </summary>
        public T PickLootDropItem()
        {
            var value = _randomGenerator.Value;
            float pickedNumber = value * probabilityTotalWeight;

            _lastDropPercent = $"{Mathf.Floor((pickedNumber / probabilityTotalWeight) * 100f)}%";

            // Find an item whose range contains pickedNumber
            foreach (T lootDropItem in lootDropItems)
            {
                // If the picked number matches the item's range, return item
                if (pickedNumber > lootDropItem.ProbabilityRangeFrom && pickedNumber < lootDropItem.ProbabilityRangeTo)
                {
                    return lootDropItem;
                }
            }

            // If item wasn't picked... Notify programmer via console and return the first item from the list
            Debug.LogError(
                "Item couldn't be picked... Be sure that all of your active loot drop tables have assigned at least one item!");
            return lootDropItems[0];
        }
    }
}