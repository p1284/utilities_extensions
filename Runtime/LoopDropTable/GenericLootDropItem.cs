using System;
using UnityEngine;

namespace Gameready.Gameplay.LootDropSystem
{
    /// <summary>
    /// Item that can be picked by a LootDropTable.
    /// </summary>
    [Serializable]
    public class GenericLootDropItem
    {
        // How many units the item takes - more units, higher chance of being picked
        public float probabilityWeight;
        
        [Tooltip("Displayed only as an information for the designer/programmer. Should not be set manually via inspector!")]   
        public float ProbabilityPercent;
        // These values are assigned via LootDropTable script. They represent from which number to which number if selected, the item will be picked.
        [NonSerialized] 
        public float ProbabilityRangeFrom;
        [NonSerialized] 
        public float ProbabilityRangeTo;
        
    }
}
