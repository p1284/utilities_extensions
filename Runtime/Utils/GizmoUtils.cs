using UnityEngine;

namespace Gameready.Utils
{
    /// <summary>
    /// From ObiSoftBody plugin
    /// </summary>
    public static class GizmoUtils
    {
        public static void DrawArrowGizmo(Transform from, float bodyLenght, float bodyWidth, float headLenght,
            float headWidth)
        {
            if (from == null) return;
            
            float halfBodyLenght = bodyLenght * 0.5f;
            float halfBodyWidth = bodyWidth * 0.5f;

            // arrow body:
            Gizmos.DrawLine(from.position + new Vector3(halfBodyWidth, 0, -halfBodyLenght),
                from.position + new Vector3(halfBodyWidth, 0, halfBodyLenght));
            Gizmos.DrawLine(from.position + new Vector3(-halfBodyWidth, 0, -halfBodyLenght),
                from.position + new Vector3(-halfBodyWidth, 0, halfBodyLenght));
            Gizmos.DrawLine(from.position + new Vector3(-halfBodyWidth, 0, -halfBodyLenght),
                from.position + new Vector3(halfBodyWidth, 0, -halfBodyLenght));

            // arrow head:
            Gizmos.DrawLine(from.position + new Vector3(halfBodyWidth, 0, halfBodyLenght),
                from.position + new Vector3(headWidth, 0, halfBodyLenght));
            Gizmos.DrawLine(from.position + new Vector3(-halfBodyWidth, 0, halfBodyLenght),
                from.position + new Vector3(-headWidth, 0, halfBodyLenght));
            Gizmos.DrawLine(from.position + new Vector3(0, 0, halfBodyLenght + headLenght),
                from.position + new Vector3(headWidth, 0, halfBodyLenght));
            Gizmos.DrawLine(from.position + new Vector3(0, 0, halfBodyLenght + headLenght),
                from.position + new Vector3(-headWidth, 0, halfBodyLenght));
        }

        public static void DebugDrawCross(Vector3 pos, float size, Color color)
        {
            Debug.DrawLine(pos - Vector3.right * size, pos + Vector3.right * size, color);
            Debug.DrawLine(pos - Vector3.up * size, pos + Vector3.up * size, color);
            Debug.DrawLine(pos - Vector3.forward * size, pos + Vector3.forward * size, color);
        }
    }
}