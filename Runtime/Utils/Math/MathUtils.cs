﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Gameready.Utils
{
    public static partial class MathUtils
    {
       
        public static Vector3 AbsVector(Vector3 source)
        {
            source.x = Math.Abs(source.x);
            source.y = Math.Abs(source.y);
            source.z = Math.Abs(source.z);
            return source;
        }
        
        public static int Abs(int value)
        {
            return (value ^ (value >> 31)) - (value >> 31);
        }
        
        public static int Repeat(int index, int len)
        {
            return index >= len ? 0 : index;
        }

        public static float SubtractPercent(float targetValue, float percent)
        {
            return targetValue - CalcPercentValue(targetValue, percent);
        }

        public static float AddPercent(float targetValue, float percent)
        {
            return targetValue + CalcPercentValue(targetValue, percent);
        }

        public static float CalcPercentValue(float currentValue, float percent)
        {
            return (currentValue * percent) * 0.01f;
        }

        public static float GetPercent(int currentValue, float maxValue)
        {
            if (maxValue == 0 && currentValue != 0) return 0f;
            return (currentValue / maxValue) * 100f;
        }

        public static float CalcPercent(float targetValue)
        {
            return targetValue / 100f;
        }
        
        public static float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360F)
                angle += 360F;
            if (angle > 360F)
                angle -= 360F;
            return Mathf.Clamp(angle, min, max);
        }

        /// <summary>
        /// Re-maps a number from one range to another.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="from1"></param>
        /// <param name="to1"></param>
        /// <param name="from2"></param>
        /// <param name="to2"></param>
        /// <returns></returns>
        public static float Remap(float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }

        /// <summary>
        /// Remap _Value from [_MinIn;_MaxIn] to [_MinOut;_MaxOut] with a step of _Step
        /// </summary>
        /// <returns>The remaped value.</returns>
        /// <param name="_Value">Value.</param>
        /// <param name="_MinIn">Minimum in.</param>
        /// <param name="_MaxIn">Max in.</param>
        /// <param name="_MinOut">Minimum out.</param>
        /// <param name="_MaxOut">Max out.</param>
        /// <param name="_Step">Step.</param>
        public static float Remap(float _Value, float _MinIn, float _MaxIn, float _MinOut, float _MaxOut, float _Step)
        {
            return _MinOut + Mathf.FloorToInt((_Value - _MinIn) * (_MaxOut - _MinOut) / ((_MaxIn - _MinIn) * _Step)) *
                _Step;
        }
    }
}