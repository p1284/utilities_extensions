using System;
using UnityEngine;

namespace Gameready.Utils
{
    public static partial class MathUtils
    {
        public const float SquareOfTwo = 1.4142f;

        public static int GetGridItemIndex(int x, int y, int z, int gridWidth, int gridHeight)
        {
            return (z * gridWidth * gridHeight) + (y * gridWidth) + x;
        }

        public static Vector3Int GetGridItemPos(int index, int gridWidth, int gridHeight)
        {
            int z = index / (gridWidth * gridHeight);
            index -= (z * gridHeight * gridWidth);
            int y = index / gridWidth;
            int x = index % gridWidth;

            return new Vector3Int(x, y, z);
        }
        
        public static int GetGridItemIndex(int x, int y, int gridWidth)
        {
            return y * gridWidth + x;
        }

        public static Vector2Int GetGridItemPos(int index, int gridWidth)
        {
            return new Vector2Int(index % gridWidth, index / gridWidth);
        }


        public static bool GridTileInsideCircle(Vector2Int center, Vector2Int tile, int radius)
        {
            float dist_squared = (center - tile).sqrMagnitude;
            return dist_squared <= radius * radius;
        }

        public static void GetGridTilesByRadius(Vector2Int center, int radius, Action<Vector2Int> getTile)
        {
            int top = Mathf.CeilToInt(center.y - radius);
            int bottom = Mathf.FloorToInt(center.y + radius);
            int left = Mathf.CeilToInt(center.x - radius);
            int right = Mathf.FloorToInt(center.x + radius);

            for (int y = top; y <= bottom; y++)
            {
                for (int x = left; x <= right; x++)
                {
                    if (GridTileInsideCircle(center, new Vector2Int(x, y), radius))
                    {
                        getTile(new Vector2Int(x, y));
                    }
                }
            }
        }

        public static Vector2 GetSquareDirection(Vector2 joystickDir)
        {
            var xDir = Mathf.Clamp(joystickDir.x, -1f, 1f);
            var yDir = Mathf.Clamp(joystickDir.y, -1f, 1f);
            return new Vector2(0.5f * Mathf.Sqrt(2 + (xDir * xDir) - (yDir * yDir) + 2 * xDir * SquareOfTwo) -
                               0.5f * Mathf.Sqrt(2 + (xDir * xDir) - (yDir * yDir) - 2 * xDir * SquareOfTwo),
                0.5f * Mathf.Sqrt(2 - (xDir * xDir) + (yDir * yDir) + 2 * yDir * SquareOfTwo) -
                0.5f * Mathf.Sqrt(2 - (xDir * xDir) + (yDir * yDir) - 2 * yDir * SquareOfTwo));
        }
    }
}