using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Gameready.Utils
{
    public static class ReflectionUtils
    {
        public static bool IsAsyncMethod(this MethodInfo methodInfo)
        {
            Type stateMachineAttribute = typeof(AsyncStateMachineAttribute);

            var attribute = methodInfo.GetCustomAttribute(stateMachineAttribute, true) as AsyncStateMachineAttribute;
            return attribute != null;
        }

        public static bool IsCoroutineMethod(this MethodInfo methodInfo)
        {
            return methodInfo.ReturnType == typeof(IEnumerator);
        }


        public static string[] GetConstantsArray<T>() where T : class, new()
        {
            return new T().GetType()
                .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly && fi.FieldType == typeof(string))
                .Select(x => (string)x.GetRawConstantValue())
                .ToArray();
        }
    }
}