using System.Collections.Generic;
using Gameready.Random;
using Gameready.Utils.Data;

namespace Gameready.Data
{
    public sealed class UniqueRandomRange
    {
        private readonly int[] _counters;

        public readonly int MIN;
        public readonly int MAX;

        private readonly List<int> _takenIndexes;

        private int _lastIndex = -1;
        
        public UniqueRandomRange(int min, int max)
        {
            MIN = min;
            MAX = max;
            _takenIndexes = new List<int>(max);
            _counters = new int[max];
            Reset();
        }

        public UniqueRandomRange(Range range)
        {
            MIN = range.MinInt;
            MAX = range.MaxInt;
            _takenIndexes = new List<int>(MAX);
            _counters = new int[MAX];
            Reset();
        }
        
        private void Reset()
        {
            for (int i = 0; i < _counters.Length; i++)
            {
                _takenIndexes.Add(i);
                _counters[i] = (i + MIN);
            }
        }

        private int GetRandomIndex()
        {
            var index = GameRandom.Range(0, _takenIndexes.Count);
            _takenIndexes.Remove(index);
            if (_takenIndexes.Count > 0 && _lastIndex == index)
            {
                index = GameRandom.Range(0, _takenIndexes.Count);
                _takenIndexes.Remove(index);
            }

            _lastIndex = index;
            return index;
        }


        public int GetUniqueRandom()
        {
            if (_takenIndexes.Count == 0)
            {
                Reset();
            }

            var index = GetRandomIndex();

            return _counters[index];
        }
    }
}