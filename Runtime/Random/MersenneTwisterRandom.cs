using System.Collections.Generic;

namespace Gameready.Random
{
    /// <summary>
    /// MersenneTwister implementation
    /// </summary>
    internal class MersenneTwisterRandom : IGameRandomGenerator
    {
        private readonly MersenneTwister _random;

        internal MersenneTwisterRandom(int seed)
        {
            _random = new MersenneTwister(seed);
        }

        public MersenneTwisterRandom()
        {
            _random = new MersenneTwister();
        }

        public float Value => (float)_random.NextDouble();

        public int Range(int min, int max)
        {
            return _random.Next(min, max);
        }

        public float Range(float min, float max)
        {
            var value = _random.NextSingle();
            var range = value * (max - min);
            return range + min;
        }

        public bool IsBiggerHalf()
        {
            return _random.NextDouble() > 0.5d;
        }

        public void Shuffle<T>(IList<T> list)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = _random.Next(n + 1);
                (list[k], list[n]) = (list[n], list[k]);
            }
        }

        public T RandomElement<T>(IList<T> list)
        {
            if (list == null || list.Count == 0)
            {
                return default(T);
            }

            var index = _random.Next(0, list.Count);
            return list[index];
        }
    }
}