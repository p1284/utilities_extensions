using System.Collections.Generic;

namespace Gameready.Random
{
    internal sealed class SystemGameRandom : IGameRandomGenerator
    {
        private System.Random _random;

        internal SystemGameRandom(int seed)
        {
            _random = new System.Random(seed);
        }

        internal SystemGameRandom()
        {
            _random = new System.Random();
        }

        public float Value => (float)_random.NextDouble();

        public int Range(int min, int max)
        {
            return _random.Next(min, max);
        }

        public float Range(float min, float max)
        {
            var value = _random.NextDouble();
            var range = (float)value * (max - min);
            return range + min;
        }

        public bool IsBiggerHalf()
        {
            return _random.NextDouble() > 0.5d;
        }

        public void Shuffle<T>(IList<T> list)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = _random.Next(n + 1);
                (list[k], list[n]) = (list[n], list[k]);
            }
        }

        public T RandomElement<T>(IList<T> list)
        {
            if (list == null || list.Count == 0)
            {
                return default(T);
            }

            var index = _random.Next(0, list.Count);
            return list[index];
        }
    }
}