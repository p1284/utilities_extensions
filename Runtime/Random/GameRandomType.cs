namespace Gameready.Random
{
    public enum GameRandomType
    {
        /// <summary>
        /// UnityEngine.Random
        /// </summary>
        Unity_Random = -1,
        /// <summary>
        /// Simple c# Random implementation
        /// https://learn.microsoft.com/en-us/dotnet/api/system.random?view=net-7.0
        /// </summary>
        System_Random = 0,

        /// <summary>
        /// Mersenne Twister implementation
        /// http://www.math.sci.hiroshima-u.ac.jp/m-mat/MT/emt.html
        /// Grabbed from https://github.com/vpmedia/template-unity/blob/master/Framework/Assets/Frameworks/URandom/MersenneTwister.cs
        /// </summary>
        Mersenne_Twister,

        /// <summary>
        /// Get From
        /// https://github.com/igiagkiozis/PCGSharp/blob/master/PCGSharp/Source/PCG.cs
        /// </summary>
        PCG_Random
    }
}