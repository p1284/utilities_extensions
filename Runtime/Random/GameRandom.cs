using System;
using System.Collections.Generic;
using GameReady.Random;

namespace Gameready.Random
{
    /// <summary>
    /// RandomGenerators Wrapper
    /// By Default Use GameRandomType.Unity_Random type
    /// Good to know
    /// https://habr.com/ru/company/vk/blog/574414/
    /// </summary>
    public static class GameRandom
    {
        /// <summary>
        /// Provides a time-dependent seed value, matching the default behavior of System.Random.
        /// </summary>
        public static ulong TimeBasedSeed()
        {
            return (ulong)(Environment.TickCount);
        }

        /// <summary>
        /// Provides a seed based on time and unique GUIDs.
        /// </summary>
        public static ulong GuidBasedSeed()
        {
            ulong upper = (ulong)(Environment.TickCount ^ Guid.NewGuid().GetHashCode()) << 32;
            ulong lower = (ulong)(Environment.TickCount ^ Guid.NewGuid().GetHashCode());
            return (upper | lower);
        }


        public static int GenerateUniqueSeed()
        {
            var seed = Guid.NewGuid().GetHashCode();
            return seed;
        }

        private static IGameRandomGenerator _gameRandomGenerator;

        static GameRandom()
        {
            SetupRandom(GameRandomType.Unity_Random);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="randomType"></param>
        /// <param name="seed"></param>
        /// <returns></returns>
        public static IGameRandomGenerator CreateRandomGenerator(GameRandomType randomType, int? seed = null)
        {
            IGameRandomGenerator gameRandomGenerator = null;
            switch (randomType)
            {
                case GameRandomType.Mersenne_Twister:
                    gameRandomGenerator =
                        seed.HasValue ? new MersenneTwisterRandom(seed.Value) : new MersenneTwisterRandom();
                    break;
                case GameRandomType.PCG_Random:
                    gameRandomGenerator = seed.HasValue ? new PCGGameRandom(seed.Value) : new PCGGameRandom();
                    break;
                case GameRandomType.System_Random:
                    gameRandomGenerator = seed.HasValue ? new SystemGameRandom(seed.Value) : new SystemGameRandom();
                    break;
                default:
                    gameRandomGenerator = seed.HasValue ? new UnityRandom(seed.Value) : new UnityRandom();
                    break; 
            }

            return gameRandomGenerator;
        }

        public static void SetupRandom(GameRandomType randomType, int? seed = null)
        {
            _gameRandomGenerator = null;
            _gameRandomGenerator = CreateRandomGenerator(randomType, seed);
        }

        public static float Value => _gameRandomGenerator.Value;

        public static int Range(int min, int max)
        {
            return _gameRandomGenerator.Range(min, max);
        }

        public static float Range(float min, float max)
        {
            return _gameRandomGenerator.Range(min, max);
        }

        public static bool IsBiggerHalf()
        {
            return _gameRandomGenerator.IsBiggerHalf();
        }

        public static void Shuffle<T>(IList<T> list)
        {
            _gameRandomGenerator.Shuffle(list);
        }

        public static T RandomElement<T>(IList<T> list)
        {
            return _gameRandomGenerator.RandomElement(list);
        }
    }
}