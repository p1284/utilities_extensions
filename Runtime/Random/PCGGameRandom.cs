using System.Collections.Generic;

namespace Gameready.Random
{
    internal class PCGGameRandom : IGameRandomGenerator
    {
        private readonly Pcg _pcg;

        internal PCGGameRandom()
        {
            _pcg = new Pcg();
        }

        internal PCGGameRandom(int seed)
        {
            _pcg = new Pcg(seed);
        }

        public float Value => _pcg.NextFloat();

        public int Range(int min, int max)
        {
            return _pcg.Next(min, max);
        }

        public float Range(float min, float max)
        {
            return _pcg.NextFloat(min, max);
        }

        public bool IsBiggerHalf()
        {
            return _pcg.NextFloat() > 0.5f;
        }

        public void Shuffle<T>(IList<T> list)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = _pcg.Next(n + 1);
                (list[k], list[n]) = (list[n], list[k]);
            }
        }

        public T RandomElement<T>(IList<T> list)
        {
            if (list == null || list.Count == 0)
            {
                return default(T);
            }

            var index = _pcg.Next(0, list.Count);
            return list[index];
        }
    }
}