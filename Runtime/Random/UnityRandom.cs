using System.Collections.Generic;
using Gameready.Random;

namespace GameReady.Random
{
    internal sealed class UnityRandom : IGameRandomGenerator
    {
        public UnityRandom()
        {
        }

        public UnityRandom(int seed)
        {
            UnityEngine.Random.InitState(seed);
        }

        public float Value => UnityEngine.Random.value;

        public int Range(int min, int max)
        {
            return UnityEngine.Random.Range(min, max);
        }

        public float Range(float min, float max)
        {
            return UnityEngine.Random.Range(min, max);
        }

        public bool IsBiggerHalf()
        {
            return Value > 0.5f;
        }

        public void Shuffle<T>(IList<T> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                var tmp = list[i];
                int rIndex = UnityEngine.Random.Range(i, list.Count);
                list[i] = list[rIndex];
                list[rIndex] = tmp;
            }
        }

        public T RandomElement<T>(IList<T> list)
        {
            if (list == null || list.Count == 0)
            {
                return default(T);
            }

            return list[UnityEngine.Random.Range(0, list.Count)];
        }
    }
}