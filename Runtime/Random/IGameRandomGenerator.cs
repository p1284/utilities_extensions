using System.Collections.Generic;

namespace Gameready.Random
{
    public interface IGameRandomGenerator
    {
        float Value { get; }
        
        int Range(int min, int max);

        float Range(float min, float max);

        bool IsBiggerHalf();

        void Shuffle<T>(IList<T> list);

        T RandomElement<T>(IList<T> list);
    }
}