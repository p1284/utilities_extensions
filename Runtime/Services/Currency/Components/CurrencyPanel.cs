using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using VContainer;

namespace GameReady.Currency
{
    public class CurrencyPanel : MonoBehaviour
    {
        [SerializeField] private CurrencyType _currencyType;
        [SerializeField] private TextMeshProUGUI _label;

        private ICurrencyManager _currencyManager;

        public TextMeshProUGUI Label => _label;

        protected CurrencyType CurrencyType
        {
            get => _currencyType;
            set => _currencyType = value;
        }

        [Inject]
        public void Inject(ICurrencyManager currencyManager)
        {
            if (_currencyManager != null) return;
            _currencyManager = currencyManager;
            _label.text = _currencyManager.GetCurrentBalance(_currencyType).ToString("f0");
            _currencyManager.OnBalanceChanged += BalanceConfigServiceChangedHandler;
        }


        private void OnDestroy()
        {
            if (_currencyManager != null)
            {
                _currencyManager.OnBalanceChanged -= BalanceConfigServiceChangedHandler;
            }
        }

        private void BalanceConfigServiceChangedHandler(CurrencyEventData eventData)
        {
            if (eventData.CurrencyType == _currencyType)
            {
                BalanceChanged(eventData);
            }
        }

        protected virtual void BalanceChanged(CurrencyEventData eventData)
        {
            _label.text = eventData.Changed.ToString("f0");
        }
    }
}