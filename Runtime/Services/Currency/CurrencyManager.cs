using System;
using System.Collections.Generic;
using System.Linq;
using GameReady.Saves;
using UnityEngine;
using VContainer.Unity;

namespace GameReady.Currency
{
    public class CurrencyManager : ICurrencyManager, IInitializable
    {
        public event Action<CurrencyEventData> OnBalanceChanged;

        private readonly CurrencyBalanceConfig _config;
        private readonly ISavesManager _savesManager;

        public CurrencyManager(CurrencyBalanceConfig config, ISavesManager savesManager)
        {
            _config = config;
            _savesManager = savesManager;
        }

        public void Initialize()
        {
            _config.saveData.SetupStartBalance(_config.StartBalance);

            _savesManager.RegisterAndLoad(_config.saveData, UpdateAllCurrencies);
        }

        public List<string> GetCurrencyNames()
        {
            return _config.CurrencyNames.Select(value => value.value).ToList();
        }

        public string GetGameCurrency(CurrencyType currencyType)
        {
            foreach (var currencyName in _config.CurrencyNames)
            {
                if (currencyType == currencyName.key)
                {
                    return currencyName.value;
                }
            }

            return currencyType.ToString();
        }

        public CurrencyType? GetCurrencyFromGameType(string gameType)
        {
            foreach (var currencyName in _config.CurrencyNames)
            {
                if (currencyName.value == gameType)
                {
                    return currencyName.key;
                }
            }

            Debug.Log($"CurrencyBalance gameType:{gameType} not found in currencyNames");
            return null;
        }


        public void UpdateAllCurrencies()
        {
            foreach (var keyValue in _config.saveData)
            {
                OnBalanceChanged?.Invoke(new CurrencyEventData(keyValue.key, keyValue.value, keyValue.value));
            }
        }

        public int GetCurrentBalance(CurrencyType currencyType)
        {
            foreach (var keyValue in _config.saveData)
            {
                if (keyValue.key == currencyType)
                {
                    return keyValue.value;
                }
            }

            return 0;
        }

        private void SetBalanceInternal(CurrencyType currencyType, int value, string placement = "")
        {
            _config.saveData.Update(currencyType, value);
            _config.saveData.MarkIsDirty();
            // if (_analyticsManager != null)
            // {
            //     _analyticsManager.TrackCustomEvent(_currencyChangedEvent.EventName, new Dictionary<string, object>()
            //     {
            //         { "currency", currencyType.ToString() },
            //         { "balance", value },
            //         { "placement", placement }
            //     }, _currencyChangedEvent.Filter);
            // }
        }

        public void SetBalance(CurrencyType currencyType, int value, string placement = "")
        {
            var newBalance = Mathf.Abs(value);
            var current = GetCurrentBalance(currencyType);

            SetBalanceInternal(currencyType, value, placement);
            OnBalanceChanged?.Invoke(new CurrencyEventData(currencyType, newBalance, current));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="toAdd">must be > 0 </param>
        /// <param name="placement"></param>
        /// <returns></returns>
        public int Add(CurrencyType currency, int toAdd, string placement = "")
        {
            if (toAdd <= 0) return 0;

            var current = GetCurrentBalance(currency);
// #if CHEATS
//             if (_cheatMode)
//             {
//                 OnBalanceChanged?.Invoke(new CurrencyEventData(currency, current, current, 0, 0, placement));
//                 return current;
//             }
//
// #endif
            var result = current + toAdd;
            SetBalanceInternal(currency, result, placement);

            OnBalanceChanged?.Invoke(new CurrencyEventData(currency, result, current, 0, toAdd, placement));

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="toRemove">must be > 0</param>
        /// <param name="placement"></param>
        /// <returns></returns>
        public bool Remove(CurrencyType currency, int toRemove, string placement = "")
        {
            if (toRemove < 0) return false;
            var current = GetCurrentBalance(currency);

// #if CHEATS
//             if (_cheatMode)
//             {
//                 OnBalanceChanged?.Invoke(
//                     new CurrencyEventData(currency, current, current, 0));
//                 return true;
//             }
// #endif

            if (current >= toRemove)
            {
                var result = current - toRemove;

                SetBalanceInternal(currency, result, placement);

                OnBalanceChanged?.Invoke(new CurrencyEventData(currency, result, current, Mathf.Abs(toRemove), 0,
                    placement));

                return true;
            }

            OnBalanceChanged?.Invoke(new CurrencyEventData(currency, 0, current, Mathf.Abs(toRemove)));

            return false;
        }
    }
}