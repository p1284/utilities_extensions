namespace GameReady.Currency
{
    public readonly struct CurrencyEventData
    {
        public readonly CurrencyType CurrencyType;
        public readonly int Removed;
        public readonly int Added;
        public readonly int Current;
        public readonly int Changed;

        public readonly bool NotEnoughCurrency;

        public readonly string Placement;

        public CurrencyEventData(CurrencyType currencyType, int changed, int current, int removed = 0, int added = 0,
            string placement = "")
        {
            CurrencyType = currencyType;
            Removed = removed;
            Added = added;
            Changed = changed;
            Current = current;
            NotEnoughCurrency = current < removed;
            Placement = placement;
        }
    }
}