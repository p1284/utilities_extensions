using System;
using System.Collections;
using System.Collections.Generic;
using Gameready.Helpers;
using GameReady.Saves;
using Gameready.Utils.Data;
using UnityEngine;

namespace GameReady.Currency
{
    [Serializable]
    public class BalanceSaveData : BaseSaveObject, IEnumerable<KeyValue<CurrencyType, int>>
    {
        [SerializeField] private List<KeyValue<CurrencyType, int>> _balance;

        public override void Dispose()
        {
            _balance.Clear();
        }

        public void SetupStartBalance(List<KeyValue<CurrencyType, int>> startBalance)
        {
            _balance.Clear();
            foreach (var keyValue in startBalance)
            {
                _balance.Add(new KeyValue<CurrencyType, int>(keyValue.key, keyValue.value));
            }
        }

        public void Update(CurrencyType currencyType, int count)
        {
            bool changed = false;
            foreach (var keyValue in _balance)
            {
                if (keyValue.key == currencyType)
                {
                    keyValue.value = count;
                    changed = true;
                    break;
                }
            }

            if (!changed)
            {
                _balance.Add(new KeyValue<CurrencyType, int>(currencyType, count));
            }
        }

        protected override void DeserializeInternal(string json)
        {
            _balance = JsonUtilityArray.FromJsonArray<KeyValue<CurrencyType, int>>(json);
        }

        protected override string SerializeInternal()
        {
            return JsonUtilityArray.ToJsonList<KeyValue<CurrencyType, int>>(_balance, true);
        }

        public IEnumerator<KeyValue<CurrencyType, int>> GetEnumerator()
        {
            return _balance.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}