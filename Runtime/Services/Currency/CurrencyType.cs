namespace GameReady.Currency
{
    public enum CurrencyType
    {
        HardCurrency = 0,
        CollectableCurrency_1 = 1,
        CollectableCurrency_2 = 2,
        CollectableCurrency_3 = 3,
        CollectableCurrency_4 = 4
    }
}