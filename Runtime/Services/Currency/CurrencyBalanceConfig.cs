using System.Collections.Generic;
using System.Linq;
using Gameready.Utils.Data;
using Gameready.Utils.Services.Core;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace GameReady.Currency
{
    public class CurrencyBalanceConfig : BaseServiceConfig
    {

        [SerializeField] private List<KeyValue<CurrencyType, int>> _startBalance;
        
        [SerializeField] private List<KeyValue<CurrencyType, string>> _currencyNames;

        [Header("SAVE DATA")]
        public BalanceSaveData saveData;

        public List<KeyValue<CurrencyType, int>> StartBalance => _startBalance;

        public List<KeyValue<CurrencyType, string>> CurrencyNames => _currencyNames;
       

#if UNITY_EDITOR

        public void ValidateEditorOnly()
        {
            if (!Application.isPlaying)
            {
                _startBalance = _startBalance.Distinct().ToList();

                _currencyNames = _currencyNames.Distinct().ToList();
            }
        }
#endif
        
        public override void RegisterService(IContainerBuilder builder)
        {
            builder.RegisterEntryPoint<CurrencyManager>().As<ICurrencyManager>().WithParameter(this);
        }
    }
}