using System;
using System.Collections.Generic;

namespace GameReady.Currency
{
    public interface ICurrencyManager
    {
        event Action<CurrencyEventData> OnBalanceChanged;

        void UpdateAllCurrencies();

        int GetCurrentBalance(CurrencyType currencyType);

        void SetBalance(CurrencyType currencyType, int value, string placement = "");

        int Add(CurrencyType currency, int toAdd, string placement = "");

        bool Remove(CurrencyType currency, int toRemove, string placement = "");

        CurrencyType? GetCurrencyFromGameType(string gameType);

        List<string> GetCurrencyNames();

        string GetGameCurrency(CurrencyType currencyType);
    }
}