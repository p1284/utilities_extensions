using System;
using Gameready.Utils.Data;
using UnityEngine;

namespace GameReady.Inventory
{
    public class ItemModel
    {
        public event Action<CountChangeEvent> OnCountChange;

        public readonly int MaxCount;

        public readonly string DisplayName;

        public readonly string ID;

        private int _count;

        private readonly KeyValueDictionary<string, string> _customData;

        public ItemModel(ItemData itemData, int count)
        {
            ID = itemData.ID;
            DisplayName = itemData.DisplayName;
            MaxCount = itemData.MAXCount;

            _count = Mathf.Clamp(count, 0, MaxCount);

            if (itemData.CustomData.Count > 0)
            {
                _customData = itemData.CustomData;
            }
        }


        /// <summary>
        /// CustomData
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public string GetCustomDataString(string key, string defaultValue = "")
        {
            if (_customData != null && _customData.TryGetValue(key, out var s))
            {
                return s;
            }

            return defaultValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public float GetCustomDataFloat(string key, float defaultValue = 0)
        {
            if (_customData != null && _customData.TryGetValue(key, out var value))
            {
                if (float.TryParse(value, out float result))
                {
                    return result;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public int GetCustomDataInt(string key, int defaultValue = 0)
        {
            if (_customData != null && _customData.TryGetValue(key, out var value))
            {
                if (int.TryParse(value, out int result))
                {
                    return result;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool HasCustomData(string key)
        {
            return _customData?.ContainsKey(key) ?? false;
        }

        public int Count => _count;

        // set
        // {
        //     if (_count == value) return;
        //     _count = value;
        //     _count = Mathf.Clamp(_count, 0, MaxCount);
        //     OnCountChange?.Invoke(ID, _count);
        // }


        /// <summary>
        /// Always increment count
        /// </summary>
        /// <param name="value">-+ value None </param>
        /// <param name="placement"></param>
        public void Change(int value, string placement = "")
        {
            _count += value;
            _count = Mathf.Clamp(_count, 0, MaxCount);
            OnCountChange?.Invoke(new CountChangeEvent(ID, _count,
                value > 0 ? ItemCountOperation.Add : ItemCountOperation.Remove, placement));
        }
    }
}