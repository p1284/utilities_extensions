using System;
using System.Collections.Generic;
using Gameready.Utils.Data;

namespace GameReady.Inventory
{
    public class ItemPackModel
    {
        public event Action<string> OnCollected;

        private readonly ItemsPackData _packData;

        public readonly string ID;

        private readonly List<ItemModel> _allItems;

        private Dictionary<string, int> _itemsDict;

        private bool _isCollected;

        public ItemPackModel(ItemsPackData packData, List<ItemModel> allItems)
        {
            _packData = packData;

            ID = packData.ID;

            _allItems = allItems;
        }

        public int TotalItems => _packData.Items.Length;

        public bool IsCollected => _isCollected;

        public ItemModel GetItemByIndex(int index)
        {
            if (index >= 0 && index < _packData.Items.Length)
            {
                return _allItems.Find(model => model.ID == _packData.Items[index].key);
            }

            return null;
        }

        public ItemModel FindItem(string id)
        {
            _itemsDict ??= KeyValue<string, int>.CreateDictionary(_packData.Items);
            if (_itemsDict.ContainsKey(id))
            {
                return _allItems.Find(model => model.ID == id);
            }

            return null;
        }

        public IReadOnlyCollection<ItemModel> GetItems()
        {
            _itemsDict ??= KeyValue<string, int>.CreateDictionary(_packData.Items);

            var list = new List<ItemModel>();
            foreach (var itemModel in _allItems)
            {
                if (_itemsDict.ContainsKey(itemModel.ID))
                {
                    list.Add(itemModel);
                }
            }

            return list.AsReadOnly();
        }

        public void Collect(string placement = "")
        {
            if (_isCollected) return;
            _isCollected = true;

            _itemsDict ??= KeyValue<string, int>.CreateDictionary(_packData.Items);

            OnCollected?.Invoke(ID);

            foreach (var itemModel in _allItems)
            {
                if (_itemsDict.TryGetValue(itemModel.ID, out var value))
                {
                    itemModel.Change(value, placement);
                }
            }
        }
    }
}