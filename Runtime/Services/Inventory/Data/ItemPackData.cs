using System;
using Gameready.Utils.Data;
using UnityEngine;

namespace GameReady.Inventory
{
    [Serializable]
    public class ItemsPackData : IEquatable<ItemsPackData>
    {
        [SerializeField] private string _id;

        [Tooltip("ID of itemdata and count to collect, when ItemPack.Collect")] [SerializeField]
        private KeyValue<string, int>[] _items;

        public KeyValue<string, int>[] Items
        {
            get { return _items; }
#if UNITY_EDITOR
            set { _items = value; }
#endif
        }

        public string ID => _id;

        public bool Equals(ItemsPackData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _id == other._id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ItemsPackData)obj);
        }

        public override int GetHashCode()
        {
            return (_id != null ? _id.GetHashCode() : 0);
        }
    }
}