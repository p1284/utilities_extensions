namespace GameReady.Inventory
{
    public enum ItemCountOperation
    {
        None,
        Add,
        Remove
    }
}