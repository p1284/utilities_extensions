using System;
using System.Collections.Generic;
using System.Linq;
using GameReady.Saves;
using Gameready.Utils.Data;

namespace GameReady.Inventory
{
    [Serializable]
    public class InventorySaveData : BaseSaveObject
    {
        public List<KeyValue<string, int>> items = default;

        public List<string> collectedItemPacks = default;

        public void Remove(string id)
        {
            items.RemoveAll(value => value.key == id);
        }

        public void Set(string id, int count)
        {
            foreach (var value in items.Where(value => value.key == id))
            {
                value.value = count;
                return;
            }

            items.Add(new KeyValue<string, int>(id, count));
        }

        public int GetCount(string id)
        {
            var count = items.Find(value => value.key == id)?.value;
            return count ?? 0;
        }

        public override void Dispose()
        {
            items.Clear();
        }
    }
}