namespace GameReady.Inventory
{
    public readonly struct CountChangeEvent
    {
        public readonly string ID;
        public readonly int Count;
        public readonly ItemCountOperation Operation;
        public readonly string Placement;

        public CountChangeEvent(string id, int count,ItemCountOperation operation, string placement)
        {
            ID = id;
            Count = count;
            Operation = operation;
            Placement = placement;
        }
    }
}