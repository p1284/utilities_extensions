using System;
using Gameready.Utils.Data;
using UnityEngine;

namespace GameReady.Inventory
{
    [Serializable]
    public class ItemData : IEquatable<ItemData>
    {
        [SerializeField] private string _id;

        [Tooltip("Max Limit count")] [SerializeField]
        private int _maxCount;

        [SerializeField] private string _displayName;

        public KeyValueDictionary<string, string> CustomData;

        public ItemData(string id, int maxCount, string displayName)
        {
            _id = id;
            _maxCount = maxCount;
            _displayName = displayName;
        }

        public string ID => _id;
        public string DisplayName => _displayName;

        public int MAXCount => _maxCount;

        public bool Equals(ItemData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _id == other._id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ItemData)obj);
        }

        public override int GetHashCode()
        {
            return (_id != null ? _id.GetHashCode() : 0);
        }
    }
}