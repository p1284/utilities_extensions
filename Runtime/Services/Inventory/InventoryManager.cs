using System;
using System.Collections.Generic;
using System.Linq;
using GameReady.Saves;
using Gameready.Utils;
using UnityEngine;
using VContainer.Unity;

namespace GameReady.Inventory
{
    public class InventoryManager:IInventoryManager,IInitializable,IDisposable
    {
        private readonly PlayerInventoryConfig _config;
        private readonly ISavesManager _savesManager;

        private readonly List<ItemModel> _itemModels;

        private readonly List<ItemPackModel> _itemPackModels;

        private InventorySaveData _saveData;
        
        public InventoryManager(PlayerInventoryConfig config,ISavesManager savesManager)
        {
            _itemModels = new List<ItemModel>(config.Items.Length);
            _itemPackModels = new List<ItemPackModel>(config.Items.Length);
            _config = config;
            _savesManager = savesManager;
        }

        public void Initialize()
        {
            _saveData = _config.Inventory;
            foreach (var startupItem in _config.StartupItems)
            {
                _saveData.Set(startupItem.key, startupItem.value);
            }
            _savesManager.RegisterAndLoad(_saveData,null);
            
           
        }
        
         /// <summary>
        /// 
        /// </summary>
        /// <returns>new List of ItemPackModels</returns>
        public List<ItemPackModel> GetItemPackModels()
        {
            return GetItemPackModelsInternal().ToList();
        }

        /// <summary>
        /// Need cache operation
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Not Collected ItemPacks</returns>
        public ItemPackModel GetItemPack(string id)
        {
            var itemPack = _itemPackModels.Find(i => i.ID == id);
            if (itemPack != null)
            {
                itemPack = AddItemPackInternal(_config.ItemPacks.Find(pack => pack.ID == id), id);
            }

            return itemPack;
        }

        public ItemModel GetItem(string id)
        {
            var item = _itemModels.Find(i => i.ID == id);

            if (item == null)
            {
                var itemData = _config.Items.Find(i => i.ID == id);
                item = AddItemInternal(itemData, id);
            }

            return item;
        }

        /// <summary>
        /// Need cache operation
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>new List of ItemModels </returns>
        public List<ItemModel> GetItems(Predicate<ItemModel> predicate)
        {
            var items = GetItems();
            return items.FindAll(predicate);
        }


        /// <summary>
        /// Need cache operation
        /// </summary>
        /// <returns>new List of ItemModels</returns>
        public List<ItemModel> GetItems()
        {
            return GetItemsInternal().ToList();
        }

        private List<ItemPackModel> GetItemPackModelsInternal()
        {
            foreach (var itemPackModel in _itemPackModels)
            {
                itemPackModel.OnCollected -= OnItemPackCollectedHandler;
            }

            _itemPackModels.Clear();

            foreach (var itemsPackData in _config.ItemPacks)
            {
                var pack = AddItemPackInternal(itemsPackData);
                if (pack != null)
                {
                    _itemPackModels.Add(pack);
                }
            }

            return _itemPackModels;
        }

        private List<ItemModel> GetItemsInternal()
        {
            if (_itemModels.Count == _config.Items.Length)
            {
                return _itemModels;
            }

            foreach (var itemModel in _itemModels)
            {
                itemModel.OnCountChange -= OnItemCountChangedHandler;
            }

            _itemModels.Clear();

            foreach (var itemData in _config.Items)
            {
                _itemModels.Add(AddItemInternal(itemData));
            }

            return _itemModels;
        }

        private ItemPackModel AddItemPackInternal(ItemsPackData packData, string itemPackId = "")
        {
            if (packData != null)
            {
                if (!_saveData.collectedItemPacks.Contains(packData.ID))
                {
                    var itemPackModel = new ItemPackModel(packData, GetItemsInternal());
                    itemPackModel.OnCollected += OnItemPackCollectedHandler;
                    _itemPackModels.Add(itemPackModel);
                    return itemPackModel;
                }

                return null;
            }

            throw new Exception($"PlayerInventory: Item with {itemPackId} not exist");
        }


        private ItemModel AddItemInternal(ItemData itemData, string id = "")
        {
            if (itemData != null)
            {
                var item = new ItemModel(itemData, _saveData.GetCount(itemData.ID));
                item.OnCountChange += OnItemCountChangedHandler;
                _itemModels.Add(item);
                return item;
            }

            throw new Exception($"PlayerInventory: Item with {id} not exist");
        }

        private void OnItemPackCollectedHandler(string packId)
        {
            if (!_config.Inventory.collectedItemPacks.Contains(packId))
            {
                _saveData.collectedItemPacks.Add(packId);

                // if (_analytics != null)
                // {
                //     _analytics.TrackCustomEvent(_inventoryChangedEvent.EventName,
                //         new Dictionary<string, object>()
                //         {
                //             { "type", "item_pack" },
                //             { "op", "collected" },
                //             { "itemID", packId }
                //         }, _inventoryChangedEvent.Filter);
                // }


                _saveData.MarkIsDirty();
            }

            var itemPack = _itemPackModels.Find(p => p.ID == packId);
            itemPack.OnCollected -= OnItemPackCollectedHandler;
            _itemPackModels.Remove(itemPack);
            Debug.Log($"PlayerInventory ItemPack:{packId} collected!");
        }

        private void OnItemCountChangedHandler(CountChangeEvent countChangeEvent)
        {
            if (countChangeEvent.Operation == ItemCountOperation.Remove)
            {
                if (countChangeEvent.Count == 0)
                {
                    _saveData.Remove(countChangeEvent.ID);
                }
            }
            else
            {
                _saveData.Set(countChangeEvent.ID, countChangeEvent.Count);
            }

            // if (_analytics != null)
            // {
            //     _analytics.TrackCustomEvent(_inventoryChangedEvent.EventName,
            //         new Dictionary<string, object>()
            //         {
            //             { "type", "item" },
            //             { "op", countChangeEvent.Operation.ToString().ToLower() },
            //             { "placement", countChangeEvent.Placement },
            //             { "count", countChangeEvent.Count },
            //             { "itemID", countChangeEvent.ID }
            //         }, _inventoryChangedEvent.Filter);
            // }


            _saveData.MarkIsDirty();
            Debug.Log(
                $"PlayerInventory item:{countChangeEvent.ID} changed: {countChangeEvent.Operation}" +
                $" count:{countChangeEvent.Count}");
        }

        public void Dispose()
        {
            foreach (var itemModel in _itemModels)
            {
                itemModel.OnCountChange -= OnItemCountChangedHandler;
            }

            _itemModels.Clear();

            foreach (var itemPackModel in _itemPackModels)
            {
                itemPackModel.OnCollected -= OnItemPackCollectedHandler;
            }

            _itemPackModels.Clear();
        }
    }
}