using System;
using System.Collections.Generic;

namespace GameReady.Inventory
{
    public interface IInventoryManager
    {
        List<ItemPackModel> GetItemPackModels();

        ItemPackModel GetItemPack(string id);

        ItemModel GetItem(string id);

        List<ItemModel> GetItems(Predicate<ItemModel> predicate);

        List<ItemModel> GetItems();
    }
}