using System;
using System.Collections.Generic;
using System.Linq;
using Gameready.Utils;
using Gameready.Utils.Data;
using Gameready.Utils.Services.Core;
using UnityEngine;
using VContainer;
using VContainer.Unity;
using Debug = UnityEngine.Debug;

namespace GameReady.Inventory
{
    public class PlayerInventoryConfig : BaseServiceConfig
    {
        [SerializeField] private List<KeyValue<string, int>> _startupItems;

        [SerializeField] private ItemData[] _items;

        [SerializeField] private ItemsPackData[] _itemPacks;


        [Header("SAVE DATA")] public InventorySaveData Inventory = default;


        public string[] GetAllItemIds
        {
            get { return _items.Select(data => data.ID).ToArray(); }
        }

        public string[] GetAllItemPackIds
        {
            get { return _itemPacks.Select(data => data.ID).ToArray(); }
        }

        public ItemData[] Items => _items;

        public ItemsPackData[] ItemPacks => _itemPacks;

        public List<KeyValue<string, int>> StartupItems => _startupItems;


#if UNITY_EDITOR

        public void ValidateEditorOnly()
        {
            _startupItems = _startupItems.Distinct().ToList();

            Inventory.items.RemoveAll(i => string.IsNullOrEmpty(i.key));

            Inventory.items = Inventory.items.Distinct().ToList();

            Inventory.collectedItemPacks.RemoveAll(string.IsNullOrEmpty);

            Inventory.collectedItemPacks = Inventory.collectedItemPacks.Distinct().ToList();

            var list = _items.ToList();

            _items = list.Distinct().ToArray();

            var packsList = _itemPacks.ToList();

            _itemPacks = packsList.Distinct().ToArray();

            foreach (var startupItem in _startupItems)
            {
                if (string.IsNullOrEmpty(startupItem.key))
                {
                    throw new Exception("PlayerInventory has empty Id in itemData");
                }

                if (!_items.Exists(i => i.ID == startupItem.key))
                {
                    throw new Exception($"PlayerInventory StartUpItems item:{startupItem.key} not exist in Items");
                }
            }

            foreach (var itemData in _items)
            {
                if (string.IsNullOrEmpty(itemData.ID))
                {
                    throw new Exception("PlayerInventory has empty Id in itemData");
                }
            }

            foreach (var itemsPackData in _itemPacks)
            {
                if (string.IsNullOrEmpty(itemsPackData.ID))
                {
                    throw new Exception("PlayerInventory has empty Id in ItemPackData");
                }

                itemsPackData.Items = itemsPackData.Items.Distinct().ToArray();

                foreach (var item in _items)
                {
                    if (!itemsPackData.Items.Exists(i => i.key == item.ID))
                    {
                        throw new Exception($"PlayerInventory has wrong item id in ItemPackData:{itemsPackData.ID}");
                    }
                }
            }

            // foreach (var asset in _assets)
            // {
            //     if (string.IsNullOrEmpty(asset.key))
            //     {
            //         throw new Exception("PlayerInventory has empty Id in Assets");
            //     }
            //
            //     var itemData = _items.Find(data => data.ID == asset.key);
            //     if (itemData == null)
            //     {
            //         throw new Exception($"PlayerInventory has wrong assetId:{asset.key} not bind to any ItemData");
            //     }
            // }

            Debug.Log("PlayerInventory validation complete.");
        }

#endif
        public override void RegisterService(IContainerBuilder builder)
        {
            builder.RegisterEntryPoint<InventoryManager>().As<IInventoryManager>().WithParameter(this);
        }
    }
}