using System.Globalization;
using System.Linq;
#if CHEATS
using Gameready.Cheats;
#endif
using GameReady.Currency;
using UnityEngine;
using UnityEngine.UIElements;
using VContainer;

namespace GameReady.Saves.Cheats
{
#if CHEATS
    // [CreateAssetMenu(menuName = "CurrencyCheats" , fileName = "CurrencyCheats")]
    public class CurrencyCheats : BaseUIToolkitCheat
    {
        private DropdownField _currencyDropDown;
        private Button _setButton;
        private TextField _balanceField;

        private VisualElement _mainContainer;

        private ICurrencyManager _currencyManager;

        protected override VisualElement BuildInternal()
        {
            _mainContainer = _template.Instantiate();

            _currencyManager = Resolver.Resolve<ICurrencyManager>();

            _currencyManager.OnBalanceChanged += OnBalanceChangedHandler;

            _currencyDropDown = _mainContainer.Q<DropdownField>();
            _setButton = _mainContainer.Q<Button>();
            _balanceField = _mainContainer.Q<TextField>();

            _setButton.clicked += OnSetCurrencyClick;

            var currencyNames = _currencyManager.GetCurrencyNames();
            _currencyDropDown.choices = currencyNames;
            _currencyDropDown.RegisterValueChangedCallback(OnCurrencyTypeSelected);
            _currencyDropDown.value = currencyNames.FirstOrDefault()?.ToString();
            var currencyType = _currencyManager.GetCurrencyFromGameType(_currencyDropDown.value);
            if (currencyType != null)
                _balanceField.value = _currencyManager.GetCurrentBalance(currencyType.Value).ToString();

            return _mainContainer;
        }

        private void OnBalanceChangedHandler(CurrencyEventData eventData)
        {
            var currencyType = _currencyManager.GetCurrencyFromGameType(_currencyDropDown.value);
            if (currencyType == eventData.CurrencyType)
            {
                _balanceField.value = eventData.Changed.ToString();
            }
        }

        private void OnSetCurrencyClick()
        {
            var currencyType = _currencyManager.GetCurrencyFromGameType(_currencyDropDown.value);
            if (currencyType != null && int.TryParse(_balanceField.value, NumberStyles.Integer, null, out int value))
            {
                _currencyManager.SetBalance(currencyType.Value, value);
            }
        }

        private void OnCurrencyTypeSelected(ChangeEvent<string> changeEvent)
        {
            var currencyType = _currencyManager.GetCurrencyFromGameType(changeEvent.newValue);

            if (currencyType != null)
                _balanceField.value = _currencyManager.GetCurrentBalance(currencyType.Value).ToString();
        }

        public override void Dispose()
        {
            base.Dispose();
            if(_currencyManager != null)
                _currencyManager.OnBalanceChanged -= OnBalanceChangedHandler;

            if (_setButton != null)
                _setButton.clicked -= OnSetCurrencyClick;

            if (_currencyDropDown != null)
                _currencyDropDown.UnregisterValueChangedCallback(OnCurrencyTypeSelected);

            _mainContainer?.RemoveFromHierarchy();
        }
    }
#endif
}