using System.Collections.Generic;
using System.Linq;
#if CHEATS
using Gameready.Cheats;
using VContainer;
#endif
using GameReady.Inventory;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameReady.Saves.Cheats
{
#if CHEATS
    
    //[CreateAssetMenu(menuName = "InventoryCheats" , fileName = "InventoryCheats")]
    public class InventoryCheats : BaseUIToolkitCheat
    {
        private DropdownField _itemsDropdown;
        private Button _addButton;
        private Button _removeButton;
        private Label _balanceLabel;

        private ItemModel _selectedItem;

        private List<ItemModel> _items;

        private VisualElement _mainContainer;

        private IInventoryManager _inventoryManager;
        
        protected override VisualElement BuildInternal()
        {
            _inventoryManager = Resolver.Resolve<IInventoryManager>();
            
            _mainContainer = _template.Instantiate();

            _itemsDropdown = _mainContainer.Q<DropdownField>();
            _addButton = _mainContainer.Q<Button>("add-button");
            _removeButton = _mainContainer.Q<Button>("remove-button");
            _balanceLabel = _mainContainer.Q<Label>("balance-label");

            _removeButton.clicked += OnItemRemoveClick;
            _addButton.clicked += OnItemAddClick;

            _items = _inventoryManager.GetItems();

            _selectedItem = _items.FirstOrDefault();
            if (_selectedItem != null)
                _selectedItem.OnCountChange += OnCountChangeHandler;

            _itemsDropdown.choices = _items.Select(i => i.ID).ToList();
            _itemsDropdown.RegisterValueChangedCallback(OnItemSelectedCallback);
            _itemsDropdown.value = _selectedItem?.ID;
            _balanceLabel.text = _selectedItem?.Count.ToString();

            return _mainContainer;
        }

        private void OnItemAddClick()
        {
            _selectedItem?.Change(1, "cheats");
        }

        private void OnItemRemoveClick()
        {
            _selectedItem?.Change(-1, "cheats");
        }

        private void OnItemSelectedCallback(ChangeEvent<string> evt)
        {
            if (_selectedItem != null)
            {
                _selectedItem.OnCountChange -= OnCountChangeHandler;
            }

            _selectedItem = _items.Find(i => i.ID == evt.newValue);
            if (_selectedItem != null)
            {
                _selectedItem.OnCountChange -= OnCountChangeHandler;
                _selectedItem.OnCountChange += OnCountChangeHandler;
                _balanceLabel.text = _selectedItem.Count.ToString();
            }
        }

        private void OnCountChangeHandler(CountChangeEvent countChangeEvent)
        {
            _balanceLabel.text = _selectedItem?.Count.ToString() ?? "0";
        }

        public override void Dispose()
        {
            base.Dispose();
            if (_selectedItem != null)
            {
                _selectedItem.OnCountChange -= OnCountChangeHandler;
            }

            if (_removeButton != null)
                _removeButton.clicked -= OnItemRemoveClick;

            if (_addButton != null)
                _addButton.clicked -= OnItemAddClick;

            _itemsDropdown?.UnregisterValueChangedCallback(OnItemSelectedCallback);

            _mainContainer?.RemoveFromHierarchy();
        }
    }
#endif
}