#if CHEATS
using Gameready.Cheats;
#endif
using System;
using UnityEngine;
using UnityEngine.UIElements;
using VContainer;

namespace GameReady.Saves.Cheats
{
#if CHEATS
    //[CreateAssetMenu(menuName = "SaveCheats", fileName = "SaveCheats")]
    public class SavesCheat : BaseUIToolkitCheat
    {
        private Button _saveButton;
        private Button _clearButton;

        private VisualElement _saveElementGroup;

        private ISavesManager _savesManager;
        
        protected override VisualElement BuildInternal()
        {

            _savesManager = Resolver.Resolve<ISavesManager>();
            
            _saveElementGroup = _template.Instantiate();
            
            _saveButton = _saveElementGroup.Q<Button>("save-button");
            _saveButton.clicked += OnSaveButtonClicked;

            _clearButton = _saveElementGroup.Q<Button>("clearsaves-button");
            _clearButton.clicked += OnClearSavesClicked;

            return _saveElementGroup;
        }

        private void OnClearSavesClicked()
        {
            _savesManager.ClearAll();
            Application.Quit();
#if UNITY_EDITOR

            UnityEditor.EditorApplication.isPlaying = false;

#endif
        }

        private void OnSaveButtonClicked()
        {
            _savesManager.SaveAll();
            GameCheatsPanel.Instance.Hide();
        }
        
        public override void Dispose()
        {
            base.Dispose();
            
            if (_saveButton != null)
            {
                _saveButton.clicked -= OnSaveButtonClicked;
            }

            if (_clearButton != null)
            {
                _clearButton.clicked -= OnClearSavesClicked;
            }
            _saveElementGroup?.RemoveFromHierarchy();
        }
    }
#endif
}