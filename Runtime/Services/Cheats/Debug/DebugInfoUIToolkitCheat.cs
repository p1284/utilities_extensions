using System.Text;
using GameReady.Build;
#if CHEATS
using Gameready.Cheats;
#endif
using Gameready.Utils;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameReady.Cheats
{
#if CHEATS
    //  [CreateAssetMenu(menuName = "DebugCheats")]
    public class DebugInfoUIToolkitCheat : BaseUIToolkitCheat
    {
        private TextElement _textElement;

        private BuildSettings _buildSettings;

        private VisualElement _debugContainer;


        protected override VisualElement BuildInternal()
        {
            _buildSettings = BuildSettings.Instance;

            _debugContainer = _template.Instantiate();
            _textElement = _debugContainer.Q<TextElement>("debug-text");
            _textElement.visible = true;

            var sb = new StringBuilder();
            sb.AppendLine($"Version:{_buildSettings.Version}");
            sb.AppendLine($"Build Number:{_buildSettings.BuildNumber}");
            sb.AppendLine($"Network status:<b>{NetworkCheck.LastConnectionStatus}</b>");

            _textElement.text = sb.ToString();

            NetworkCheck.OnNetworkStateChanged += OnNetworkChangedHandler;

            return _debugContainer;
        }

        void OnDestroy()
        {
            NetworkCheck.OnNetworkStateChanged -= OnNetworkChangedHandler;

            _debugContainer.RemoveFromHierarchy();
        }

        private void OnNetworkChangedHandler()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Version:{_buildSettings.Version}");
            sb.AppendLine($"Build Number:{_buildSettings.BuildNumber}");
            sb.AppendLine($"Network status:<b>{NetworkCheck.LastConnectionStatus}</b>");

            _textElement.text = sb.ToString();
        }
    }
#endif
}