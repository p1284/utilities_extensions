using UnityEngine;
using VContainer;

namespace Gameready.Utils.Services.Core
{
    public abstract class BaseServiceConfig : ScriptableObject
    {
        public abstract void RegisterService(IContainerBuilder builder);
    }
}