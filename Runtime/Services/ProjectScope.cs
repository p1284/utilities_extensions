using System.Collections.Generic;
using Gameready.Utils;
using Gameready.Utils.Services.Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using VContainer;
using VContainer.Unity;

namespace Gameready.Services
{
    public class ProjectScope : LifetimeScope
    {
        [SerializeField] private List<BaseServiceConfig> _serviceConfigs;

#if UNITY_EDITOR

        [Header("EDITOR ONLY")] [Tooltip("If empty run by autorun everywhere")] [SerializeField] [NonReorderable]
        private UnityEditor.SceneAsset[] _runOnlyInScenes;

        public List<BaseServiceConfig> ServiceConfigs => _serviceConfigs;
#endif

        protected override void Configure(IContainerBuilder builder)
        {
            MonoHelper.Initialize();

#if UNITY_EDITOR

            var canConfigure = true;
            foreach (var runOnlyInScene in _runOnlyInScenes)
            {
                if (runOnlyInScene == null) continue;
                if (SceneManager.GetActiveScene().name == runOnlyInScene.name)
                {
                    break;
                }

                canConfigure = false;
            }

            if (!canConfigure) return;

#endif

            foreach (var serviceConfig in _serviceConfigs)
            {
                serviceConfig.RegisterService(builder);
            }

#if CHEATS
#endif
        }
    }
}