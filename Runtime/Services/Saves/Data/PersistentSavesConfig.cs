using System;
using Gameready.Utils;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace GameReady.Saves.Data
{

    public class PersistentSavesConfig:BaseSavesConfig
    {
        [SerializeField] private bool _useEncryption;
        [HideInInspector] [SerializeField] private SaveEncryptionType _encryptionType;
        [HideInInspector] [SerializeField] private int _aesSeed;
        [Range(5, 10)] [HideInInspector] [SerializeField] private int _aesCount;
        
        [NonSerialized] private string _aesPass;
        
        public string AesPass
        {
            get
            {
                if (string.IsNullOrEmpty(_aesPass))
                {
                    _aesPass = CryptoUtils.CreateAesPassword(_aesSeed, _aesCount, "persistent");
                }

                return _aesPass;
            }
        }
        
        public SaveEncryptionType EncryptionType => _encryptionType;

        public int AesSeed => _aesSeed;

        public int AesCount => _aesCount;

        public bool UseEncryption => _useEncryption;
        
        
        public override void RegisterService(IContainerBuilder builder)
        {
            builder.RegisterEntryPoint<PersistentSavesManager>().As<ISavesManager>().WithParameter(this);
        }
    }
}