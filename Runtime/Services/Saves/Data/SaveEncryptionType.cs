namespace GameReady.Saves.Data
{
    public enum SaveEncryptionType
    {
        None = 0,
        Base64,
        AES
    }
}