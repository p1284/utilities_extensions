namespace GameReady.Saves.Data
{
    public enum LoadSaveState
    {
        Idle,
        Loading,
        Saving
    }
}