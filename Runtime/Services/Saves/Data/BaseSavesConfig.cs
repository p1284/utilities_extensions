using System;
using System.Collections.Generic;
using Gameready.Utils.Services.Core;
using UnityEngine;

namespace GameReady.Saves.Data
{
    public abstract class BaseSavesConfig : BaseServiceConfig
    {
        private const float MinAutoSaveFrequencySeconds = 10;

        [SerializeField] private List<BaseSaveObject> _savesList;

        [SerializeField] private bool _saveOnPause;

        [SerializeField] private float _autoSaveFrequency = -1f;

        [SerializeField] private bool _logOnSave;

        public List<BaseSaveObject> SavesList => _savesList;

        public bool SaveOnPause => _saveOnPause;

        public bool LogOnSave => _logOnSave;

        public float AutoSaveFrequency
        {
            get
            {
                if (_autoSaveFrequency >= 0)
                {
                    _autoSaveFrequency = Mathf.Max(_autoSaveFrequency, MinAutoSaveFrequencySeconds);
                }

                return _autoSaveFrequency;
            }
        }
    }
}