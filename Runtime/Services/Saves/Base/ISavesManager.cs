using System;

namespace GameReady.Saves
{
    public interface ISavesManager
    {
        event Action OnSaveAllComplete;

        event Action OnClearAllComplete;
        
        void RegisterAndLoad(BaseSaveObject saveObject, Action callback);

        void SaveAll(bool forceSaveAll = false);

        T GetSaveData<T>() where T : BaseSaveObject;

        void ClearAll();
    }
}