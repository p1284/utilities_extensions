using System;
using System.Diagnostics;
using GameReady.Saves.Data;
using UnityEngine;

namespace GameReady.Saves
{
    [Serializable]
    public abstract class BaseSaveObject :IDisposable
    {
        internal event Action<BaseSaveObject> OnSaved;

        [Tooltip("Unique ID")]
        [SerializeField] private string _name;
        
        [NonSerialized] internal bool IsDirty;

        [NonSerialized] internal bool _isLoaded;

        [NonSerialized] internal LoadSaveState _state;

        public bool IsLoaded => _isLoaded;
        
        internal SaveEncryptionType EncryptedType
        {
            get => (SaveEncryptionType)PlayerPrefs.GetInt(this._name, 0);
            set => PlayerPrefs.SetInt(this._name, (int)value);
        }

        public string Name => _name;
        
        internal string Serialize()
        {
            return SerializeInternal();
        }

        internal void Deserialize(string json)
        {
            DeserializeInternal(json);
        }

        protected virtual string SerializeInternal()
        {
            return JsonUtility.ToJson(this);
        }

        protected virtual void DeserializeInternal(string json)
        {
            JsonUtility.FromJsonOverwrite(json, this);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="forceSave"></param>
        public void MarkIsDirty(bool forceSave = false)
        {
            IsDirty = true;
            if (forceSave)
            {
                OnSaved?.Invoke(this);
            }
        }

        public abstract void Dispose();
    }
}