using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Cysharp.Threading.Tasks;
using GameReady.Saves.Data;
using Gameready.Utils;
using UnityEngine;
using VContainer.Unity;

namespace GameReady.Saves
{
    public sealed class PersistentSavesManager : ISavesManager, IPauseFocusChanged, IInitializable, IDisposable
    {
        public event Action OnSaveAllComplete;
        public event Action OnClearAllComplete;

        private Dictionary<string, string> _encrIv;

        private readonly HashSet<BaseSaveObject> _savesList;

        private readonly PersistentSavesConfig _config;

        public PersistentSavesManager(PersistentSavesConfig config)
        {
            _config = config;
            _savesList = new HashSet<BaseSaveObject>();
        }

        public void Initialize()
        {
            LoadCachedAesEncr();

            if (_config.AutoSaveFrequency > 0f)
            {
                StartAutoSaveAsync().Forget();
            }

            MonoHelper.Initialize();

            MonoHelper.AddPauseFocusListener(this);
        }


        private bool HasAnyToSave()
        {
            foreach (var data in _savesList)
            {
                if (data.IsDirty) return true;
            }

            return false;
        }

        private void OnSavePersistentHandler(BaseSaveObject persistent)
        {
            if (persistent == null) return;
            ForceSaveData(persistent);
        }


        public T GetSaveData<T>() where T : BaseSaveObject
        {
            var type = typeof(T);

            foreach (var saveObject in _savesList)
            {
                if (saveObject.GetType() == type)
                {
                    return (T)saveObject;
                }
            }

            return default(T);
        }

        public void RegisterAndLoad(BaseSaveObject saveObject, Action callback)
        {
            if (!_savesList.Contains(saveObject))
            {
                saveObject.OnSaved += OnSavePersistentHandler;
                _savesList.Add(saveObject);
            }

            LoadAsync(saveObject, callback).Forget();
        }


        public void ClearAll()
        {
            foreach (var persistent in _savesList)
            {
                persistent.IsDirty = false;
                persistent._isLoaded = false;
                persistent._state = LoadSaveState.Idle;
                persistent.Dispose();

                var path = Path.Combine(Application.persistentDataPath, $"{persistent.Name}.dat");
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }

            _encrIv?.Clear();
            OnClearAllComplete?.Invoke();
        }

        public void SaveAll(bool forceSaveAll = false)
        {
            CacheAesEncr();

            if (!HasAnyToSave()) return;
            if (forceSaveAll)
            {
                foreach (var saveObject in _savesList)
                {
                    ForceSaveData(saveObject);
                }

                OnSaveAllComplete?.Invoke();
            }
            else
            {
                SaveAllAsync().Forget();
            }
        }

        private void ForceSaveData(BaseSaveObject persistent, Action callback = null)
        {
            if (!persistent.IsDirty) return;
            persistent.IsDirty = false;

            try
            {
                var path = Path.Combine(Application.persistentDataPath, $"{persistent.Name}.dat");
                var input = Encrypt(persistent, _config.EncryptionType, _config.AesPass);
                File.WriteAllText(path, input, Encoding.UTF8);

                if (_config.LogOnSave)
                {
                    Debug.Log($"{persistent.GetType().Name}: {persistent.Name} saved.");
                }
            }
            catch (Exception e)
            {
                Debug.LogError($"{persistent.GetType().Name}: {persistent.Name} save error:{e.Message}.");
            }

            callback?.Invoke();
        }

        private async UniTask SaveDataAsync(BaseSaveObject persistent)
        {
            while (persistent._state is LoadSaveState.Loading or LoadSaveState.Saving)
            {
                await UniTask.Yield();
            }

            persistent._state = LoadSaveState.Saving;


            if (persistent.IsDirty)
            {
                persistent.IsDirty = false;

                var path = Path.Combine(Application.persistentDataPath, $"{persistent.Name}.dat");
                var text = Encrypt(persistent, _config.EncryptionType, _config.AesPass);

                await File.WriteAllTextAsync(path, text, Encoding.UTF8);

                if (_config.LogOnSave)
                {
                    Debug.Log($"{persistent.GetType().Name}: {persistent.Name} saved.");
                }
            }
        }

        void IPauseFocusChanged.OnPaused(bool paused)
        {
            if (_config.SaveOnPause && paused)
            {
                SaveAll(true);
            }
        }

        void IPauseFocusChanged.OnFocus(bool focused)
        {
            if (_config.SaveOnPause && !focused)
            {
                SaveAll(true);
            }
        }

        private async UniTaskVoid StartAutoSaveAsync()
        {
            var timer = 0f;
            var saveDelaySec = _config.AutoSaveFrequency;
            while (saveDelaySec > 0)
            {
                await UniTask.Yield();
                timer += Time.unscaledDeltaTime;
                if (timer >= saveDelaySec)
                {
                    SaveAll();
                    timer = 0f;
                }
            }
        }

        private void CacheAesEncr()
        {
            var json = _encrIv.ToJson();
            PlayerPrefs.SetString("encr", Convert.ToBase64String(Encoding.UTF8.GetBytes(json)));
        }

        private void LoadCachedAesEncr()
        {
            var encriVJson = PlayerPrefs.GetString("encr", "");
            if (!string.IsNullOrEmpty(encriVJson))
            {
                var json = Encoding.UTF8.GetString(Convert.FromBase64String(encriVJson));
                _encrIv.FromJsonOverwrite(json);
            }
        }

        private string Encrypt(BaseSaveObject saveObject, SaveEncryptionType encryptionType, string aesPass)
        {
            var text = saveObject.Serialize();
            saveObject.EncryptedType = !_config.UseEncryption ? SaveEncryptionType.None : encryptionType;

            var dataKey = saveObject.Name;
            switch (saveObject.EncryptedType)
            {
                case SaveEncryptionType.Base64:
                    try
                    {
                        return Convert.ToBase64String(Encoding.UTF8.GetBytes(text));
                    }
                    catch (Exception e)
                    {
                        Debug.Log($"{this.GetType().Name} Encrypt Error:{e.Message}");
                        return text;
                    }

                case SaveEncryptionType.AES:
                    try
                    {
                        var data = CryptoUtils.Encrypt(text, aesPass);
                        if (!_encrIv.TryAdd(dataKey, data.IV))
                        {
                            _encrIv[dataKey] = data.IV;
                        }

                        return data.EncryptedText;
                    }
                    catch (Exception e)
                    {
                        Debug.Log($"{this.GetType().Name} Encrypt Error:{e.Message}");
                        return text;
                    }
                default:
                    return text;
            }
        }

        private string Decrypt(string text, string dataKey, SaveEncryptionType encryptionType, string aesPass)
        {
            switch (encryptionType)
            {
                case SaveEncryptionType.Base64:
                    try
                    {
                        return Encoding.UTF8.GetString(Convert.FromBase64String(text));
                    }
                    catch (Exception e)
                    {
                        Debug.Log($"{this.GetType().Name} Decrypt Error:{e.Message}");
                        return text;
                    }

                case SaveEncryptionType.AES:
                    try
                    {
                        _encrIv.TryGetValue(dataKey, out var ivKey);
                        return CryptoUtils.Decrypt(text, ivKey, aesPass);
                    }
                    catch (Exception e)
                    {
                        Debug.Log($"{this.GetType().Name} Decrypt Error:{e.Message}");
                        return text;
                    }

                default:
                    return text;
            }
        }

        public void Dispose()
        {
            foreach (var saveObject in _savesList)
            {
                saveObject.OnSaved -= OnSavePersistentHandler;
                saveObject.Dispose();
            }
        }


        private async UniTaskVoid SaveAllAsync()
        {
            foreach (var saveObject in _savesList)
            {
                try
                {
                    await SaveDataAsync(saveObject);
                }
                catch (Exception e)
                {
                    Debug.LogError($@"{saveObject.GetType().Name} Save Error:{e.Message}");
                }
                finally
                {
                    saveObject._state = LoadSaveState.Idle;
                    OnSaveAllComplete?.Invoke();
                }
            }
        }

        private async UniTaskVoid LoadAsync(BaseSaveObject saveObject, Action callback)
        {
            while (saveObject._state is LoadSaveState.Loading or LoadSaveState.Saving)
            {
                await UniTask.Yield();
            }

            saveObject._state = LoadSaveState.Loading;

            var currentLoadName = "";

            try
            {
                currentLoadName = saveObject.Name;
                var path = Path.Combine(Application.persistentDataPath, $"{saveObject.Name}.dat");
                if (File.Exists(path))
                {
                    var text = await File.ReadAllTextAsync(path);
                    saveObject.Deserialize(Decrypt(text, saveObject.Name, saveObject.EncryptedType,
                        _config.AesPass));
                }
                else
                {
                    saveObject.IsDirty = false;
                }

                saveObject._isLoaded = true;

                Debug.Log($"{saveObject.GetType().Name} Load complete.");
            }
            catch (Exception e)
            {
                Debug.LogError($@"{saveObject.GetType().Name} Load Error:{e.Message} for persistent:{currentLoadName}");
            }
            finally
            {
                saveObject._isLoaded = true;
                saveObject._state = LoadSaveState.Idle;
                callback?.Invoke();
            }
        }
    }
}