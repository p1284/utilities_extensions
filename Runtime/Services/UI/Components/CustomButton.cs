using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Gameready.UI.Components
{
    [Serializable]
    public class OnCustomEventClick : UnityEvent<CustomButton>
    {
    }

    [RequireComponent(typeof(Selectable))]
    public class CustomButton : MonoBehaviour, IPointerClickHandler
    {
        public OnCustomEventClick OnClick;

        [SerializeField] private Selectable _selectable;
        
        [SerializeField] private string _id;

        [SerializeField] private TextMeshProUGUI _label;
        
        public bool Enabled
        {
            get => _selectable.interactable;
            set => _selectable.interactable = value;
        }

        public string ID
        {
            get => _id;
            set => _id = value;
        }

        public TextMeshProUGUI Label => _label;

        public object Data { get; set; }

        private void Awake()
        {
            if (_selectable == null)
            {
                _selectable = GetComponent<Selectable>();
            }

            if (_label == null)
            {
                _label = GetComponentInChildren<TextMeshProUGUI>();
            }
        }


        public void OnPointerClick(PointerEventData eventData)
        {
            if (_selectable.interactable)
            {
                OnClick?.Invoke(this);
            }
        }
    }
}