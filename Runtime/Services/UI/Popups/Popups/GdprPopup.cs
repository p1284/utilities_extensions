﻿using Gameready.UI.Components;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameReady.UI.Popups
{
    [RequireComponent(typeof(Canvas), typeof(GraphicRaycaster), typeof(CanvasScaler))]
    public sealed class GdprPopup : BaseGamePopup
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private string _termsOfServiceUrl;
        [SerializeField] private string _privacyPolicyUrl;

        public CustomButton privacyPolicyButton;
        public CustomButton termsOfServiceButton;
        public CustomButton acceptButton;
        public TMP_Text Description;

        protected override void SetSortOrder(int? order)
        {
            if (order != null)
            {
                _canvas.sortingOrder = order.Value;
            }
        }

        public void OpenPrivacyPolicy()
        {
            Application.OpenURL(_privacyPolicyUrl);
        }

        public void OpenTermsOfService()
        {
            Application.OpenURL(_termsOfServiceUrl);
        }
    }
}