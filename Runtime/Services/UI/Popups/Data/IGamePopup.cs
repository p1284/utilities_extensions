using System;

namespace GameReady.UI.Popups.Data
{
    public interface IGamePopup:IDisposable
    {
        event Action<IGamePopup> OnClosed;
        
        string Name { get; set; }
        
        void Open(int? order = null);

        void Close();
        
    }
}