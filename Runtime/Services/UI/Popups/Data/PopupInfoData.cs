using System;
using UnityEngine;

namespace GameReady.UI.Popups.Data
{
    [Serializable]
    public class PopupInfoData : IEquatable<PopupInfoData>
    {
        [SerializeField] private string _popupId;

        [SerializeField] private bool _isPoolable;

        [SerializeField] private GameObject _popupPrefab;

        public bool IsPoolable => _isPoolable;

        public GameObject PopupPrefab
        {
            get { return _popupPrefab; }
#if UNITY_EDITOR
            set { _popupPrefab = value; }
#endif
        }

        public string PopupId
        {
            get { return _popupId; }
#if UNITY_EDITOR
            set { _popupId = value; }
#endif
        }

        public bool IsPopupTypeEqualsPopupId()
        {
            if (_popupPrefab == null) return false;
            var type = _popupPrefab.GetComponent<IGamePopup>()?.GetType().Name;
            return _popupId == type;
        }

        public bool Equals(PopupInfoData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _popupId == other._popupId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PopupInfoData)obj);
        }

        public override int GetHashCode()
        {
            return (_popupId != null ? _popupId.GetHashCode() : 0);
        }
    }
}