using System;
using System.Collections.Generic;
using System.Linq;
using Gameready.Utils.Services.Core;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace GameReady.UI.Popups.Data
{
    public class PopupsManagerConfig : BaseServiceConfig
    {
        [SerializeField] private List<PopupInfoData> _popups;

        public List<PopupInfoData> Popups => _popups;


        public void Validate()
        {
            _popups.RemoveAll(p => p.PopupPrefab == null);
            _popups = _popups.Distinct().ToList();

            foreach (var popupPrefab in _popups)
            {
                if (!popupPrefab.IsPopupTypeEqualsPopupId())
                {
                    throw new Exception(
                        $"PopupsManagerConfig: prefab {popupPrefab.PopupId} must implement IGamePopup interface and PopupId must equal Type.Name of IGamePopup");
                }
            }

            if (!Application.isPlaying)
                Debug.Log("PopupsManagerConfig validation complete.");
        }

        public override void RegisterService(IContainerBuilder builder)
        {
            builder.RegisterEntryPoint<Popups.PopupsManager>().As<IPopupsManager>().WithParameter(this);
        }
    }
}