using System;
using System.Collections.Generic;
using GameReady.UI.Popups.Data;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace GameReady.UI.Popups
{
    public class PopupsManager :IPopupsManager,IDisposable,IInitializable
    {
        private readonly Data.PopupsManagerConfig _config;
        private readonly IObjectResolver _resolver;

        private readonly List<IGamePopup> _activePopups;
        private readonly Dictionary<string, IGamePopup> _popupsPool;
        
        public PopupsManager(Data.PopupsManagerConfig config,IObjectResolver resolver)
        {
            _config = config;
            _resolver = resolver;
            _popupsPool = new Dictionary<string, IGamePopup>();
            _activePopups = new List<IGamePopup>();
        }
        

        public void Initialize()
        {
            _config.Validate();
        }

        /// <summary>
        /// Popup script name must equal to prefab name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetOrCreatePopup<T>() where T : IGamePopup
        {
            var type = typeof(T);
            var popup = _activePopups.Find(p => p.Name == type.Name) ?? GetOrCreatePopupInternal(type.Name);
            popup.Name = type.Name;
            popup.OnClosed -= OnPopupClosedHandler;
            popup.OnClosed += OnPopupClosedHandler;
            _activePopups.Add(popup);
            if (popup is T typeLessPopup)
            {
                return typeLessPopup;
            }

            popup.Close();
            Debug.LogError("PopupsManager: GetOrCreatePopup<T> T type name must equal popupId");
            return default;
        }

        public void RemovePopup(string popupId)
        {
            var popup = _activePopups.Find(p => p.Name == popupId);
            popup?.Close();
        }

        private void OnPopupClosedHandler(IGamePopup popup)
        {
            _activePopups.Remove(popup);
            popup.OnClosed -= OnPopupClosedHandler;
            var popupInfo = GetPopupInfo(popup.Name);
            if (popupInfo.IsPoolable)
            {
                _popupsPool.TryAdd(popup.Name, popup);
            }
            else
            {
                popup.Dispose();
            }
        }


        private IGamePopup GetOrCreatePopupInternal(string popupId)
        {
            var popupInfo = GetPopupInfo(popupId);
            if (popupInfo != null)
            {
                if (popupInfo.IsPoolable && _popupsPool.TryGetValue(popupInfo.PopupId, out var @internal))
                {
                    return @internal;
                }

                return _resolver.Instantiate(popupInfo.PopupPrefab).GetComponent<IGamePopup>();
            }

            throw new Exception($"PopupsManager: {popupId} not exist in config");
        }

        private PopupInfoData GetPopupInfo(string id)
        {
            return _config.Popups.Find(p => p.PopupId == id);
        }

       

        public void Dispose()
        {
            _popupsPool.Clear();
            _activePopups.Clear();
        }
    }
}