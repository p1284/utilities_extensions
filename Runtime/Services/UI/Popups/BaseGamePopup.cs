using System;
using GameReady.UI.Popups.Data;
using UnityEngine;

namespace GameReady.UI.Popups
{
    public abstract class BaseGamePopup : MonoBehaviour, IGamePopup
    {
        public event Action<IGamePopup> OnClosed;
        
        public string Name
        {
            get => gameObject.name;
            set => gameObject.name = value;
        }

        protected abstract void SetSortOrder(int? order);

        public void Open(int? order = null)
        {
            SetSortOrder(order);
            OpenInternal();
        }

        public void Close()
        {
            CloseInternal();
        }

        protected virtual void CloseInternal()
        {
            gameObject.SetActive(false);
            FireOnClosed();
        }

        protected virtual void OpenInternal()
        {
            gameObject.SetActive(true);
        }

        protected void FireOnClosed()
        {
            OnClosed?.Invoke(this);
        }

        public virtual void Dispose()
        {
            Destroy(gameObject);
        }
    }
}