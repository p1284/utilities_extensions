using GameReady.UI.Popups.Data;

namespace GameReady.UI.Popups
{
    public interface IPopupsManager
    {
        T GetOrCreatePopup<T>() where T : IGamePopup;

        void RemovePopup(string popupId);
    }
}