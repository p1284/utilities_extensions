using System;
using System.Collections;
using GameReady.UI.Screens.Data;
using UnityEngine;
using UnityEngine.UI;

namespace GameReady.UI.Screens
{
    public abstract class BaseScreen : MonoBehaviour
    {
        internal GameScreenType ScreenType;
        
        internal IEnumerator HideRoutine(GameScreenType nextState, Action callback = null)
        {
            yield return HideInternal(nextState);
            callback?.Invoke();
        }

        internal IEnumerator ShowRoutine(GameScreenType prevState)
        {
            yield return ShowInternal(prevState);
        }
        
        protected virtual IEnumerator HideInternal(GameScreenType nextState)
        {
            yield return null;
        }

        protected virtual IEnumerator ShowInternal(GameScreenType prevState)
        {
            yield return null;
        }

        internal BaseScreen GetFromPoolInternal()
        {
            return GetFromPool();
        }

        internal void ReleaseToPoolInternal()
        {
            ReleaseToPool();
        }

        protected virtual BaseScreen GetFromPool()
        {
            gameObject.SetActive(true);
            return this;
        }

        protected virtual void ReleaseToPool()
        {
            gameObject.SetActive(false);
        }
        
    }
}