namespace GameReady.UI.Screens.Data
{
    public enum GameScreenType
    {
        Empty = 0,
        Loader,
        Preloader,
        Game,
        LevelFinish,
        LevelFailed,
        Lobby,
        Main,
        CustomScreen_1 = 100,
        CustomScreen_2 = 101,
        CustomScreen_3 = 102,
        CustomScreen_4 = 103,
        CustomScreen_5 = 104,
        
    }
}