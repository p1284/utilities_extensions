using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameReady.UI.Screens.Data
{
    [Serializable]
    public class ScreenState : IEquatable<ScreenState>
    {
        [SerializeField] private GameScreenType _screenType;

        [SerializeField] private ScreenTransitionsType _transitionsType;

        [SerializeField] private bool _poolable;

        [SerializeField] private GameObject _screenPrefab;

        public ScreenState(GameScreenType screenType)
        {
            _screenType = screenType;
            _transitionsType = ScreenTransitionsType.Immediate;
            _poolable = false;
        }

        public GameScreenType ScreenType => _screenType;

        public ScreenTransitionsType TransitionsType => _transitionsType;

        public bool Poolable => _poolable;

        public GameObject ScreenPrefab => _screenPrefab;
        
        public bool Equals(ScreenState other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _screenType == other._screenType;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ScreenState)obj);
        }

        public override int GetHashCode()
        {
            return (int)_screenType;
        }
    }
}