using System;
using System.Collections.Generic;
using System.Linq;
using Gameready.Utils.Services.Core;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace GameReady.UI.Screens.Data
{
    public class ScreensManagerConfig : BaseServiceConfig
    {
        [SerializeField] private GameScreenType _startUpScreen = GameScreenType.Loader;

        [SerializeField] private List<ScreenState> _screens;

        public GameScreenType StartUpScreen => _startUpScreen;

        public List<ScreenState> Screens => _screens;

        public void Validate()
        {
            _screens = _screens.Distinct().ToList();

            _screens.RemoveAll(s => s.ScreenType == GameScreenType.Empty);

            foreach (var screenState in _screens)
            {
                if (screenState.ScreenPrefab == null)
                {
                    throw new Exception(
                        $"ScreensManagerConfig: screen prefab for type:{screenState.ScreenType} not set.");
                }

                if (screenState.ScreenPrefab.GetComponent<BaseScreen>() == null)
                {
                    throw new Exception(
                        "ScreensManagerConfig: screen prefab must be inherited from BaseScreen component");
                }
            }

            if (!Application.isPlaying)
                Debug.Log("ScreensManagerConfig validation complete.");
        }

        public override void RegisterService(IContainerBuilder builder)
        {
            builder.RegisterEntryPoint<ScreensManager>().As<IScreensManager>().WithParameter(this);
        }
    }
}