namespace GameReady.UI.Screens.Data
{
    public readonly struct ScreenTypeData
    {
        public readonly GameScreenType Prev;
        public readonly GameScreenType Current;

        public ScreenTypeData(GameScreenType prev, GameScreenType current)
        {
            Prev = prev;
            Current = current;
        }
    }
}