namespace GameReady.UI.Screens.Data
{
    public enum ScreenTransitionsType
    {
        Immediate,
        Parallel,
        Sequential
    }
}