using System;
using GameReady.UI.Screens.Data;

namespace GameReady.UI.Screens
{
    public interface IScreensManager
    {
        event Action OnStateTransitionsComplete;

        int MaxScreens { get; }

        GameScreenType CurrentScreenType { get; }

        GameScreenType PrevScreenType { get; }

        bool IsInTransition { get; }

        void SetScreen(GameScreenType screenType, ScreenTransitionsType? transitionsType = null);
    }
}