using System;
using System.Collections;
using System.Collections.Generic;
using GameReady.UI.Screens.Data;
using Gameready.Utils;
using UnityEngine;
using VContainer;
using VContainer.Unity;
using Object = UnityEngine.Object;

namespace GameReady.UI.Screens
{
    public class ScreensManager : IScreensManager, IDisposable, IInitializable, IStartable
    {
        public event Action OnStateTransitionsComplete;

        private readonly ScreensManagerConfig _config;
        private readonly IObjectResolver _resolver;

        private readonly HashSet<BaseScreen> _pool;
        private readonly Queue<ScreenTypeData> _screensChangeQueue;

        private GameScreenType _prevScreenType;
        private GameScreenType _currentScreenType;

        private BaseScreen _currentScreen;
        private bool _isInTransition;
        private ScreenTransitionsType? _globalTransitionType;

        public int MaxScreens => _config.Screens.Count;

        public GameScreenType CurrentScreenType => _currentScreenType;

        public GameScreenType PrevScreenType => _prevScreenType;

        public bool IsInTransition => _isInTransition;

        public ScreensManager(ScreensManagerConfig config,IObjectResolver resolver)
        {
            _config = config;
            _resolver = resolver;
            _pool = new HashSet<BaseScreen>();
            _screensChangeQueue = new Queue<ScreenTypeData>();
        }


        public void Initialize()
        {
            _config.Validate();
        }

        public void Start()
        {
            SetScreen(_config.StartUpScreen);
        }

        public void SetScreen(GameScreenType screenType, ScreenTransitionsType? transitionsType = null)
        {
            _globalTransitionType = transitionsType;
            if (screenType == _currentScreenType) return;
            _prevScreenType = _currentScreenType;
            _currentScreenType = screenType;
            _screensChangeQueue.Enqueue(new ScreenTypeData(_prevScreenType, _currentScreenType));
            if (_isInTransition) return;
            _isInTransition = true;
            MonoHelper.StartCoroutine(StartStatesTransitions());
        }


        private IEnumerator StartStatesTransitions()
        {
            while (_screensChangeQueue.Count > 0)
            {
                var screenType = _screensChangeQueue.Dequeue();

                var prevScreenState = GetState(screenType.Prev);

                var transitionType = _globalTransitionType ?? prevScreenState.TransitionsType;

                var prevScreen = _currentScreen;

                if (prevScreen != null)
                {
                    switch (transitionType)
                    {
                        case ScreenTransitionsType.Immediate:
                            RemoveScreen(prevScreen, prevScreenState.Poolable);
                            break;
                        case ScreenTransitionsType.Parallel:
                            MonoHelper.StartCoroutine(prevScreen.HideRoutine(screenType.Current,
                                () => { RemoveScreen(prevScreen, prevScreenState.Poolable); }));
                            break;
                        case ScreenTransitionsType.Sequential:
                            yield return prevScreen.HideRoutine(screenType.Current,
                                () => RemoveScreen(prevScreen, prevScreenState.Poolable));
                            break;
                    }
                }

                var currentState = GetState(screenType.Current);

                transitionType = _globalTransitionType ?? currentState.TransitionsType;

                _currentScreen = GetOrCreateScreen(currentState);

                if (_currentScreen != null)
                {
                    switch (transitionType)
                    {
                        case ScreenTransitionsType.Parallel:
                            MonoHelper.StartCoroutine(_currentScreen.ShowRoutine(screenType.Prev));
                            break;
                        case ScreenTransitionsType.Sequential:
                            yield return _currentScreen.ShowRoutine(screenType.Prev);
                            break;
                    }
                }
                else
                {
                    Debug.LogError($"{this.GetType().Name}: ScreenType-{screenType.Current} not exist.");
                }
            }

            _isInTransition = false;
            OnStateTransitionsComplete?.Invoke();
        }

        private void RemoveScreen(BaseScreen screen, bool addToPool)
        {
            if (addToPool)
            {
                screen.ReleaseToPoolInternal();
                _pool.Add(screen);
            }
            else
            {
                Object.Destroy(screen.gameObject);
            }
        }

        private BaseScreen GetOrCreateScreen(ScreenState screenState)
        {
            var screen = screenState.Poolable ? FindScreen(screenState.ScreenType) : null;
            if (screen == null)
            {
                if (screenState.ScreenPrefab != null)
                {
                    screen = _resolver.Instantiate(screenState.ScreenPrefab).GetComponent<BaseScreen>();
                    Object.DontDestroyOnLoad(screen.gameObject);
                    screen.ScreenType = screenState.ScreenType;
                }

                return screen;
            }

            return screenState.Poolable ? screen.GetFromPoolInternal() : screen;
        }

        private BaseScreen FindScreen(GameScreenType screenType)
        {
            foreach (var screen in _pool)
            {
                if (screen.ScreenType == screenType)
                {
                    return screen;
                }
            }

            return null;
        }


        public void Dispose()
        {
            _pool.Clear();
            _screensChangeQueue.Clear();
        }

        private ScreenState GetState(GameScreenType type)
        {
            return _config.Screens.Find(s => s.ScreenType == type) ?? new ScreenState(type);
        }
    }
}