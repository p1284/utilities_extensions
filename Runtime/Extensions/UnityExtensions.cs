using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public static class UnityExtensions
{
    public static bool CheckColliderLayer(this Collider collider, LayerMask layerMask)
    {
        return ((1 << collider.gameObject.layer) | layerMask) == layerMask;
    }

    public static bool CheckTags(this Collider collider, string[] tags, out int tagIndex)
    {
        tagIndex = -1;
        for (var i = 0; i < tags.Length; i++)
        {
            var tag = tags[i];
            if (collider.CompareTag(tag))
            {
                tagIndex = i;
                return true;
            }
        }

        return false;
    }

    public static bool CheckTags(this Collider collider, string[] tags)
    {
        for (var i = 0; i < tags.Length; i++)
        {
            var tag = tags[i];
            if (collider.CompareTag(tag))
            {
                return true;
            }
        }

        return false;
    }


    public static bool IsInLayerMask(this LayerMask lasyerMask, int layer)
    {
        return lasyerMask == (lasyerMask | (1 << layer));
    }

    public static int LayerMaskToLayer(this LayerMask layerMask)
    {
        int layerNumber = 0;
        int layer = layerMask.value;
        while (layer > 0)
        {
            layer = layer >> 1;
            layerNumber++;
        }

        return layerNumber - 1;
    }

    public static float GetMaxAxisSize(this Bounds bounds)
    {
        return Mathf.Max(bounds.size.x, bounds.size.y, bounds.size.z);
    }

    public static void DestroyChildren(this Transform holder, List<GameObject> exludeList = null)
    {
        foreach (Transform child in holder)
        {
            if (exludeList != null && exludeList.Contains(child.gameObject)) continue;
            UnityEngine.Object.Destroy(child.gameObject);
        }

        Canvas.ForceUpdateCanvases();
    }

    public static void DestroyChildrenImmediate(this Transform holder, List<GameObject> exludeList = null)
    {
        var list = holder.Cast<Transform>().ToList();
        foreach (Transform child in list)
        {
            if (exludeList != null && exludeList.Contains(child.gameObject)) continue;
            UnityEngine.Object.DestroyImmediate(child.gameObject);
        }
    }

    public static Bounds GetBounds(this Transform holder, string childName)
    {
        Transform child = holder.Find(childName);
        if (child != null)
        {
            return GetBounds(child);
        }

        return GetBounds(holder);
    }

    public static void SetAlpha(this Image image, float alpha)
    {
        var color = image.color;
        color.a = alpha;
        image.color = color;
    }

    public static void SetLayerRecursively(this GameObject obj, int layer)
    {
        obj.layer = layer;

        foreach (Transform child in obj.transform)
        {
            child.gameObject.SetLayerRecursively(layer);
        }
    }

    public static void SetTagRecursively(this GameObject gameObject, string tag)
    {
        gameObject.tag = tag;
        foreach (Transform transform in gameObject.transform)
        {
            transform.gameObject.SetTagRecursively(tag);
        }
    }

    public static float Height(this Bounds bounds)
    {
        return Mathf.Abs(bounds.size.y);
    }

    public static float Width(this Bounds bounds)
    {
        return Mathf.Abs(bounds.size.x);
    }

    public static Bounds GetCalculatedBounds(this Transform holder)
    {
        var bounds = new Bounds();
        Collider[] colliders = holder.GetComponentsInChildren<Collider>();
        if (colliders.Length > 0)
        {
            foreach (var collider in colliders)
            {
                bounds.Encapsulate(collider.bounds);
            }
        }
        else
        {
            Renderer[] renderers = holder.GetComponentsInChildren<Renderer>();
            if (renderers.Length > 0)
            {
                foreach (var renderer in renderers)
                {
                    bounds.Encapsulate(renderer.bounds);
                }
            }
        }

        return bounds;
    }

    public static Bounds GetBounds(this Transform holder)
    {
        Collider collider = holder.GetComponent<Collider>();
        if (collider != null)
        {
            return collider.bounds;
        }

        Collider2D col = holder.transform.GetComponent<Collider2D>();
        if (col != null)
        {
            return col.bounds;
        }

        Renderer renderer = holder.GetComponent<Renderer>();
        if (renderer != null)
        {
            return renderer.bounds;
        }

        return new Bounds(Vector3.one * 0.5f, Vector3.one);
    }

    public static GameObject AddChild(this Transform holder, GameObject prefab)
    {
        return Object.Instantiate(prefab, Vector3.zero, Quaternion.identity, holder);
    }

    public static GameObject AddChild(this Transform holder, string goName)
    {
        var go = new GameObject(goName);
        go.transform.SetParent(holder);
        return go;
    }

    public static GameObject AddNewChild(this Transform holder, string name)
    {
        var go = new GameObject(name);
        go.transform.SetParent(holder, false);
        go.transform.position = Vector3.zero;
        go.transform.rotation = Quaternion.identity;
        go.transform.localScale = Vector3.one;

        return go;
    }

    public static GameObject AddChild(this RectTransform holder, GameObject prefab)
    {
        return Object.Instantiate(prefab, Vector2.zero, Quaternion.identity, holder);
    }
    
    public static void SnapUIToWorldPos(this RectTransform uiElement, Camera camera, Vector3 wPos,
        RectTransform canvasRect)
    {
        var fromScreenPoint1 = camera.WorldToScreenPoint(wPos);

        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRect,
            fromScreenPoint1,
            camera,
            out Vector2 localPoint);


        uiElement.transform.localPosition = localPoint;
    }
}