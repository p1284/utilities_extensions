﻿using UnityEngine;

namespace Gameready.Utils
{
    public static class CameraExtensions
    {
        private static Vector2 _frustumSize;

        public static Vector2 CalculateFrustum(this Camera camera, float distance, bool recalculate = false)
        {
            if (!recalculate) return _frustumSize;

            var frustumHeight = Mathf.Abs(2.0f * distance *
                                          Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad));

            var frustumWidth = frustumHeight * camera.aspect;

            _frustumSize = new Vector2(frustumWidth, frustumHeight);
            return _frustumSize;
        }

        public static void FitPerspective(this Camera camera, float cameraDistance, Bounds bounds)
        {
            Vector3 objectSizes = bounds.max - bounds.min;
            float objectSize = Mathf.Max(objectSizes.x, objectSizes.y, objectSizes.z);
            float cameraView =
                2.0f * Mathf.Tan(0.5f * Mathf.Deg2Rad * camera.fieldOfView); // Visible height 1 meter in front
            float distance = cameraDistance * objectSize / cameraView; // Combined wanted distance from the object
            distance += 0.5f * objectSize; // Estimated offset from the center to the outside of the object
            camera.transform.position = -distance * camera.transform.forward;
        }

        public static float CalculateOrtographicSize(this Camera cam, float width)
        {
            float aspect = Screen.width > Screen.height
                ? Screen.height / (float) Screen.width
                : Screen.width /
                  (float) Screen.height; // not using cam.aspect because it is not always updated immediately
            return width * aspect * 0.5f;
        }

        public static Vector2 GetOrthoSize(this Camera cam)
        {
            float aspect = Screen.width > Screen.height
                ? Screen.height / (float) Screen.width
                : Screen.width /
                  (float) Screen.height; // not using cam.aspect because it is not always updated immediately

            float height = 2f * cam.orthographicSize;
            float width = height * aspect;

            return new Vector2(width, height);
        }

        public static Bounds OrthographicBounds(this Camera camera)
        {
            float screenAspect =
                Screen.width /
                (float) Screen.height; // not using cam.aspect because it is not always updated immediately
            float cameraHeight = camera.orthographicSize * 2f;
            Bounds bounds = new Bounds(
                camera.transform.position,
                new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
            return bounds;
        }

        public static void CalcFOV(this Camera camera, float defaultFov, float aspect)
        {
            camera.fieldOfView = defaultFov * aspect / ((float) camera.pixelWidth / camera.pixelHeight);
        }
    }
}