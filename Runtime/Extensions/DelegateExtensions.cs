using System;

namespace Gameready.Utils
{
    public static class DelegateExtensions
    {
        /// <summary>
        /// Simple check
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static bool IsAnonymous(this Delegate action)
        {
            return action.Method.Name.Contains("<");
        }

        public static void RemoveAll(this Action listener)
        {
            var list = listener?.GetInvocationList();

            if (list != null)
            {
                foreach (var @delegate in list)
                {
                    listener -= (Action) @delegate;
                }
            }
        }

        public static void RemoveAll<T>(this Action<T> listener)
        {
            var list = listener?.GetInvocationList();

            if (list != null)
            {
                foreach (var @delegate in list)
                {
                    listener -= (Action<T>) @delegate;
                }
            }
        }
    }
}