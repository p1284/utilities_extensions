using System.Collections.Generic;
using Gameready.Helpers;

namespace Gameready.Utils
{
    public static class JsonUtitlityExtensions
    {
        public static string ToJson<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, bool prettyPrint = false)
        {
            return JsonUtilityDictionary<TKey, TValue>.ToJson(dictionary, prettyPrint);
        }

        public static void FromJsonOverwrite<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, string json)
        {
            JsonUtilityDictionary<TKey, TValue>.FromJson(json, ref dictionary);
        }
    }
}