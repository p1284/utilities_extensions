# Changelog
## [3.0.5] - 2024-06-02
- improvements for ItemsSystem
- add HashItemsDb for group HashItemsStorages
- add StorageName to HashItemIdsSelectorAttribute
- improvements and bugfixes HashItemId property drawers and Editors

## [3.0.4] - 2024-29-01
- add new helper components for physics collision/trigger events 
- add Collider extension methods
- refactor and updates PropertyDrawers, improve UIElements usages
- fix and improve ButtonAttribute usages
- add new flexible ItemsSystem(HashItemsDataStorage,HashItemData) for any entities 

## [3.0.3] - 2023-06-11
- refactor FSM,BaseState
- FSMBase improvements:
  - add RemoveState with fallback
  - ~~add can transition check functor to constructor~~
  - add optional internal logs
  - support structs for IState
  - improvements for inheritance(AddState,RemoveState)

## [3.0.2] - 2023-28-10
- rename StateMachineGeneric to SimpleEnumFSM
- add new complex FSM with struct classes as states

## [3.0.1] - 2023-15-10
- bug fixes
- improve MonoHelper batch updaters
- add FastRemoveList with new list extensions

## [3.0.0] - 2023-09-10
- Add all managers Saves,UI,Ads,Inapp,Analytics.. inside
- add Cheats package
- add VContainer,Unitask dependencies

## [2.0.2] - 2023-05-10
- Add BaseObjectPoolGeneric pool helper for Unity ObjectPool
- refactor and improve Upgrade functions

## [2.0.1] - 2023-11-09
- remove all Services, move them to other packages
- remove BuildScripts
- remove Cheats
- add ReflectionUtils
- KeyValue label view fixes

## [2.0.0] - 2023-08-09
- add Saves,Inventory,Currency,Analytics,IAP,Ads,Screens,Popups services
- add Build settings with BuildScript
- add UIToolkit Cheats
- new editor menu from GameReady

## [1.1.0] - 2023-07-09
- add GameRoot ServiceLocator

## [1.0.9] - 2023-10-08
- add CryptoUtils with AES Encrypt/Decrypt implementation
- add Update method to KeyValueDictionary

## [1.0.8] - 2023-09-08
- Add GenericLootDropTable,GenericLootDropItem and LootDropTable sample

## [1.0.7] - 2023-26-07
- Add Range data type for min,max helper with UIElements Property drawer
- Add new <code>GameRandomType.Unity_Random</code> and make it by Default for GameRandom
- Refactor MathUtils, make it partial for Geometry,Grid math.

## [1.0.6] - 2023-13-07
- Add Custom GameReadyEditor with UIToolkit inspector
- Update KeyValue drawer to UIToolkit
- add KeyValueDictionary helper and drawer
- update ButtonAttribute,ReadOnlyAttribute
- add TagAttribute
- add AnimationCurveEaseDrawer
- update Unity to 2022.3.4x

## [1.0.5] - 2023-30-06
- Remove NaughtyAttributes dependencies
- Improve EditorHelpers
- fix AddressablesHelper
- refactor and improve KeyValuePropertyDrawer
- add ButtonAttribute for methods in inspector
- add ReadOnlyAttribute
- Add Global GameReadeEditor for all custom editors

## [1.0.4] - 2023-22-03
- add PauseFocus change to MonoHelper
- add StateMachineGeneric FSM helper, add samples how to use
- EditorHelpers improvements
- KeyValue data add FillList,FillDictionary helper methods

## [1.0.3] - 2023-21-03
- add CreateRandomGenerator to GameRandom
- add UIFlippable helper
- add GizmoUtils
- add ScriptOrderAttribute
- add NetworkCheck helper
- add AnimationFrameEvent,AnimatorStateBehavior helpers
- add BillBoard helper
- Update MathUtils
- improve MonoHelper, add AutoInit,Manual Init Editor options

## [1.0.2] - 2022-24-12
- change name to GameReadyPack
- add LoopInt
- add UniqueRandomRange
- refactor namespaces
- impelement PCG random refactor GameRandom
- refactor bugfixes
- add tests for GameRandom

## [1.0.1] - 2022-07-03
- refactor
- add GameExtensions
- add GameRandom
- implement SimpleGameRandom
- implement MersenneTwister random
- add Editor unused sprites finder SpriteAtlas

## [1.0.0] - 2021-16-10
- CopyToFolder Improvements

## [1.0.0] - 2021-03-10
### First Release pack
- Add Extensions for List,Unity,Reflection,Camera,Deletate
- Add ColorUtils,MonoHelper,FileStorageUtils,MathUtils
- Add JsonUtilityArray - hack for JsonUtility array parse
- Add commonly use EditorHelpers in Editor stuff
- Add Fix for empty folders with metas, Remove Empty Folder menu command
- Add CopyLightningSettings workaround , works < Unity 2020