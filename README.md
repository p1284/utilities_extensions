
# GameReady Pack #

Packages contains:

 - EditorHelpers,DefinesHelper,UnityPackagesHelper
 - KeyValue,LoopInt data types...
 - Animation,Animator helpers
 - Many InGame Helpers
 - Math,Color,FileStorage utils
 - Extensions,List,Unity,Camera,Delegates,Game
 - Simple CoroutineManager with Timers,DelayCall,Update,Pause callbacks
 - GameRandom with PRNG implementations    
    - System.Random
    - Mersenne Twister
    - PCG
 - Core VContainer services, with default Saves,UI,Build managers
 - VContainer, Unitask dependencies


License
MIT License

Copyright © 2023 Gameready
