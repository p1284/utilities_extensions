using System.Collections.Generic;
using Gameready.Random;
using UnityEngine;
using UnityEngine.Assertions;

namespace Tests.Runtime
{
    public class RandomTests
    {
        private const int Seed = 0;
        
        private Vector2Int _minMax = new Vector2Int(0, 10);

        [NUnit.Framework.Test]
        public void RandomUnityPass()
        {
            GameRandom.SetupRandom(GameRandomType.Unity_Random,Seed);
            var checkValue = new int[] { 6, 6, 2, 2, 1 };
            // Use the Assert class to test conditions.
            for (int i = 0; i < checkValue.Length; i++)
            {
                var value = GameRandom.Range(_minMax.x, _minMax.y);
                Debug.Log("Unity random: " + value);
                Assert.AreEqual(checkValue[i], value);
            }
        }
        
        [NUnit.Framework.Test]
        public void RandomSystemPass()
        {
            GameRandom.SetupRandom(GameRandomType.System_Random,Seed);
            
            var checkValue = new int[] { 7, 8, 7, 5, 2 };
            // Use the Assert class to test conditions.
            for (int i = 0; i < checkValue.Length; i++)
            {
                var value = GameRandom.Range(_minMax.x, _minMax.y);
                Debug.Log("System random: " + value);
                Assert.AreEqual(checkValue[i], value);
            }
        }

        [NUnit.Framework.Test]
        public void RandomMersenneTwisterPass()
        {
            GameRandom.SetupRandom(GameRandomType.Mersenne_Twister,Seed);
            
            var checkValue = new int[] { 5, 7, 6, 5, 4 };
            // Use the Assert class to test conditions.
            for (int i = 0; i < checkValue.Length; i++)
            {
                var value = GameRandom.Range(_minMax.x, _minMax.y);
                Debug.Log("MersenneTwister random: " + value);
                Assert.AreEqual(checkValue[i], value);
            }
        }

        [NUnit.Framework.Test]
        public void RandomPCGPass()
        {
            GameRandom.SetupRandom(GameRandomType.PCG_Random,Seed);
            
            var checkValue = new int[] { 2, 3, 4, 8, 3 };
            // Use the Assert class to test conditions.
            for (int i = 0; i < checkValue.Length; i++)
            {
                var value = GameRandom.Range(_minMax.x, _minMax.y);
                Debug.Log("PCG random: " + value);
                Assert.AreEqual(checkValue[i], value);
            }
        }

        // // A UnityTest behaves like a coroutine in PlayMode
        // // and allows you to yield null to skip a frame in EditMode
        // [UnityEngine.TestTools.UnityTest]
        // public System.Collections.IEnumerator RandomTestsWithEnumeratorPasses()
        // {
        //     // Use the Assert class to test conditions.
        //     // yield to skip a frame
        //     yield return null;
        // }
    }
}