using System.Text;
using Gameready.Gameplay;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.Windows
{
    public class UpgradeCalcWindow : EditorWindow
    {
        private TextField _textField;
        private ScrollView _scrollView;

        public static void Show<T>(string title, T priceUpgrade) where T:Upgrade
        {
            var wnd = GetWindow<UpgradeCalcWindow>();
            wnd._upgradeField = priceUpgrade;
            wnd.UpdateTextField(title);
            wnd.titleContent = new GUIContent("Upgrade Calculation Tester");
            wnd.minSize = new Vector2(200, 200);
        }

        private Upgrade _upgradeField;

        public void CreateGUI()
        {
            _scrollView = new ScrollView(ScrollViewMode.Vertical);
            _scrollView.style.flexGrow = 1f;
            
            _textField = new TextField("Result");
            _textField.style.flexGrow = 1f;
            _textField.style.flexShrink = 1f;
            _textField.style.unityTextAlign = new StyleEnum<TextAnchor>(TextAnchor.UpperLeft);
            _textField.multiline = true;
            _textField.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Column);
            _scrollView.Add(_textField);

            rootVisualElement.Add(_scrollView);
        }

        protected virtual void UpdateTextField(string upgradeTitle)
        {
            _textField.label = upgradeTitle;
            var sb = new StringBuilder();
            if (_upgradeField is PriceUpgrade priceUpgrade)
            {
                sb.AppendLine("PRICE");
                for (int i = 1; i <= priceUpgrade.MaxUpgrades; i++)
                {
                    sb.AppendLine($"Level:{i} Price:{priceUpgrade.GetPrice(i)}");
                }

                sb.AppendLine();
            }
            
            
            sb.AppendLine("UPGRADES");
            for (int i = 1; i <= _upgradeField.MaxUpgrades; i++)
            {
                sb.AppendLine($"Level:{i} Upgrade:{_upgradeField.GetUpgradeValue(i)}");
            }

            _textField.value = sb.ToString();
        }
    }
}