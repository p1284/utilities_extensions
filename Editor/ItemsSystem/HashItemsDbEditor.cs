using System;
using System.Linq;
using Gameready.Utils.ItemsSystem;
using UnityEditor;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.ItemsSystem
{
    [CustomEditor(typeof(HashItemsDb))]
    public class HashItemsDbEditor : GameReadyEditor
    {
        private HashItemsDb _hashItemsDb;

        private void OnEnable()
        {
            _hashItemsDb = target as HashItemsDb;
        }

        public override VisualElement CreateInspectorGUI()
        {
            var container = base.CreateInspectorGUI();

            var btn = new Button(GenerateConstClass);
            btn.text = "Generate Constants For StorageNames";

            container.Add(btn);

            return container;
        }

        private void GenerateConstClass()
        {
            var constValues = _hashItemsDb.ItemsDataStorage.Select(storage => storage.name).ToArray();
            ConstClassGenerator.GenerateConstClassFile("Create Items StorageNames Constants", $"{_hashItemsDb.name}Constants",
                $"{_hashItemsDb.name}Constants", constValues);
        }
    }
}