using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameReady.EditorTools.PropertyDrawers.ItemsSystem;
using GameReady.EditorTools.UIElements;
using Gameready.Utils.ItemsSystem;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.ItemsSystem
{
    [CustomEditor(typeof(HashItemsDataStorage))]
    public class HashItemsDataStorageEditor : UnityEditor.Editor
    {
        private HashItemsDataStorage _dataStorage;
        
        private void OnEnable()
        {
            _dataStorage = target as HashItemsDataStorage;
        }


        public override VisualElement CreateInspectorGUI()
        {
            var visualTreeAsset =
                UIElementTemplatesData.LoadAssetTree(UIElementTemplatesData.HashItemsDataStorageEditorGuid);

            var root = new VisualElement();

            root.style.flexGrow = 1f;
            root.style.flexShrink = 1f;

            visualTreeAsset.CloneTree(root);

            var itemNameField = root.Q<TextField>("itemNameField");


            var itemsList = root.Q<ScrollView>("itemsList");
            itemsList.contentContainer.style.flexGrow = 1f;

            FillItems(itemsList, serializedObject.FindProperty("_items"));

            var createItemButton = root.Q<Button>("createBtn");
            createItemButton.RegisterCallback<MouseUpEvent>(evt =>
                CreateItem(itemNameField, itemsList, serializedObject.FindProperty("_items")));

            var validateButton = root.Q<Button>("validateBtn");
            validateButton.RegisterCallback<MouseUpEvent>(evt =>
            {
                ValidateItemsHandler(itemsList,serializedObject.FindProperty("_items"));
            });

            var saveAssetButton = root.Q<Button>("saveAssetBtn");
            saveAssetButton.RegisterCallback<MouseUpEvent>(evt =>
                SaveAsset(itemsList, serializedObject.FindProperty("_items")));

            var saveToJson = root.Q<Button>("saveJsonBtn");
            saveToJson.RegisterCallback<MouseUpEvent>(evt => SaveDataStorageToJson());

            var loadFromJson = root.Q<Button>("loadJsonBtn");
            loadFromJson.RegisterCallback<MouseUpEvent>(evt =>
                LoadFromJson(itemsList, serializedObject.FindProperty("_items")));

            var backButton = root.Q<Button>("backBtn");
            backButton.RegisterCallback<MouseUpEvent>(evt =>
            {
                EditorHelpers.SelectAssetByType<HashItemsDb>();
            });

            return root;
        }

        private void LoadFromJson(ScrollView itemsList, SerializedProperty listProperty)
        {
            var result = EditorUtility.OpenFilePanel("Load HashItemsDataStorage",
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "json");

            if (!string.IsNullOrEmpty(result))
            {
                try
                {
                    var json = File.ReadAllText(result);
                    JsonUtility.FromJsonOverwrite(json, _dataStorage);

                    SaveAsset(itemsList, listProperty);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }

        private void SaveDataStorageToJson()
        {
            var result = EditorUtility.SaveFilePanel("Save HashItemsDataStorage",
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop), _dataStorage.name, "json");

            if (!string.IsNullOrEmpty(result))
            {
                try
                {
                    File.WriteAllText(result, JsonUtility.ToJson(_dataStorage, true));
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }

        private void SaveAsset(ScrollView itemsList, SerializedProperty listProperty)
        {
            _dataStorage.SortByName();

            listProperty.serializedObject.Update();

            FillItems(itemsList, listProperty);

            EditorUtility.SetDirty(_dataStorage);
            AssetDatabase.SaveAssets();
        }


        private void FillItems(ScrollView itemsList, SerializedProperty itemsProperty)
        {
            itemsList.Clear();
            for (int i = 0; i < itemsProperty.arraySize; i++)
            {
                var prop = itemsProperty.GetArrayElementAtIndex(i);
                var item = HashItemIDPropertyDrawer.CreateDefaultHashItemId("", Color.cyan, false);
                item.BindProperty(prop);
                itemsList.userData = prop.name;
                itemsList.Add(item);
            }
        }

        private void CreateItem(TextField itemNameField, ScrollView container, SerializedProperty listProperty)
        {
            if (string.IsNullOrEmpty(itemNameField.value)) return;

            var names = itemNameField.value.Split(',');

            if (names.Contains(HashItemId.EmptyItemId.Name)) return;

            for (int i = 0; i < names.Length; i++)
            {
                var itemName = names[i].Trim();
                var hashItem = _dataStorage.GetItem(itemName);

                if (!hashItem.Equals(HashItemId.EmptyItemId))
                {
                    Debug.LogError($"{_dataStorage.GetType().Name} hashItem with name:{itemName} already exist!");
                    continue;
                }

                var index = listProperty.arraySize;
                //
                listProperty.InsertArrayElementAtIndex(index);
                var prop = listProperty.GetArrayElementAtIndex(index);
                prop.boxedValue = HashItemId.Create(itemName, _dataStorage.name);
                prop.serializedObject.ApplyModifiedProperties();

                var item = HashItemIDPropertyDrawer.CreateDefaultHashItemId("", Color.green, false);
                item.BindProperty(prop);
                container.Add(item);
            }
        }


        private void ValidateItemsHandler(ScrollView scrollView,SerializedProperty property)
        {
            var hashSet = new HashSet<HashItemId>();
            _dataStorage.Items.ToList().ForEach(id => hashSet.Add(id));
            
            property.ClearArray();

            var index = 0;
            foreach (var hashItemId in hashSet)
            {
                property.InsertArrayElementAtIndex(index);
                var prop = property.GetArrayElementAtIndex(index);
                prop.boxedValue = HashItemId.Create(hashItemId.Name, _dataStorage.name);
                index++;
            }

            property.serializedObject.ApplyModifiedProperties();
            property.serializedObject.Update();
            
            FillItems(scrollView,property);
        }
    }
}