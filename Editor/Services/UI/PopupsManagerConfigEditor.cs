using GameReady.EditorTools;
using GameReady.UI.Popups;
using GameReady.UI.Popups.Data;
using UnityEditor;
using UnityEngine;

namespace GameReady.Saves.Editor
{
    [CustomEditor(typeof(PopupsManagerConfig))]
    public class PopupsManagerConfigEditor : UnityEditor.Editor
    {
        private const string GdprPopupGUID = "1ee813d6ccdc2204fa52d4497d9f23a7";

        public override void OnInspectorGUI()
        {
            EditorGUILayout.HelpBox("Popup Prefab Name must be equal to Script class name", MessageType.Info);

            base.OnInspectorGUI();

            if (GUILayout.Button("Set Popup Ids"))
            {
                SetPopupIds();
            }

            EditorGUILayout.Space();


            if (GUILayout.Button("Add GdprPopup Variant"))
            {
                var popupsProperty = serializedObject.FindProperty("_popups");
                for (int i = 0; i < popupsProperty.arraySize; i++)
                {
                    var item = popupsProperty.GetArrayElementAtIndex(i);
                    var popupInfoData = item.boxedValue as PopupInfoData;
                    if (popupInfoData.PopupPrefab != null && popupInfoData.PopupPrefab.GetComponent<GdprPopup>())
                    {
                        return;
                    }
                }

                var path = AssetDatabase.GUIDToAssetPath(GdprPopupGUID);
                ;

                var gdprPopupOriginal = AssetDatabase.LoadAssetAtPath<GameObject>(path);

                if (gdprPopupOriginal != null)
                {
                    var gdprPrefabVariant = EditorHelpers.CreatePrefabInProject(gdprPopupOriginal, "GdprPopup", true);
                    popupsProperty.InsertArrayElementAtIndex(popupsProperty.arraySize);
                    popupsProperty.GetArrayElementAtIndex(popupsProperty.arraySize - 1).boxedValue = new PopupInfoData()
                    {
                        PopupPrefab = gdprPrefabVariant
                    };

                    serializedObject.ApplyModifiedProperties();
                }
            }
        }

        private void SetPopupIds()
        {
            var popupsProperty = serializedObject.FindProperty("_popups");

            for (int i = 0; i < popupsProperty.arraySize; i++)
            {
                var item = popupsProperty.GetArrayElementAtIndex(i);
                var popupInfoData = item.boxedValue as PopupInfoData;
                if (popupInfoData.PopupPrefab != null)
                {
                    popupInfoData.PopupId = popupInfoData.PopupPrefab.GetComponent<IGamePopup>().Name;
                }

                item.boxedValue = popupInfoData;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}