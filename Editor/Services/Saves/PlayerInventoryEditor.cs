using GameReady.EditorTools;
using GameReady.Inventory;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace GameReady.Saves.Editor
{
    [CustomEditor(typeof(PlayerInventoryConfig))]
    public class PlayerInventoryEditor : UnityEditor.Editor
    {
        private PlayerInventoryConfig _playerInventoryConfig;

        private void OnEnable()
        {
            _playerInventoryConfig = (PlayerInventoryConfig)target;
        }

        public override VisualElement CreateInspectorGUI()
        {
            var root = new VisualElement();

            InspectorElement.FillDefaultInspector(root, serializedObject, this);

            var validateButton = new Button(() => { _playerInventoryConfig.ValidateEditorOnly(); })
            {
                text = "Validate"
            };

            var generateConstsButton = new Button(() =>
            {
                _playerInventoryConfig.ValidateEditorOnly();

                var itemsIds = _playerInventoryConfig.GetAllItemIds;

                ConstClassGenerator.GenerateConstClassFile("Save Inventory Items Constants", "InventoryItems",
                    "InventoryItems", itemsIds);
            })
            {
                text = "Generate InventoryItems Const"
            };

            var generateItemPacks = new Button(() =>
            {
                _playerInventoryConfig.ValidateEditorOnly();

                var itemPackIds = _playerInventoryConfig.GetAllItemPackIds;

                ConstClassGenerator.GenerateConstClassFile("Save Inventory ItemPacks Constants", "InventoryItemPacks",
                    "InventoryItems", itemPackIds);
            });
            generateItemPacks.text = "Generate InventoryItemPacks Const";

            var saveButton = new Button(() =>
            {
                EditorUtility.SetDirty(_playerInventoryConfig);
                AssetDatabase.SaveAssets();
            });

            saveButton.text = "Save Config";


            root.Add(validateButton);
            root.Add(generateConstsButton);
            root.Add(generateItemPacks);
            root.Add(saveButton);


            return root;
        }
    }
}