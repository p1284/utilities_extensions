using GameReady.Currency;
using GameReady.EditorTools;
using GameReady.Inventory;
using UnityEditor;

namespace GameReady.Saves.Editor
{
    public static class SavesEditorSetup
    {
        // [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Saves/Create GameSavesManager",false,0)]
        // public static void CreateGameSaves()
        // {
        //     EditorHelpers.CreateSceneInstance<GameSaveManager>("GameSaves");
        // }

        // [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Saves/GetOrCreate Persistent Save Manager",false,0)]
        // public static void CreatePersistentSavesServiceConfig()
        // {
        //     EditorHelpers.CreateScriptableAsset<PersistentSavesManager>(true, true);
        // }
        //
        // [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Saves/GetOrCreate PlayerInventory",false,0)]
        // public static void CreatePlayerInventory()
        // {
        //     EditorHelpers.CreateScriptableAsset<PlayerInventory>(true, true);
        // }
        //
        // [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Saves/GetOrCreate CurrencyBalance",false,0)]
        // public static void CreateCurrencyBalance()
        // {
        //     EditorHelpers.CreateScriptableAsset<CurrencyBalance>(true, true);
        // }

#if CHEATS
        // [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Saves/Create Saves Cheats", false, 0)]
        // public static void CreateSavesCheats()
        // {
        //     EditorHelpers.CreateScriptableAsset<GameReady.Saves.Cheats.SavesCheat>(true, true);
        // }
        //
        // [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Saves/Create Currency Cheats", false, 0)]
        // public static void CreateCurrencyBalanceCheats()
        // {
        //     EditorHelpers.CreateScriptableAsset<GameReady.Saves.Cheats.CurrencyCheats>(true, true);
        // }
        //
        // [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Saves/Create PlayerInventory Cheats", false, 0)]
        // public static void CreateInventoryCheats()
        // {
        //     EditorHelpers.CreateScriptableAsset<GameReady.Saves.Cheats.InventoryCheats>(true, true);
        // }
#endif
    }
}