using GameReady.Saves.Data;
using UnityEditor;
using UnityEngine;

namespace GameReady.Saves.Editor
{
    [CustomEditor(typeof(PersistentSavesConfig))]
    public class PersistentSavesConfigEditor : UnityEditor.Editor
    {
        private PersistentSavesConfig _persistentSavesConfig;

        private void OnEnable()
        {
            if (target == null) return;
            _persistentSavesConfig = (PersistentSavesConfig)target;
        }


        public override void OnInspectorGUI()
        {
            var encryptionType = serializedObject.FindProperty("_encryptionType");

            EditorGUILayout.PropertyField(encryptionType);

            if ((SaveEncryptionType)encryptionType.enumValueIndex == SaveEncryptionType.AES)
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("_aesSeed"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("_aesCount"));
            }

            serializedObject.ApplyModifiedProperties();

            base.OnInspectorGUI();
            
            if (GUILayout.Button("Save Config"))
            {
               // _persistentSavesConfig.Validate();
                EditorUtility.SetDirty(_persistentSavesConfig);
                AssetDatabase.SaveAssets();
            }

            EditorGUILayout.Separator();
        }
    }
}