using UnityEditor;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.UIElements
{
    internal static class UIElementTemplatesData
    {
        public const string KeyValueDictionaryGuid = "9f6af2a85dd7c4f7da84d06a71fa395f";
        public const string HashItemsDataStorageEditorGuid = "5fb279759a6114b21b69518f5cf4b48c";

        public static VisualTreeAsset LoadAssetTree(string guid)
        {
            var path = AssetDatabase.GUIDToAssetPath(guid);
            return AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(path);
        }
    }
}