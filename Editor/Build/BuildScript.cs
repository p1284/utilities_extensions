using GameReady.Build;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

namespace GameReady.Editor.Build
{
    public class BuildScript : IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
        public int callbackOrder => 0;

        private BuildSettings _buildSettings;

        public void OnPostprocessBuild(BuildReport report)
        {
            _buildSettings = BuildSettings.Instance;

            _buildSettings.FillFromProjectSettings();

            _buildSettings.DebugBuild = (report.summary.options & BuildOptions.Development) != 0;

#if UNITY_ANDROID
            if (_buildSettings.UseCustomKeyStore)
            {
                PlayerSettings.Android.useCustomKeystore = _buildSettings.UseCustomKeyStore;
                PlayerSettings.Android.keystoreName = _buildSettings.CertificateRelativePath;
                PlayerSettings.Android.keyaliasName = _buildSettings.CertificateAlias;
                PlayerSettings.Android.keystorePass = _buildSettings.CertificatePassword;
                PlayerSettings.Android.keyaliasPass = _buildSettings.CertificatePassword;
            }
#endif
            
            EditorUtility.SetDirty(_buildSettings);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public void OnPreprocessBuild(BuildReport report)
        {
            _buildSettings = BuildSettings.Instance;
        }
    }
}