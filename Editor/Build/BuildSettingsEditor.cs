using System;
using GameReady.Build;
using UnityEditor;
using UnityEngine;

namespace GameReady.Editor.Build
{
    [CustomEditor(typeof(BuildSettings))]
    public class BuildSettingsEditor : UnityEditor.Editor
    {
        private BuildSettings _buildSettings;
        private SerializedProperty _version;
        private SerializedProperty _buildNumber;
        private SerializedProperty _debug;

        private void OnEnable()
        {
            _buildSettings = (BuildSettings)target;
            _version = serializedObject.FindProperty("_version");
            _debug = serializedObject.FindProperty("_debug");

#if UNITY_ANDROID
            _buildNumber = serializedObject.FindProperty("_buildNumberAndroid");
#elif UNITY_IOS
            _buildNumber = serializedObject.FindProperty("_buildNumberIos");
#else
            _buildNumber = serializedObject.FindProperty("_buildNumberAndroid");
#endif

            _buildSettings.FillFromProjectSettings();
        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            GUI.enabled = false;
            _version.stringValue =
                EditorGUILayout.TextField("Version", _version.stringValue);

            _buildNumber.intValue = EditorGUILayout.IntField("Build Number", _buildNumber.intValue);
            
            _debug.boolValue = EditorGUILayout.Toggle(new GUIContent("Debug", "Changes only in build process"),
                _debug.boolValue);
            GUI.enabled = true;

            GUILayout.Label("Auto Fill when Start Build", EditorStyles.boldLabel);
            if (GUILayout.Button("Fill From Player Settings"))
            {
                _buildSettings.FillFromProjectSettings();
            }

            if (GUILayout.Button("Save"))
            {
                EditorUtility.IsDirty(_buildSettings);
                AssetDatabase.SaveAssets();
            }

#if UNITY_ANDROID
            DrawAndroidCertificates("Android Certificates");

#endif

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawAndroidCertificates(string label)
        {
            EditorGUILayout.Separator();

            EditorGUILayout.BeginVertical(GUI.skin.box);

            EditorGUILayout.LabelField(label, EditorStyles.boldLabel);

            GUI.enabled = false;
            _buildSettings.CertificateRelativePath =
                EditorGUILayout.TextField("Keystore path", _buildSettings.CertificateRelativePath);
            _buildSettings.CertificateAlias =
                EditorGUILayout.TextField("Keystore alias", _buildSettings.CertificateAlias);
            _buildSettings.CertificatePassword =
                EditorGUILayout.PasswordField("Keystore Password", _buildSettings.CertificatePassword);

            _buildSettings.UseCustomKeyStore =
                EditorGUILayout.Toggle("Custom keystore", _buildSettings.UseCustomKeyStore);
            GUI.enabled = true;

            if (GUILayout.Button("Fill From Android Publish Settings"))
            {
#if UNITY_ANDROID
                if (UnityEditor.PlayerSettings.Android.useCustomKeystore)
                {
                    _buildSettings.UseCustomKeyStore = true;
                    _buildSettings.CertificateRelativePath = PlayerSettings.Android.keystoreName;
                    _buildSettings.CertificateAlias = PlayerSettings.Android.keyaliasName;
                    _buildSettings.CertificatePassword = PlayerSettings.Android.keystorePass;
                }
#endif
            }

            EditorGUILayout.EndVertical();
        }
    }
}