using GameReady.EditorTools;
using UnityEditor;

namespace GameReady.Build.Editor
{
    public static class BuildEditorHelper
    {
        [InitializeOnLoadMethod]
        private static void Init()
        {
            if (ValidateCreateBuildSettings())
            {
                CreateBuildSettingsAsset();
            }
        }
        
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Build/Select BuildSettings", false, 0)]
        public static void SelectBuildSettings()
        {
            var path = EditorHelpers.CreateNestedFoldersFromAssetsSubfolder("Resources");
            EditorHelpers.CreateScriptableAsset<BuildSettings>(path, true);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Build/Create BuildSettings", false, 0)]
        public static void CreateBuildSettingsAsset()
        {
            var path = EditorHelpers.CreateNestedFoldersFromAssetsSubfolder("Resources");
            EditorHelpers.CreateScriptableAsset<BuildSettings>(path, true);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Build/Create BuildSettings", true, 0)]
        public static bool ValidateCreateBuildSettings()
        {
            var buildInfo = BuildSettings.Instance;
            if (buildInfo == null)
            {
                return true;
            }

            return false;
        }
    }
}