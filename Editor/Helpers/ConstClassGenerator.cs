﻿using System.IO;
using System.Text;
using UnityEditor;

namespace GameReady.EditorTools
{
    public static class ConstClassGenerator
    {
        public static void GenerateConstClassFile(string title, string defaultFileName, string className,
            string[] constValues, string namespaceName = "Game.Data")
        {
            if (constValues == null || constValues.Length == 0) return;

            var path = EditorUtility.SaveFilePanelInProject(title, defaultFileName, "cs",
                "");
            if (!string.IsNullOrEmpty(path))
            {
                var classData = GenerateClassData(className, constValues, namespaceName);
                File.WriteAllText(path, classData);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }

        private static string GenerateClassData(string className, string[] constValues, string namespaceName)
        {
            var sb = new StringBuilder();

            sb.AppendLine("// Generated by ConstClassGenerator");
            sb.AppendLine($"namespace {namespaceName}");
            sb.AppendLine("{");
            sb.AppendLine($"    public static class {className}");
            sb.AppendLine("    {");
            foreach (var varName in constValues)
            {
                sb.AppendLine($"        public const string {varName.ToUpper()} = \"{varName}\";");
            }

            sb.AppendLine("    }");
            sb.AppendLine("}");
            return sb.ToString();
        }
    }
}