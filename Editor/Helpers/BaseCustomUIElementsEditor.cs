using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameReady.EditorTools
{
    public abstract class BaseCustomUIElementsEditor : UnityEditor.Editor
    {
        protected void DrawNestedScriptable<TScriptable>(SerializedProperty property, VisualElement container,
            int childIndex = 0, string propertyName = "", bool useFoldout = true)
            where TScriptable : ScriptableObject
        {
            if (property == null || container == null)
            {
                Debug.LogError("DrawNestedScriptable null arguments");
                return;
            }

            var propName = string.IsNullOrEmpty(propertyName)
                ? $"{property.displayName}"
                : $"{propertyName}";

            if (property.objectReferenceValue != null)
            {
                CreateNestedEditorFromProperty(property, container, childIndex, propName, useFoldout);
            }
            else
            {
                var btn = new Button(() =>
                {
                    property.objectReferenceValue =
                        EditorHelpers.CreateScriptableAsset<TScriptable>(false, true);
                    serializedObject.ApplyModifiedProperties();

                    if (property.objectReferenceValue != null)
                    {
                        CreateNestedEditorFromProperty(property, container, childIndex, propName, useFoldout);
                    }
                    
                })
                {
                    name = property.name,
                    text = $"Create {propName}"
                };

                container.Insert(childIndex, btn);
            }
        }

        private void CreateNestedEditorFromProperty(SerializedProperty property, VisualElement container,
            int childIndex, string propertyName, bool usefoldOut)
        {
            container.Q<Button>(property.name)?.RemoveFromHierarchy();

            if (usefoldOut)
            {
                var foldout = new Foldout();
                foldout.name = property.name;
                foldout.text = propertyName;
                foldout.Add(new InspectorElement(CreateEditor(property.objectReferenceValue)));
                container.Insert(childIndex, foldout);
            }
            else
            {
                container.Insert(childIndex, new InspectorElement(CreateEditor(property.objectReferenceValue)));
            }
        }
    }
}