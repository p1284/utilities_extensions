﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Gameready.Utils;
using Gameready.Utils.Attributes;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using Object = UnityEngine.Object;

namespace GameReady.EditorTools
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(Object), true)]
    public class GameReadyEditor : UnityEditor.Editor
    {
        private List<MethodInfo> _methods;

        public override VisualElement CreateInspectorGUI()
        {
            var container = new VisualElement();

            InspectorElement.FillDefaultInspector(container, serializedObject, this);

            DrawButtons(container);

            return container;
        }

        protected void DrawButtons(VisualElement container)
        {
            GetMethods(ref _methods);

            if (_methods == null) return;

            foreach (var methodInfo in _methods)
            {
                var attribute = methodInfo.GetCustomAttributes(typeof(ButtonAttribute), true)[0] as ButtonAttribute;
                var buttonName = attribute != null && string.IsNullOrEmpty(attribute.Name)
                    ? methodInfo.Name
                    : attribute.Name;

                if (methodInfo.GetParameters().All(p => p.IsOptional))
                {
                    var button = new Button(() => { InvokeButtonMethod(methodInfo, attribute); })
                    {
                        text = buttonName
                    };

                    if (attribute.order > 0)
                    {
                        container.Insert(attribute.order, button);
                    }
                    else
                    {
                        container.Add(button);
                    }

                    button.SetEnabled(attribute.EditorOnly ? !Application.isPlaying : Application.isPlaying);
                }
                else

                {
                    var helpBox =
                        new HelpBox(
                            $"Script:{target.GetType().Name} with button attribute on:{methodInfo.Name} method, with parameters not supported",
                            HelpBoxMessageType.Warning);

                    container.Add(helpBox);
                }
            }
        }

        protected virtual void InvokeButtonMethod(MethodInfo methodInfo, ButtonAttribute buttonAttribute)
        {
            try
            {
                if (methodInfo.IsAsyncMethod() || methodInfo.IsCoroutineMethod())
                {
                    throw new Exception("ButtonAttribute method can't be async or coroutine");
                }


                switch (buttonAttribute.InvokeType)
                {
                    case ButtonInvokeType.InvokeInPrefabSceneModeWithSceneDirty:


                        methodInfo.Invoke(target, null);
                        if (!Application.isPlaying)
                        {
                            // Set target object and scene dirty to serialize changes to disk
                            EditorUtility.SetDirty(target);

                            PrefabStage stage = PrefabStageUtility.GetCurrentPrefabStage();
                            if (stage != null)
                            {
                                // Prefab mode
                                EditorSceneManager.MarkSceneDirty(stage.scene);
                            }
                            else
                            {
                                // Normal scene
                                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                            }
                        }

                        break;
                    default:
                        methodInfo.Invoke(target, null);
                        break;
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        protected virtual void GetMethods(ref List<MethodInfo> methods)
        {
            if (methods == null || methods.Count == 0)
            {
                methods = target.GetType()
                    .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic |
                                BindingFlags.Static | BindingFlags.DeclaredOnly).Where(info =>
                        info.GetCustomAttributes(typeof(ButtonAttribute), true).Length > 0).ToList();
            }
        }
    }
}