using System;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using UnityEngine;

namespace GameReady.EditorTools
{
    public class UPMHelper
    {
        private ListRequest _checkRequest;

        private AddRequest _addRequest;
        private RemoveRequest _removeRequest;

        private string _packageId;
        private string _checkPackageId;

        private Action<bool> _deleteCallback;
        private Action<bool> _installCallback;
        private Action<bool> _existCallback;

        public void CheckPackageInstalled(string packageId, Action<bool> callback)
        {
            _existCallback = callback;
            _checkPackageId = packageId;
            _checkRequest = Client.List(offlineMode: true);
            EditorApplication.update += progressCheckUpdate;
        }

        public void DeletePackage(string packageId, Action<bool> callback)
        {
            _packageId = packageId;
            _deleteCallback = callback;
            _removeRequest = Client.Remove(packageId);
            EditorApplication.update += progressRemoveUpdate;
        }

        public void SetupPackage(string packageId, Action<bool> callback)
        {
            _packageId = packageId;
            _addRequest = Client.Add(packageId);
            _installCallback = callback;
            EditorApplication.update += progressSetupUpdate;
        }

        private void progressRemoveUpdate()
        {
            if (_removeRequest.IsCompleted)
            {
                if (_removeRequest.Status == StatusCode.Success)
                {
                    _deleteCallback?.Invoke(true);
                    Debug.Log($"Package with id:{_packageId} remove complete");
                }
                else
                {
                    _deleteCallback?.Invoke(false);
                    Debug.Log($"Package with id:{_packageId} remove failed:{_removeRequest.Error.message}");
                }

                EditorApplication.update -= progressRemoveUpdate;
            }
        }

        private void progressSetupUpdate()
        {
            if (_addRequest.IsCompleted)
            {
                EditorApplication.update -= progressSetupUpdate;
                if (_addRequest.Status == StatusCode.Success)
                {
                    _installCallback?.Invoke(true);
                    Debug.Log($"Package with id:{_packageId} install complete");
                }
                else
                {
                    _installCallback?.Invoke(false);
                    Debug.Log($"Package with id:{_packageId} install failed:{_addRequest.Error.message}");
                }
            }
        }

        private void progressCheckUpdate()
        {
            if (_checkRequest.IsCompleted)
            {
                if (_checkRequest.Status == StatusCode.Success)
                {
                    var exist = ContainsPackage(_checkRequest.Result, _checkPackageId);
                    _existCallback.Invoke(exist);
                    Debug.Log($"Package with id:{_checkPackageId} {(exist ? "found" : "not found")}");
                }
                else if (_checkRequest.Status >= StatusCode.Failure)
                {
                    Debug.Log("Could not check for packages: " + _checkRequest.Error.message);
                    _existCallback.Invoke(false);
                }

                EditorApplication.update -= progressCheckUpdate;
            }
        }

        private bool ContainsPackage(PackageCollection packages, string packageId)
        {
            foreach (var package in packages)
            {
                if (String.CompareOrdinal(package.name, packageId) == 0)
                    return true;

                foreach (var dependencyInfo in package.dependencies)
                    if (String.CompareOrdinal(dependencyInfo.name, packageId) == 0)
                        return true;
            }

            return false;
        }
    }
}