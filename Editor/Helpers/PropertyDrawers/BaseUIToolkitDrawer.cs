using UnityEditor;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.PropertyDrawers
{
    public abstract class BaseUIToolkitDrawer : PropertyDrawer
    {
        public sealed override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            return CreateProperty(property);
        }

        protected abstract VisualElement CreateProperty(SerializedProperty property);

    }
}