using Gameready.Utils.Attributes;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(TagAttribute), true)]
    public class TagPropertyDrawer : BaseUIToolkitDrawer
    {
        protected override VisualElement CreateProperty(SerializedProperty property)
        {
            var root = new VisualElement();

            if (property.propertyType == SerializedPropertyType.String)
            {
                var tagsField = new TagField(property.displayName, property.stringValue);
                tagsField.RegisterValueChangedCallback(evt =>
                {
                    property.stringValue = evt.newValue;
                    property.serializedObject.ApplyModifiedProperties();
                });
                root.Add(tagsField);
            }
            else
            {
                root.Add(new HelpBox("Tags field must be string type", HelpBoxMessageType.Warning));
                root.Add(new PropertyField(property));
            }

            return root;
        }
    }
}