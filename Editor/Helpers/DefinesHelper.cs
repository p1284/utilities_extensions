using System.Linq;
using UnityEditor;
using UnityEditor.Compilation;

namespace GameReady.EditorTools
{
    public class DefinesHelper
    {
         private string _currentSetupRemoveDefine;
         private string _operation = "Process";

        public bool HasDefine(BuildTargetGroup group, string define)
        {
            PlayerSettings.GetScriptingDefineSymbolsForGroup(group, out string[] defines);

            return defines.Contains(define);
        }

        public void AddDefines(BuildTargetGroup group, string define)
        {
            _currentSetupRemoveDefine = define;
            _operation = "Add";

            PlayerSettings.GetScriptingDefineSymbolsForGroup(group, out string[] defines);

            if (!defines.Contains(define))
            {
               // EditorUtility.ClearProgressBar();
                // CompilationPipeline.compilationStarted -= DefineSetupCompilationStarted;
                // CompilationPipeline.compilationFinished -= DefineSetupCompilationComplete;
                // CompilationPipeline.compilationStarted += DefineSetupCompilationStarted;
                // CompilationPipeline.compilationFinished += DefineSetupCompilationComplete;
                var list = defines.ToList();
                list.Add(define);
                PlayerSettings.SetScriptingDefineSymbolsForGroup(group, list.ToArray());
            }
        }

        // private void DefineSetupCompilationComplete(object obj)
        // {
        //     CompilationPipeline.compilationFinished -= DefineSetupCompilationComplete;
        //     EditorUtility.ClearProgressBar();
        // }
        //
        // private void DefineSetupCompilationStarted(object obj)
        // {
        //     CompilationPipeline.compilationStarted -= DefineSetupCompilationStarted;
        //     EditorUtility.DisplayProgressBar($"Compilation...", $"{_operation} Define:{_currentSetupRemoveDefine}", 1f);
        // }

        public void RemoveDefine(BuildTargetGroup group, string define)
        {
            _currentSetupRemoveDefine = define;
            _operation = "Remove";

            PlayerSettings.GetScriptingDefineSymbolsForGroup(group, out string[] defines);

            if (defines.Contains(define))
            {
                // CompilationPipeline.compilationStarted += DefineSetupCompilationStarted;
                // CompilationPipeline.compilationFinished += DefineSetupCompilationComplete;
                var list = defines.ToList();
                list.Remove(define);
                PlayerSettings.SetScriptingDefineSymbolsForGroup(group, list.ToArray());
            }
        }
    }
}