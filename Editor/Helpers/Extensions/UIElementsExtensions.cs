using UnityEngine.UIElements;

namespace GameReady.EditorTools.Extensions
{
    public static class UIElementsExtensions
    {
        public static VisualElement GetFirstAncestorWithClass(this VisualElement element, string className)
        {
            if (element == null)
                return null;
 
            if (element.name == className)
                return element;
 
            return element.parent.GetFirstAncestorWithClass(className);
        }
    }
}