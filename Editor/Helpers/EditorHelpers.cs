using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameReady.EditorTools
{
    public static class EditorHelpers
    {
        private static DefinesHelper _definesHelper;

        public const string DEFAULT_MENU_ITEMNAME = "GameReady";


        public static string GetRelativeAssetsPath(string absolutePath)
        {
            var relativePath = absolutePath;
            if (absolutePath.StartsWith(Application.dataPath))
            {
                relativePath = "Assets" + absolutePath.Substring(Application.dataPath.Length);
            }

            return relativePath;
        }

        public static bool SelectAssetByLabel(string label)
        {
            var assets = UnityEditor.AssetDatabase.FindAssets($"l:{label}");
            if (assets.Length > 0)
            {
                var path = UnityEditor.AssetDatabase.GUIDToAssetPath(assets.First());
                SelectAssetByPath<Object>(path);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Find first Object by Type and Ping it
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static bool SelectAssetByType<T>() where T : Object
        {
            var assets = AssetDatabase.FindAssets($"t:{typeof(T)}");
            if (assets.Length > 0)
            {
                var path = AssetDatabase.GUIDToAssetPath(assets[0]);
                var asset = AssetDatabase.LoadAssetAtPath<T>(path);

                Selection.activeObject = asset;
                EditorGUIUtility.PingObject(asset);
                return true;
            }

            return false;
        }

        public static bool SelectAssetByPath<T>(string path) where T : Object
        {
            var asset = AssetDatabase.LoadAssetAtPath<T>(path);
            if (asset != null)
            {
                Selection.activeObject = asset;
                EditorGUIUtility.PingObject(asset);
                return true;
            }

            return false;
        }

        public static bool SelectAssetByPath(string path)
        {
            var asset = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
            if (asset != null)
            {
                Selection.activeObject = asset;
                EditorGUIUtility.PingObject(asset);
                return true;
            }

            return false;
        }


        public static string CopyAssetToFolder(params Object[] assets)
        {
            if (assets is { Length: > 0 })
            {
                var saveFolderPath = EditorUtility.SaveFolderPanel(assets.Length > 1 ? "Save Assets" : "Save Asset",
                    Application.dataPath, "");
                foreach (var asset in assets)
                {
                    var originPath = AssetDatabase.GetAssetPath(asset);
                    var fileName = Path.GetFileName(originPath);

                    if (string.IsNullOrEmpty(saveFolderPath)) continue;
                    var path = GetRelativeAssetsPath(saveFolderPath + "/" + fileName);
                    if (AssetDatabase.CopyAsset(originPath, path))
                    {
                        Debug.Log($"Asset {asset.name} copied success!");
                    }
                    else
                    {
                        Debug.LogError($"Asset {asset.name} copy failed!");
                    }
                }

                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                return saveFolderPath;
            }

            return "";
        }

        [MenuItem("Assets/CopyToFolder", false, 0)]
        public static void CopyAssetToFolder(MenuCommand command)
        {
            var assets = Selection.objects;

            CopyAssetToFolder(assets);
        }

        [MenuItem("Assets/CopyPrefabVariantToFolder", true, 1)]
        private static bool CopyPrefabVariantValidate()
        {
            var assets = Selection.objects;
            if (assets != null && assets.Length > 0)
            {
                foreach (var asset in assets)
                {
                    if (!PrefabUtility.IsPartOfAnyPrefab(asset))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        [MenuItem("Assets/CopyPrefabVariantToFolder", false, 1)]
        public static void CopyPrefabVariantToFolder()
        {
            var assets = Selection.objects;
            if (assets != null && assets.Length > 0)
            {
                var saveFolderPath =
                    EditorUtility.SaveFolderPanel("Save PrefabVariant Asset", Application.dataPath, "");
                foreach (var asset in assets)
                {
                    var originPath = AssetDatabase.GetAssetPath(asset);
                    var fileName = Path.GetFileName(originPath);

                    if (string.IsNullOrEmpty(saveFolderPath)) continue;

                    // var path = GetRelativeAssetsPath(saveFolderPath + "/" + fileName);

                    GameObject objSource = PrefabUtility.InstantiatePrefab(asset) as GameObject;
                    try
                    {
                        PrefabUtility.SaveAsPrefabAsset(objSource, $"{saveFolderPath}/{fileName}");
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e.Message);
                    }
                    finally
                    {
                        Object.DestroyImmediate(objSource);
                    }
                }
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public static GameObject CreatePrefabInProject(GameObject prefabSource, string savePrefabName, bool useVariant,
            string assetLabel = "")
        {
            if (prefabSource != null)
            {
                GameObject objSource = useVariant
                    ? PrefabUtility.InstantiatePrefab(prefabSource) as GameObject
                    : Object.Instantiate(prefabSource);

                try
                {
                    var path = EditorUtility.SaveFilePanelInProject("Save Prefab", savePrefabName, "prefab", "");

                    if (!string.IsNullOrEmpty(path))
                    {
                        GameObject prefabAsset =
                            PrefabUtility.SaveAsPrefabAsset(objSource, path);


                        if (!string.IsNullOrEmpty(assetLabel))
                        {
                            AssetDatabase.SetLabels(prefabAsset, new[] { assetLabel });
                        }

                        AssetDatabase.SaveAssets();

                        return prefabAsset;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message);
                }
                finally
                {
                    Object.DestroyImmediate(objSource);
                }
            }

            return null;
        }

        public static void CreatePrefab(GameObject prefabSource, string savePrefabName, bool useVariant,
            string assetLabel = "", string assetsSubFolder = "Resources",
            params string[] subFolders)
        {
            if (prefabSource != null)
            {
                GameObject objSource = useVariant
                    ? PrefabUtility.InstantiatePrefab(prefabSource) as GameObject
                    : Object.Instantiate(prefabSource);

                try
                {
                    var savePath = CreateNestedFoldersFromAssetsSubfolder(assetsSubFolder, subFolders);

                    GameObject prefabAsset =
                        PrefabUtility.SaveAsPrefabAsset(objSource, $"{savePath}/{savePrefabName}");


                    if (!string.IsNullOrEmpty(assetLabel))
                    {
                        AssetDatabase.SetLabels(prefabAsset, new[] { assetLabel });
                    }

                    AssetDatabase.SaveAssets();
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message);
                }
                finally
                {
                    Object.DestroyImmediate(objSource);
                }
            }
        }

        public static string CreateNestedFoldersFromAssetsSubfolder(string assetsSubFolder, params string[] subFolders)
        {
            if (string.IsNullOrEmpty(assetsSubFolder))
            {
                throw new ArgumentNullException();
            }

            var fullPath = $"Assets/{assetsSubFolder}";
            if (!AssetDatabase.IsValidFolder(fullPath))
            {
                AssetDatabase.CreateFolder("Assets", assetsSubFolder);
            }

            foreach (var subFolder in subFolders)
            {
                if (string.IsNullOrEmpty(subFolder)) continue;

                if (!AssetDatabase.IsValidFolder(fullPath + "/" + subFolder))
                {
                    AssetDatabase.CreateFolder(fullPath, subFolder);
                }

                fullPath += $"/{subFolder}";
            }

            return fullPath;
        }

        public static T CreateSceneInstance<T>(string name) where T : MonoBehaviour
        {
            var instance = Object.FindObjectOfType<T>();
            if (instance == null)
            {
                instance = new GameObject(name).AddComponent<T>();
            }

            EditorGUIUtility.PingObject(instance.gameObject);

            return instance;
        }

        public static ScriptableObject CreateScriptableAsset(Type type, bool selectAsset = false,
            bool checkExist = false, string overrideAssetName = "")
        {
            var assetName = string.IsNullOrEmpty(overrideAssetName) ? type.Name : overrideAssetName;

            if (checkExist)
            {
                var asset = FindScriptableAsset(type, selectAsset);
                if (asset != null)
                {
                    return asset;
                }
            }

            var path = EditorUtility.SaveFilePanelInProject($"Save {assetName} Config", assetName, "asset", "");

            if (string.IsNullOrEmpty(path)) return null;

            var loadAsset = AssetDatabase.LoadAssetAtPath(path, type);

            if (loadAsset == null)
            {
                loadAsset = ScriptableObject.CreateInstance(type);

                AssetDatabase.CreateAsset(loadAsset, path);
                AssetDatabase.SaveAssets();
            }

            if (selectAsset)
            {
                EditorUtility.FocusProjectWindow();

                Selection.activeObject = loadAsset;
            }

            return (ScriptableObject)loadAsset;
        }

        public static bool SaveScriptableAssetInProject(string assetName, ScriptableObject asset)
        {
            if (string.IsNullOrEmpty(assetName) || asset == null) return false;
            var path = EditorUtility.SaveFilePanelInProject($"Save {assetName} scriptableobject", assetName, "asset",
                "");
            if (string.IsNullOrEmpty(path)) return false;
            var loadAsset = AssetDatabase.LoadAssetAtPath(path, asset.GetType());

            if (loadAsset == null)
            {
                AssetDatabase.CreateAsset(asset, path);
                AssetDatabase.SaveAssets();
            }

            return true;
        }

        public static ScriptableObject FindScriptableAsset(Type assetType, bool selectAsset = false)
        {
            var assets = AssetDatabase.FindAssets($"t:{assetType.FullName}");

            if (assets.Length > 0)
            {
                foreach (var asset in assets)
                {
                    var assetPath = AssetDatabase.GUIDToAssetPath(asset);

                    if (assetPath.Contains(".asset"))
                    {
                        var loadedAsset = AssetDatabase.LoadAssetAtPath(assetPath, assetType);

                        if (loadedAsset != null && loadedAsset.GetType() == assetType)
                        {
                            if (selectAsset)
                            {
                                SelectAssetByPath(assetPath);
                            }

                            return (ScriptableObject)loadedAsset;
                        }
                    }
                }
            }

            return null;
        }

        public static T CreateScriptableAsset<T>(bool selectAsset = false, bool checkExist = false,
            string overrideAssetName = "")
            where T : ScriptableObject
        {
            var type = typeof(T);

            return (T)CreateScriptableAsset(type, selectAsset, checkExist, overrideAssetName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path">Assets folder path without assetname</param>
        /// <param name="selectAsset"></param>
        /// <typeparam name="T"></typeparam>
        public static T CreateScriptableAsset<T>(string path, bool selectAsset = false) where T : ScriptableObject
        {
            var assetName = typeof(T).Name;

            var loadAsset = AssetDatabase.LoadAssetAtPath<T>(path + $"/{assetName}.asset");

            if (loadAsset == null)
            {
                loadAsset = ScriptableObject.CreateInstance<T>();

                AssetDatabase.CreateAsset(loadAsset, path + $"/{assetName}.asset");
                AssetDatabase.SaveAssets();
            }

            if (selectAsset)
            {
                EditorUtility.FocusProjectWindow();

                Selection.activeObject = loadAsset;
            }

            return loadAsset;
        }

        #region Define Helpers

        public static void AddDefineForCurrentPlatform(string define)
        {
            _definesHelper ??= new DefinesHelper();
            var target = EditorUserBuildSettings.activeBuildTarget;
            var group = BuildPipeline.GetBuildTargetGroup(target);
            _definesHelper.AddDefines(group, define);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public static void RemoveDefineForCurrentPlatform(string define)
        {
            _definesHelper ??= new DefinesHelper();
            var target = EditorUserBuildSettings.activeBuildTarget;
            var group = BuildPipeline.GetBuildTargetGroup(target);
            _definesHelper.RemoveDefine(group, define);
            AssetDatabase.SaveAssets();
        }

        public static void AddDefineForPlatform(string define, BuildTargetGroup targetGroup)
        {
            _definesHelper ??= new DefinesHelper();
            _definesHelper.AddDefines(targetGroup, define);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public static void AddDefineForIOSAndroid(string define)
        {
            _definesHelper ??= new DefinesHelper();
            _definesHelper.AddDefines(BuildTargetGroup.Android, define);
            _definesHelper.AddDefines(BuildTargetGroup.iOS, define);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }


        public static bool HasDefine(string define)
        {
            _definesHelper ??= new DefinesHelper();
            var target = EditorUserBuildSettings.activeBuildTarget;
            var group = BuildPipeline.GetBuildTargetGroup(target);
            return _definesHelper.HasDefine(group, define);
        }

        public static void RemoveDefineForPlatform(string define, BuildTargetGroup targetGroup)
        {
            _definesHelper ??= new DefinesHelper();
            _definesHelper.RemoveDefine(targetGroup, define);
            AssetDatabase.SaveAssets();
        }

        public static void RemoveDefinesForIOSAndroid(params string[] defines)
        {
            _definesHelper ??= new DefinesHelper();
            foreach (var define in defines)
            {
                _definesHelper.RemoveDefine(BuildTargetGroup.Android, define);
                _definesHelper.RemoveDefine(BuildTargetGroup.iOS, define);
            }

            AssetDatabase.SaveAssets();
        }

        public static void RemoveDefineForIOSAndroid(string define)
        {
            _definesHelper ??= new DefinesHelper();
            _definesHelper.RemoveDefine(BuildTargetGroup.Android, define);
            _definesHelper.RemoveDefine(BuildTargetGroup.iOS, define);
            AssetDatabase.SaveAssets();
        }

        #endregion

        #region UPM helpers

        public static void CheckPackageInstalled(string packageid, Action<bool> callback)
        {
            var packageHelper = new UPMHelper();
            packageHelper.CheckPackageInstalled(packageid, callback);
        }

        public static void InstallPackage(string packageId, Action<bool> callback)
        {
            var packageHelper = new UPMHelper();
            packageHelper.SetupPackage(packageId, callback);
        }

        public static void UninstallPackage(string packageId, Action<bool> callback)
        {
            var packageHelper = new UPMHelper();
            packageHelper.DeletePackage(packageId, callback);
        }

        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetLabel"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> FindByLabel<T>(string assetLabel) where T : Object
        {
            var list = new List<T>();
            var assets = AssetDatabase.FindAssets($"l:{assetLabel}");
            foreach (var asset in assets)
            {
                var path = AssetDatabase.GUIDToAssetPath(asset);
                var obj = AssetDatabase.LoadAssetAtPath<T>(path);
                if (obj != null)
                {
                    list.Add(obj);
                }
            }

            return list;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"> in Assets</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T[] GetAtPath<T>(string path)
        {
            try
            {
                ArrayList al = new ArrayList();
                string[] fileEntries = Directory.GetFiles(Application.dataPath + "/" + path);

                foreach (string fileName in fileEntries)
                {
                    string temp = fileName.Replace("\\", "/");
                    int index = temp.LastIndexOf("/", StringComparison.Ordinal);
                    string localPath = "Assets/" + path;

                    if (index > 0)
                        localPath += temp.Substring(index);

                    Object t = UnityEditor.AssetDatabase.LoadAssetAtPath(localPath, typeof(T));

                    if (t != null)
                        al.Add(t);
                }

                T[] result = new T[al.Count];

                for (int i = 0; i < al.Count; i++)
                    result[i] = (T)al[i];

                return result;
            }
            catch (Exception e)
            {
                Debug.LogError("EditorHelpers: " + e.Message);
            }

            return default;
        }
    }
}