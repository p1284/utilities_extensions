using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameReady.EditorTools
{
    public class SpriteToolSettings : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private Object[] _prefabsFolders;
        [SerializeField] private Object[] _spritesFolders;

        [SerializeField] private List<Sprite> _unusedSpritesResult;

        //[Button("Delete All UnUsed Sprites")]
        private void DeleteAllUnused()
        {
            if (_unusedSpritesResult.Count > 0)
            {
                var result = EditorUtility.DisplayDialogComplex("Delete Unused Sprites ?", "", "yes", "no", "");
                if (result == 0)
                {
                    foreach (var sprite in _unusedSpritesResult)
                    {
                        AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(sprite));
                    }


                    _unusedSpritesResult.Clear();

                    AssetDatabase.Refresh();
                    AssetDatabase.SaveAssets();
                }
            }
        }

        private T[] FindAssets<T>(string label, string[] foldersToSearch = null) where T : Object
        {
            var paths = AssetDatabase.FindAssets(label, foldersToSearch);
            var list = new List<T>();

            foreach (var path in paths)
            {
                var asset = AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(path));
                if (asset != null)
                {
                    list.Add(asset);
                }
            }

            return list.ToArray();
        }

        public string FindUnusedSprites()
        {
            _unusedSpritesResult.Clear();

            var hashMap = new HashSet<Sprite>();

            var sprites = FindAssets<Sprite>("t:Sprite", GetFoldersToSearch(_spritesFolders)).ToList();

            foreach (var sprite in sprites)
            {
                if (!hashMap.Contains(sprite))
                {
                    hashMap.Add(sprite);
                    _unusedSpritesResult.Add(sprite);
                }
            }

            var allPrefabs = FindAssets<GameObject>("t:Prefab", GetFoldersToSearch(_prefabsFolders));

            foreach (var prefab in allPrefabs)
            {
                var dependencies = AssetDatabase.GetDependencies(AssetDatabase.GetAssetPath(prefab));

                for (int i = 0; i < dependencies.Length; i++)
                {
                    if (dependencies[i].EndsWith(".png") || dependencies[i].EndsWith(".jpg"))
                    {
                        var sprite = AssetDatabase.LoadAssetAtPath<Sprite>(dependencies[i]);

                        if (sprite != null && hashMap.Contains(sprite))
                        {
                            _unusedSpritesResult.Remove(sprite);
                        }
                    }
                }
            }

            if (hashMap.Count > _unusedSpritesResult.Count)
            {
                return $"Found {_unusedSpritesResult.Count} unused sprites from total {hashMap.Count} in sprite folders";
            }


            return "Not Found Unused sprites";
        }

        private string[] GetFoldersToSearch(Object[] foldersToSearch)
        {
            if (foldersToSearch.Length == 0) throw new Exception("SpriteTools: Folders must be set");
            var folders = new List<string>();
            foreach (var folder in foldersToSearch)
            {
                if (folder != null)
                {
                    var path = AssetDatabase.GetAssetPath(folder);
                    if (AssetDatabase.IsValidFolder(path))
                    {
                        folders.AddRange(GetSubFoldersRecursive(path));
                    }
                }
            }

            return folders.ToArray();
        }

        private List<string> GetSubFoldersRecursive(string path)
        {
            var list = new List<string>();
            var subFolders = AssetDatabase.GetSubFolders(path);

            if (subFolders.Length == 0)
            {
                return list;
            }

            list.AddRange(subFolders);

            foreach (var subFolder in subFolders)
            {
                list.AddRange(GetSubFoldersRecursive(subFolder));
            }

            return list;
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
        }
    }
}