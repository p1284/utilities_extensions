using System;
using UnityEditor;
using UnityEngine;

namespace GameReady.EditorTools
{
    public class SpriteEditorHelpers
    {
        private const string EditorResourceFolder = "Editor Default Resources";

        private static SpriteToolSettings _settings;

        [MenuItem("Tools/SpriteTools/SelectToolSettings", false,100)]
        private static void SelectSpriteAtlasToolAsset()
        {
            GetOrCreateToolSettings();

            Selection.activeObject =
                AssetDatabase.LoadAssetAtPath(AssetDatabase.GetAssetPath(_settings), typeof(SpriteToolSettings));
        }

        [MenuItem("Tools/SpriteTools/FindUnUsedSprites", false,100)]
        private static void FindSpriteAtlasUnusedSprites()
        {
            EditorUtility.DisplayProgressBar("Process...", "Find Unused sprites", 1f);
            GetOrCreateToolSettings();
            var result = "";
            try
            {
                result = _settings.FindUnusedSprites();
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
            finally
            {
                EditorUtility.ClearProgressBar();
                SelectSpriteAtlasToolAsset();
                if (!string.IsNullOrEmpty(result))
                {
                    EditorUtility.DisplayDialog("Result", result, "close");
                }
                else
                {
                    EditorUtility.DisplayDialog("Result", "ERROR", "close");
                }

            }
        }

        private static void GetOrCreateToolSettings()
        {
            _settings = EditorGUIUtility.Load("SpriteToolSettings.asset") as SpriteToolSettings;
            if (_settings == null)
            {
                _settings = ScriptableObject.CreateInstance<SpriteToolSettings>();
                var path = $"{Application.dataPath}/{EditorResourceFolder}";
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                var assetPath = $"Assets/{EditorResourceFolder}/SpriteToolSettings.asset";
                AssetDatabase.CreateAsset(_settings, assetPath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }
    }
}