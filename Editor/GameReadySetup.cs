using Gameready.Utils;
using Gameready.Utils.ItemsSystem;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;

namespace GameReady.EditorTools
{
    public static class GameReadySetup
    {
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/ItemsSystem/Create HashItems DataStorage<NAME>", false, 31)]
        public static void CreateItemsSystemDataStorage()
        {
            EditorHelpers.CreateScriptableAsset<HashItemsDataStorage>(true);
        }
        
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/ItemsSystem/GetOrCreate HashItemsDb Container", false, 31)]
        public static void CreateItemsSystemDataStoragesDb()
        {
            EditorHelpers.CreateScriptableAsset<HashItemsDb>(true,true);
        }
        

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Recompile All Scripts", false, 31)]
        public static void RecompileAllAssemblies()
        {
            EditorUtility.ClearProgressBar();
            CompilationPipeline.RequestScriptCompilation(RequestScriptCompilationOptions.CleanBuildCache);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/ClearAll Prefs and Persistent", false, 32)]
        public static void ClearAll()
        {
            PlayerPrefs.DeleteAll();
            FileStorageUtils.DeleteAllPersistent();
            Debug.Log("Prefs and persistent cleared");
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/ClearAll AssetBundles", false, 33)]
        public static void ClearAllAssetBundlesCache()
        {
            if (Caching.ready && Caching.ClearCache())
            {
                Debug.Log("AssetBundles cache cleared");
            }
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Show PersistentData PathFolder", false, 34)]
        public static void ShowPersistentDataPathFolder()
        {
            OpenFoldersHelper.Open(Application.persistentDataPath);
            Debug.Log($"Open path folder:{Application.dataPath}");
        }

        //MonoHelper Auto Init Setup
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/MonoHelper/Auto Init", false, 35)]
        public static void CoroutineManagerAutoInit()
        {
            var target = EditorUserBuildSettings.activeBuildTarget;
            var group = BuildPipeline.GetBuildTargetGroup(target);
            EditorHelpers.AddDefineForPlatform(MonoHelper.AutoInitDefine, group);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/MonoHelper/Manual Init", false, 36)]
        public static void CoroutineManagerManualInit()
        {
            var target = EditorUserBuildSettings.activeBuildTarget;
            var group = BuildPipeline.GetBuildTargetGroup(target);
            EditorHelpers.RemoveDefineForPlatform(MonoHelper.AutoInitDefine, group);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/MonoHelper/Manual Init", true, 35)]
        public static bool CoroutineManagerManualInitValidate()
        {
            return EditorHelpers.HasDefine(MonoHelper.AutoInitDefine);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/MonoHelper/Auto Init", true, 35)]
        public static bool CoroutineManagerAutoInitValidate()
        {
            return !EditorHelpers.HasDefine(MonoHelper.AutoInitDefine);
        }
        
        //MonoHelper SafeUpdaters setup
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/MonoHelper/Safe Updaters Enable", false, 36)]
        public static void CoroutineManagerSafeUpdaterEnable()
        {
            var target = EditorUserBuildSettings.activeBuildTarget;
            var group = BuildPipeline.GetBuildTargetGroup(target);
            EditorHelpers.AddDefineForPlatform(MonoHelper.SafeUpdatersDefine, group);
        }
        
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/MonoHelper/Safe Updaters Enable", true, 36)]
        public static bool CoroutineManagerSafeUpdaterEnableValid()
        {
            return !EditorHelpers.HasDefine(MonoHelper.SafeUpdatersDefine);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/MonoHelper/Safe Updaters Disable", false, 37)]
        public static void CoroutineManagerSafeUpdaterDisable()
        {
            var target = EditorUserBuildSettings.activeBuildTarget;
            var group = BuildPipeline.GetBuildTargetGroup(target);
            EditorHelpers.RemoveDefineForPlatform(MonoHelper.SafeUpdatersDefine, group);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/MonoHelper/Safe Updaters Disable", true, 37)]
        public static bool CoroutineManagerSafeUpdaterDisableValid()
        {
            return EditorHelpers.HasDefine(MonoHelper.SafeUpdatersDefine);
        }

      
    }
}