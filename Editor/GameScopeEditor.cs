using System;
using System.Collections.Generic;
using System.Linq;
using GameReady.Currency;
using GameReady.Inventory;
using GameReady.Saves.Data;
using Gameready.Services;
using GameReady.UI.Popups.Data;
using GameReady.UI.Screens.Data;
using Gameready.Utils.Services.Core;
using UnityEditor;
using UnityEngine;

namespace GameReady.EditorTools
{
    [CustomEditor(typeof(ProjectScope), true)]
    public class GameScopeEditor : UnityEditor.Editor
    {
        private SerializedProperty _serviceConfigsProperty;

        private ProjectScope _projectScope;

        private readonly List<Type> _defaultServices = new List<Type>()
        {
            typeof(PersistentSavesConfig),
            typeof(PlayerInventoryConfig),
            typeof(CurrencyBalanceConfig),
            typeof(ScreensManagerConfig),
            typeof(PopupsManagerConfig)
        };

        private void OnEnable()
        {
            _projectScope = (ProjectScope)target;
            _serviceConfigsProperty = serializedObject.FindProperty("_serviceConfigs");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Separator();

            DrawCreateServices();
        }

        private bool CheckServiceConfig(Type serviceConfigType)
        {
            if (_projectScope.ServiceConfigs == null) return false;
            return _projectScope.ServiceConfigs.Any(serviceConfig =>
                serviceConfig != null && serviceConfig.GetType() == serviceConfigType);
        }

        private void DrawCreateServices()
        {
            foreach (var defaultServiceType in _defaultServices)
            {
                if (!CheckServiceConfig(defaultServiceType))
                {
                    CreateServiceConfig(defaultServiceType);
                }
            }
        }

        private void CreateServiceConfig(Type serviceType)
        {
            if (GUILayout.Button($"Create {serviceType.Name}"))
            {
                var asset = EditorHelpers.CreateScriptableAsset(serviceType, false, true) as BaseServiceConfig;

                _projectScope.ServiceConfigs.Add(asset);

                EditorUtility.SetDirty(_projectScope.gameObject);
            }
        }
    }
}