using System.Collections.Generic;
using UnityEditor;
#if ADDRESSABLES
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.AddressableAssets.Settings.GroupSchemas;
#endif
using UnityEngine;

namespace GameReady.EditorTools
{
    public static class AddressablesHelper
    {
        private const string ADDRESSABLES_SUPPORT = "ADDRESSABLES";

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Addressables/Enable", false, 40)]
        public static void EnableAddressables()
        {
            EditorHelpers.CheckPackageInstalled("com.unity.addressables", b =>
            {
                if (b)
                {
                    EditorHelpers.AddDefineForIOSAndroid(ADDRESSABLES_SUPPORT);
                }
            });
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Addressables/Disable", false, 41)]
        public static void DisableAddressables()
        {
            EditorHelpers.RemoveDefineForIOSAndroid(ADDRESSABLES_SUPPORT);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Addressables/Disable", true)]
        public static bool DisableAddressablesValidate()
        {
            return EditorHelpers.HasDefine(ADDRESSABLES_SUPPORT);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Addressables/Enable", true)]
        public static bool EnableAddressablesValidation()
        {
            return !EditorHelpers.HasDefine(ADDRESSABLES_SUPPORT);
        }

#if ADDRESSABLES
        /// <summary>
        /// Setup Addressable option exstension inside specified group
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="groupName"></param>
        public static void SetAddressableGroup(this Object obj, string groupName)
        {
            var settings = AddressableAssetSettingsDefaultObject.Settings;

            if (settings)
            {
                var group = settings.FindGroup(groupName);
                if (!group)
                    group = settings.CreateGroup(groupName, false, false, true, null, typeof(ContentUpdateGroupSchema),
                        typeof(BundledAssetGroupSchema));

                var assetpath = AssetDatabase.GetAssetPath(obj);
                var guid = AssetDatabase.AssetPathToGUID(assetpath);

                var e = settings.CreateOrMoveEntry(guid, group, false, false);
                var entriesAdded = new List<AddressableAssetEntry> { e };

                group.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entriesAdded, false, true);
                settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entriesAdded, true, false);
            }
        }
#endif
    }
}