﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameReady.EditorTools.UIElements;
using Gameready.Utils.Data;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(KeyValueDictionary<,>), true)]
    public class KeyValueDictionaryDrawer : BaseUIToolkitDrawer
    {
        private List<VisualElement> _kvElements;

        private SerializedProperty _property;
        private SerializedProperty _keyValues;
        private int _keyValuesSize;
        private VisualElement _content;
        private VisualElement _root;

        private IVisualElementScheduledItem _scheduler;

        protected override VisualElement CreateProperty(SerializedProperty property)
        {
            _property = property;

            _keyValues = property.FindPropertyRelative("_keyValues");

            _kvElements = new List<VisualElement>();

            var asset = UIElementTemplatesData.LoadAssetTree(UIElementTemplatesData.KeyValueDictionaryGuid);

            _root = new VisualElement();
            _root.style.flexGrow = 1f;

            asset.CloneTree(_root);

            var foldout = _root.Q<Foldout>("foldout");
            foldout.text = property.displayName;

            var addButton = _root.Q<Button>("add");
            addButton.clicked += OnAddItemHandler;

            var removeButton = _root.Q<Button>("remove");
            removeButton.clicked += OnRemoveClickedHandler;

            var clearButton = _root.Q<Button>("clear");
            clearButton.clicked += OnClearButtonHandler;


            _content = _root.Q<VisualElement>("content");

            _scheduler = _root.schedule.Execute(CheckKeyValues).Every(150);

            FillItems();

            return _root;
        }

        private void CheckKeyValues()
        {
            try
            {
                if (_keyValuesSize != _keyValues.arraySize)
                {
                    _keyValuesSize = _keyValues.arraySize;
                    FillItems();
                }
            }
            catch (Exception)
            {
                _scheduler?.Pause();
            }
        }

        private void OnClearButtonHandler()
        {
            var result =
                EditorUtility.DisplayDialogComplex($"Clear Dictionary?", _property.displayName, "yes", "no", "");
            if (result == 1) return;
            foreach (var element in _kvElements)
            {
                element.Unbind();
            }

            _keyValues?.ClearArray();
            _keyValues?.serializedObject.ApplyModifiedProperties();
            _kvElements.Clear();

            //FillItems();
        }

        private void OnRemoveClickedHandler()
        {
            var index = _keyValues.arraySize - 1;
            if (index >= 0)
            {
                _keyValues.DeleteArrayElementAtIndex(index);
                _keyValues.serializedObject.ApplyModifiedProperties();
                _keyValues.serializedObject.Update();
                _kvElements.RemoveAt(index);

                // FillItems();
            }
        }

        private void OnAddItemHandler()
        {
            var index = Mathf.Max(0, _keyValues.arraySize - 1);

            _keyValues.InsertArrayElementAtIndex(index);
            _keyValues.serializedObject.ApplyModifiedProperties();
            _keyValues.serializedObject.Update();

            //FillItems();
        }

        private void FillItems()
        {
            _kvElements.Clear();
            _content.Clear();
            _content.MarkDirtyRepaint();

            for (int i = 0; i < _keyValues.arraySize; i++)
            {
                var property = _keyValues.GetArrayElementAtIndex(i);
                var element = KeyValuePropertyDrawer.CreateKeyValueElement(property, false);
                element.Bind(property.serializedObject);
                element.Q<PropertyField>("key")?.RegisterValueChangeCallback(OnKeyChangedHandler);

                _content.Add(element);
                _kvElements.Add(element);
            }

            CheckDuplicates();
        }

        private void OnKeyChangedHandler(SerializedPropertyChangeEvent evt)
        {
            evt.changedProperty.serializedObject.ApplyModifiedProperties();
            CheckDuplicates();
        }

        private void DrawItemOutline(VisualElement element, Color color, float width = 0f, string tooltip = "")
        {
            element.style.borderBottomColor = color;
            element.style.borderLeftColor = color;
            element.style.borderRightColor = color;
            element.style.borderTopColor = color;
            element.style.borderBottomWidth = width * 0.5f;
            element.style.borderLeftWidth = width;
            element.style.borderRightWidth = width;
            element.style.borderTopWidth = width * 0.5f;
            element.tooltip = tooltip;
        }

        private void CheckDuplicates()
        {
            var hashTableStr = new HashSet<string>();
            var hashTableInt = new HashSet<int>();
            var hashTableObject = new HashSet<object>();


            _kvElements.ForEach(element => DrawItemOutline(element, Color.clear));

            for (int i = 0; i < _keyValues.arraySize; i++)
            {
                var property = _keyValues.GetArrayElementAtIndex(i);
                var key = property.FindPropertyRelative("key");

                switch (key.propertyType)
                {
                    case SerializedPropertyType.String:
                        if (!hashTableStr.Add(key.stringValue))
                        {
                            DrawItemOutline(_kvElements[i], Color.red, 1f,
                                $"Has Duplicated key:{key.stringValue}");
                        }

                        break;
                    case SerializedPropertyType.Integer:

                        if (!hashTableInt.Add(key.intValue))
                        {
                            DrawItemOutline(_kvElements[i], Color.red, 1f,
                                $"Has Duplicated key:{key.intValue}");
                        }

                        break;
                    case SerializedPropertyType.Enum:
                        if (!hashTableInt.Add(key.enumValueIndex))
                        {
                            DrawItemOutline(_kvElements[i], Color.red, 1f,
                                $"Has Duplicated key:{key.enumValueIndex}");
                        }

                        break;
                    default:
                        try
                        {
                            if (key.boxedValue != null && !hashTableObject.Contains(key.boxedValue))
                            {
                                DrawItemOutline(_kvElements[i], Color.red, 1f,
                                    $"Has Duplicated key:{key.boxedValue.ToString()}");
                            }
                        }
                        catch (Exception e)
                        {
                            Debug.LogError(e.Message);
                        }


                        break;
                }
            }
        }
    }
}