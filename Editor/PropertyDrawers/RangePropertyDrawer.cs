using Gameready.Utils.Data;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(Range), true)]
    public class RangePropertyDrawer : PropertyDrawer
    {
        private SerializedProperty _property;

        private VisualElement _container;

        private FloatField _minField;
        private FloatField _maxField;

        private Range _range;

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            _range = (Range)property.boxedValue;

            _property = property;
            var root = new VisualElement();
            root.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
            root.style.alignContent = new StyleEnum<Align>(Align.FlexStart);
            root.style.alignItems = new StyleEnum<Align>(Align.Stretch);

            var minProp = _property.FindPropertyRelative("_min");
            var maxProp = _property.FindPropertyRelative("_max");

            _container = new VisualElement();
            _container.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
            _container.style.alignContent = new StyleEnum<Align>(Align.Stretch);
            _container.style.alignSelf = new StyleEnum<Align>(Align.Stretch);

            _container.style.flexGrow = 1f;
            _container.style.flexShrink = 1f;
            _container.style.justifyContent = new StyleEnum<Justify>(Justify.SpaceBetween);
            _container.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);

            _minField = new FloatField(minProp.displayName);
            _minField.BindProperty(minProp);
            _minField.bindingPath = minProp.propertyPath;
            _minField.labelElement.style.minWidth = new StyleLength(StyleKeyword.Auto);
            _minField.style.flexGrow = 1f;
            _minField.RegisterValueChangedCallback(MinValueChanged);


            _maxField = new FloatField(maxProp.displayName);
            _maxField.BindProperty(maxProp);
            _maxField.bindingPath = maxProp.propertyPath;
            _maxField.style.alignContent = new StyleEnum<Align>(Align.Stretch);
            _maxField.style.alignItems = new StyleEnum<Align>(Align.Stretch);
            _maxField.style.alignSelf = new StyleEnum<Align>(Align.Stretch);

            _maxField.style.flexGrow = 1f;
            _maxField.labelElement.style.minWidth = new StyleLength(StyleKeyword.Auto);
            _maxField.RegisterValueChangedCallback(MaxValueChanged);

            var label = new Label(property.displayName + ":");
            label.style.unityTextAlign = new StyleEnum<TextAnchor>(TextAnchor.MiddleCenter);
            _container.Add(label);
            _container.Add(_minField);
            _container.Add(_maxField);

            root.Add(_container);

            return root;
        }

        private void MinValueChanged(ChangeEvent<float> evt)
        {
            _range = (Range)_property.boxedValue;

            if (!_range.Validate())
            {
                _minField.style.backgroundColor = new StyleColor(Color.red);
                _minField.tooltip = "Min Value Bigger than Max";
            }
            else
            {
                _minField.tooltip = "";
                _minField.style.backgroundColor = new StyleColor(StyleKeyword.Auto);
            }
        }

        private void MaxValueChanged(ChangeEvent<float> evt)
        {
            _range = (Range)_property.boxedValue;
            if (!_range.Validate())
            {
                _minField.style.backgroundColor = new StyleColor(Color.red);
                _minField.tooltip = "Min Value Bigger than Max";
            }
            else
            {
                _minField.tooltip = "";
                _minField.style.backgroundColor = new StyleColor(StyleKeyword.Auto);
            }
        }
    }
}