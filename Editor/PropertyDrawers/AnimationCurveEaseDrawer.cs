using Gameready.Animations.Easing;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(AnimationCurveEasings))]
    public class AnimationCurveEaseDrawer : BaseUIToolkitDrawer
    {
        protected override VisualElement CreateProperty(SerializedProperty property)
        {
            var root = new VisualElement();

            root.style.alignContent = new StyleEnum<Align>(Align.Auto);
            root.style.alignItems = new StyleEnum<Align>(Align.Auto);
            root.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);

            var ease = property.FindPropertyRelative("_ease");
            var curve = property.FindPropertyRelative("_curve");

            var updateButton = new Button(() =>
            {
                var animationCurveEasings = property.boxedValue as AnimationCurveEasings;
                animationCurveEasings?.UpdateCurve();
            });
            updateButton.text = "Update";
            updateButton.style.flexShrink = 0.5f;

            var easeField = new PropertyField(ease, "")
            {
                style =
                {
                    flexGrow = 0.5f
                }
            };

            var curveField = new CurveField(property.displayName);
            curveField.BindProperty(curve);
           
            curveField.style.flexGrow = 0.5f;
            curveField.style.flexShrink = 0.5f;
            curveField.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
            curveField.style.alignSelf = new StyleEnum<Align>(Align.Stretch);
            
            root.Add(curveField);
            root.Add(easeField);
            root.Add(updateButton);

            return root;
        }
    }
}