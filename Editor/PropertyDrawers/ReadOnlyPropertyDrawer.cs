﻿using GameReady.EditorTools.PropertyDrawers.ItemsSystem;
using Gameready.Utils.Attributes;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute), true)]
    public class ReadOnlyPropertyDrawer : BaseUIToolkitDrawer
    {
        protected override VisualElement CreateProperty(SerializedProperty property)
        {
            var container = new VisualElement();
            container.SetEnabled(false);
            if (property.type.Contains("KeyValue`2"))
            {
                container.Add(KeyValuePropertyDrawer.CreateKeyValueElement(property));
            }
            else if (property.type.Contains("HashItemId"))
            {
                container.Add(HashItemIDPropertyDrawer.DrawDefaultHashItemId(property,Color.clear));
            }
            else
            {
                container.Add(new PropertyField(property));
            }

            return container;
        }
    }
}