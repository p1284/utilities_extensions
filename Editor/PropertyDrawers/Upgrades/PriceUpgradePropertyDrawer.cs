using GameReady.EditorTools.Windows;
using Gameready.Gameplay;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(PriceUpgrade))]
    public class PriceUpgradePropertyDrawer : UpgradePropertyDrawer
    {
        protected override void CreateFields(Foldout foldout, SerializedProperty property)
        {
            foldout.Add(new PropertyField(property.FindPropertyRelative("_price")));
            base.CreateFields(foldout, property);
        }

        protected override void OpenCalcWindow(SerializedProperty property)
        {
            var upgrade = property.boxedValue as PriceUpgrade;
            UpgradeCalcWindow.Show(property.displayName, upgrade);
        }
    }
}