using System;
using Gameready.Gameplay;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(UpgradeElement))]
    public class UpgradeElementPropertyDrawer : PropertyDrawer
    {
        private FloatField _upgradeKoofField;
        private EnumField _upgradeCalcType;
        private CurveField _upgradeCurveField;

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var root = new VisualElement();

            var foldout = new Foldout();
            foldout.text = property.displayName.ToUpper();

            var label = foldout.Q<Label>();
            label.style.color = new StyleColor(Color.blue);
            label.style.backgroundColor = new StyleColor(Color.gray);
            label.style.flexGrow = 1f;
            label.style.color = new StyleColor(Color.white);
            label.style.paddingLeft = 5;
            label.style.paddingBottom = label.style.paddingTop = 2;
            label.style.fontSize = 12;
            label.style.unityFontStyleAndWeight = new StyleEnum<FontStyle>(FontStyle.Bold);

            _upgradeKoofField = new FloatField("Upgrade Koof");
            _upgradeKoofField.bindingPath = property.FindPropertyRelative("_upgradeKoof").propertyPath;

            _upgradeCalcType = new EnumField("Upgrade CalcType", UpgradeCalcType.Linear);
            _upgradeCalcType.bindingPath = property.FindPropertyRelative("_upgradeCalcType").propertyPath;
            _upgradeCalcType.RegisterValueChangedCallback(UpgradeCalcTypeChanged);

            _upgradeCurveField = new CurveField("Upgrade Curve");
            _upgradeCurveField.bindingPath = property.FindPropertyRelative("_upgradeCurve").propertyPath;
            _upgradeCurveField.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.None);

            foldout.Add(_upgradeKoofField);
            foldout.Add(new PropertyField(property.FindPropertyRelative("_baseUpgradeValue")));
            foldout.Add(_upgradeCalcType);
            foldout.Add(_upgradeCurveField);

            foldout.Add(new PropertyField(property.FindPropertyRelative("_maxUpgrades")));

            root.Add(foldout);

            return root;
        }

        private void UpgradeCalcTypeChanged(ChangeEvent<Enum> evt)
        {
            var calcType = (UpgradeCalcType)evt.newValue;
            _upgradeKoofField.label = calcType == UpgradeCalcType.Percentage ? "Upgrade %" : "Upgrade Koof";

            _upgradeCurveField.style.display = calcType == UpgradeCalcType.Curve
                ? new StyleEnum<DisplayStyle>(DisplayStyle.Flex)
                : new StyleEnum<DisplayStyle>(DisplayStyle.None);
        }

        private Label CreateHeader(string text)
        {
            var header = new Label(text);
            header.style.backgroundColor = new StyleColor(Color.gray);
            header.style.color = new StyleColor(Color.white);
            header.style.paddingLeft = 5;
            header.style.fontSize = 12;
            header.style.unityFontStyleAndWeight = new StyleEnum<FontStyle>(FontStyle.Bold);

            return header;
        }
    }
}