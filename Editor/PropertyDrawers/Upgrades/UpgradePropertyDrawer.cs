using GameReady.EditorTools.Windows;
using Gameready.Gameplay;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(Upgrade))]
    public class UpgradePropertyDrawer : PropertyDrawer
    {
        private Button _calcButton;

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var root = new VisualElement();

            var foldout = new Foldout();
            foldout.text = property.displayName;

            CreateFields(foldout, property);

            _calcButton = new Button(() => OpenCalcWindow(property));
            _calcButton.text = "Calculate";
            _calcButton.style.flexGrow = 1f;
            _calcButton.style.width = new StyleLength(Length.Percent(100));

            foldout.Add(new PropertyField(property.FindPropertyRelative("_maxUpgrades")));

            foldout.Add(_calcButton);

            root.Add(foldout);

            return root;
        }

        protected virtual void CreateFields(Foldout foldout, SerializedProperty property)
        {
            foldout.Add(new PropertyField(property.FindPropertyRelative("_upgrade")));
        }

        protected virtual void OpenCalcWindow(SerializedProperty property)
        {
            var upgrade = property.boxedValue as Upgrade;
            UpgradeCalcWindow.Show(property.displayName, upgrade);
        }
    }
}