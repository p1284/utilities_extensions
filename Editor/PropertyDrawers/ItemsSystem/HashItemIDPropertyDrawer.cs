using System.Collections.Generic;
using System.Linq;
using GameReady.EditorTools.Extensions;
using Gameready.Utils.ItemsSystem;
using Gameready.Utils.ItemsSystem.Attributes;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.PropertyDrawers.ItemsSystem
{

    [CustomPropertyDrawer(typeof(HashItemIdsSelectorAttribute))]
    [CustomPropertyDrawer(typeof(HashItemId), true)]
    public class HashItemIDPropertyDrawer : BaseUIToolkitDrawer
    {
        private static HashItemsDb _itemsDb;


        private static IReadOnlyList<HashItemId> GetHashItems(string storageName)
        {
            if (_itemsDb == null)
            {
                _itemsDb =
                    EditorHelpers.FindScriptableAsset(typeof(HashItemsDb)) as HashItemsDb;
            }

            return _itemsDb == null ? new List<HashItemId>() : _itemsDb.GetHashItemsFromStorage(storageName);
        }


        protected override VisualElement CreateProperty(SerializedProperty property)
        {
            var root = new VisualElement();

            HashItemIdsSelectorAttribute selectorAttribute = attribute as HashItemIdsSelectorAttribute;

            if (property.InsideArray())
            {
                if (selectorAttribute != null && property.type.Contains(nameof(HashItemId)))
                {
                    return CreateDataStorageIdsSelector(selectorAttribute.StorageName, property,false);
                }

                root.Add(DrawDefaultHashItemId(property, Color.clear, false));
            }
            else
            {
                if (selectorAttribute != null && property.type.Contains(nameof(HashItemId)))
                {
                    return CreateDataStorageIdsSelector(selectorAttribute.StorageName, property);
                }

                root.Add(DrawDefaultHashItemId(property, Color.green));
            }

            return root;
        }

        public static BindableElement CreateDataStorageIdsSelector(string storageName, SerializedProperty property,
            bool drawLabel = true, string label = "")
        {
            HashItemId selectedItem = HashItemId.EmptyItemId;

            if (property.boxedValue is HashItemId item)
            {
                selectedItem = HashItemId.Create(item.Name, item.StorageName);
            }

            var dropDownField =
                new PopupField<HashItemId>(drawLabel ? $"{property.displayName}" : label);


            dropDownField.choices.Add(HashItemId.EmptyItemId);

            var hashItems = GetHashItems(storageName);

            foreach (var hashItemData in hashItems)
            {
                dropDownField.choices.Add(hashItemData);
            }

            dropDownField.RegisterValueChangedCallback(evt =>
            {
                property.boxedValue = evt.newValue;
                property.serializedObject.ApplyModifiedProperties();
            });

            dropDownField.value = selectedItem;

            return dropDownField;
        }

        public static GroupBox CreateDefaultHashItemId(string label, Color backColor, bool drawLabel = true)
        {
            var hbox = new GroupBox();
            hbox.pickingMode = PickingMode.Ignore;
            hbox.style.height = 22;
            hbox.style.marginBottom = hbox.style.marginTop = 0;
            hbox.style.paddingBottom = hbox.style.paddingTop = 2f;
            hbox.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);

            if (drawLabel)
            {
                hbox.Add(new Label(label)
                {
                    style =
                    {
                        flexGrow = 1f, flexShrink = 1f, width = Length.Percent(20),
                        unityTextAlign = new StyleEnum<TextAnchor>(TextAnchor.MiddleLeft)
                    },
                    name = "label"
                });
            }


            var nameLabel = new Label("");
            nameLabel.style.width = Length.Percent(30);
            nameLabel.style.flexShrink = 1f;
            nameLabel.style.flexGrow = 1f;
            nameLabel.style.textOverflow = new StyleEnum<TextOverflow>(TextOverflow.Clip);
            nameLabel.style.unityTextAlign =
                new StyleEnum<TextAnchor>(TextAnchor.MiddleLeft);
            nameLabel.style.unityFontStyleAndWeight = new StyleEnum<FontStyle>(FontStyle.Bold);
            nameLabel.style.backgroundColor = backColor;
            nameLabel.name = "nameLabel";
            nameLabel.bindingPath = "_name";
            hbox.Add(nameLabel);

            var hashLabel = new Label("");
            hashLabel.style.width = Length.Percent(50);
            hashLabel.name = "hashLabel";
            hashLabel.style.backgroundColor = backColor;
            hashLabel.bindingPath = "_hash";
            hashLabel.style.textOverflow = new StyleEnum<TextOverflow>(TextOverflow.Clip);
            hashLabel.style.unityTextAlign =
                new StyleEnum<TextAnchor>(TextAnchor.MiddleLeft);
            hashLabel.AddManipulator(new ContextualMenuManipulator(@event =>
            {
                @event.menu.AppendAction("Copy", action => { EditorGUIUtility.systemCopyBuffer = nameLabel.text; });
            }));
            hashLabel.style.flexShrink = 1f;
            hashLabel.style.flexGrow = 1f;
            hashLabel.style.unityFontStyleAndWeight = new StyleEnum<FontStyle>(FontStyle.Bold);

            hbox.Add(hashLabel);

            return hbox;
        }

        public static VisualElement DrawDefaultHashItemId(SerializedProperty property, Color backColor,
            bool drawLabel = true)
        {
            var hbox = CreateDefaultHashItemId(property.displayName, backColor, drawLabel);
            hbox.BindProperty(property);
            return hbox;
        }
    }
}