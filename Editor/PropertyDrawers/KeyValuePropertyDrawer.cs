using System.Collections.Generic;
using GameReady.EditorTools.Extensions;
using Gameready.Utils.Data;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameReady.EditorTools.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(KeyValue<,>), true)]
    public class KeyValuePropertyDrawer : BaseUIToolkitDrawer
    {
        private static readonly HashSet<SerializedPropertyType> ValuePropertyTypes = new HashSet<SerializedPropertyType>
        {
            SerializedPropertyType.Boolean,
            SerializedPropertyType.Enum,
            SerializedPropertyType.Float,
            SerializedPropertyType.Integer,
            SerializedPropertyType.Color,
            SerializedPropertyType.String,
            SerializedPropertyType.AnimationCurve,
            SerializedPropertyType.ExposedReference,
            SerializedPropertyType.ObjectReference,
            SerializedPropertyType.LayerMask,
            SerializedPropertyType.Character,
            SerializedPropertyType.Hash128
        };

        private static readonly HashSet<SerializedPropertyType> keyPropertyTypes = new HashSet<SerializedPropertyType>()
        {
            SerializedPropertyType.String,
            SerializedPropertyType.Integer,
            SerializedPropertyType.Enum
        };

        protected override VisualElement CreateProperty(SerializedProperty property)
        {
            return CreateKeyValueElement(property);
        }

        public static VisualElement CreateKeyValueElement(SerializedProperty property, bool addContextMenu = true)
        {
            var root = new VisualElement();
            //root.style.flexGrow = 1f;

            var value = property.FindPropertyRelative("value");
            var key = property.FindPropertyRelative("key");

            var keyProp = new PropertyField(key, "");
            keyProp.name = "key";


            var valueProp = new PropertyField(value, "");
            valueProp.name = "value";

            if (CanDrawKeyInline(key.propertyType) && CanDrawInLineValue(value))
            {
                root.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
                root.style.alignContent = new StyleEnum<Align>(Align.Stretch);
                root.style.alignItems = new StyleEnum<Align>(Align.Stretch);
                root.style.justifyContent = new StyleEnum<Justify>(Justify.SpaceBetween);
                root.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);
                
                if (property.InsideArray())
                {
                    if (addContextMenu)
                    { 
                        var list = property.FindParentProperty();
                        root.AddManipulator(new ContextualMenuManipulator(@event =>
                        {
                            @event.menu.AppendAction("Delete", action =>
                            {
                                for (int i = 0; i < list.arraySize; i++)
                                {
                                    var item = list.GetArrayElementAtIndex(i);
                                    if (item.contentHash == property.contentHash)
                                    {
                                        list.DeleteArrayElementAtIndex(i);
                                        list.serializedObject.ApplyModifiedProperties();
                                        break;
                                    }
                                }
                            });
                        }));
                    }

                }
              
                keyProp.style.width = new StyleLength(Length.Percent(50));
                valueProp.style.width = new StyleLength(Length.Percent(50));


              //  root.Add(label);
                root.Add(keyProp);
                root.Add(valueProp);
            }
            else
            {
                var foldout = new Foldout();
                foldout.text = property.displayName;
                keyProp.label = "Key";
                valueProp.label = "Value";
                foldout.Add(keyProp);
                foldout.Add(valueProp);
                root.Add(foldout);
            }

            return root;
        }

        public static bool CanDrawInLineValue(SerializedProperty valueProp)
        {
            if (valueProp.type.Contains("KeyValue")) return false;

#if ADDRESSABLES
            if (valueProp.boxedValue is UnityEngine.AddressableAssets.AssetReference) return true;
#endif
            return ValuePropertyTypes.Contains(valueProp.propertyType);
        }

        public static bool CanDrawKeyInline(SerializedPropertyType type)
        {
            return keyPropertyTypes.Contains(type);
        }
    }
}